<?php

include_once dirname(__FILE__) . '/'  . 'exceptions.php';
include_once dirname(__FILE__) . '/'  . 'duration-field-type.php';

/**
 * Identifies a field, such as year or minuteOfHour, in a chronology-neutral way.
 * <p>
 * A field type defines the type of the field, such as hourOfDay.
 * If does not directly enable any calculations, however it does provide a
 * {@link #getField(Chronology)} method that returns the actual calculation engine
 * for a particular chronology.
 * It also provides access to the related {@link DurationFieldType}s.
 * <p>
 * Instances of <code>DateTimeFieldType</code> are singletons.
 * They can be compared using <code>==</code>.
 * <p>
 * If required, you can create your own field, for example a quarterOfYear.
 * You must create a subclass of <code>DateTimeFieldType</code> that defines the field type.
 * This class returns the actual calculation engine from {@link #getField(Chronology)}.
 * The subclass should implement equals and hashCode.
 */
abstract class DateTimeFieldType {

    /** Ordinal values for standard field types. */
    const   ERA = 1,
        YEAR_OF_ERA = 2,
        CENTURY_OF_ERA = 3,
        YEAR_OF_CENTURY = 4,
        YEAR = 5,
        DAY_OF_YEAR = 6,
        MONTH_OF_YEAR = 7,
        DAY_OF_MONTH = 8,
        WEEKYEAR_OF_CENTURY = 9,
        WEEKYEAR = 10,
        WEEK_OF_WEEKYEAR = 11,
        DAY_OF_WEEK = 12,
        HALFDAY_OF_DAY = 13,
        HOUR_OF_HALFDAY = 14,
        CLOCKHOUR_OF_HALFDAY = 15,
        CLOCKHOUR_OF_DAY = 16,
        HOUR_OF_DAY = 17,
        MINUTE_OF_DAY = 18,
        MINUTE_OF_HOUR = 19,
        SECOND_OF_DAY = 20,
        SECOND_OF_MINUTE = 21,
        MILLIS_OF_DAY = 22,
        MILLIS_OF_SECOND = 23;

    /**
     * @var DateTimeFieldType
     */
    private static $eraType;
    private static $yearType;
    private static $yearOfEraType;
    private static $centuryOfEraType;
    private static $yearOfCenturyType;
    private static $dayOfYearType;
    private static $monthOfYearType;
    private static $dayOfMonthType;
    private static $weekyearOfCenturyType;
    private static $weekyearType;
    private static $weekOfWeekyearType;
    private static $dayOfWeekType;
    private static $halfdayOfDayType;
    private static $hourOfHalfdayType;
    private static $clockhourOfHalfdayType;
    private static $clockhourOfDayType;
    private static $hourOfDayType;
    private static $minuteOfDayType;
    private static $minuteOfHourType;
    private static $secondOfDayType;
    private static $secondOfMinuteType;
    private static $millisOfDayDayType;
    private static $millisOfSecondType;

    public static function staticInit() {


        self::$eraType = new StandardDateTimeFieldType("era", self::ERA, DurationFieldType::eras(), null);
        self::$yearType = new StandardDateTimeFieldType("year", self::YEAR, DurationFieldType::years(), null);

        self::$yearOfEraType = new StandardDateTimeFieldType("yearOfEra", self::YEAR_OF_ERA, DurationFieldType::years(), DurationFieldType::eras());
        self::$centuryOfEraType = new StandardDateTimeFieldType("centuryOfEra", self::CENTURY_OF_ERA, DurationFieldType::centuries(), DurationFieldType::eras());
        self::$yearOfCenturyType = new StandardDateTimeFieldType("yearOfCentury", self::YEAR_OF_CENTURY, DurationFieldType::years(), DurationFieldType::centuries());


        self::$dayOfYearType = new StandardDateTimeFieldType("dayOfYear", self::DAY_OF_YEAR, DurationFieldType::days(), DurationFieldType::years());
        self::$monthOfYearType = new StandardDateTimeFieldType("monthOfYear", self::MONTH_OF_YEAR, DurationFieldType::months(), DurationFieldType::years());
        self::$dayOfMonthType = new StandardDateTimeFieldType("dayOfMonth", self::DAY_OF_MONTH, DurationFieldType::days(), DurationFieldType::months());
        self::$weekyearOfCenturyType = new StandardDateTimeFieldType("weekyearOfCentury", self::WEEKYEAR_OF_CENTURY, DurationFieldType::weekyears(), DurationFieldType::centuries());
        self::$weekyearType = new StandardDateTimeFieldType("weekyear", self::WEEKYEAR, DurationFieldType::weekyears(), null);
        self::$weekOfWeekyearType = new StandardDateTimeFieldType("weekOfWeekyear", self::WEEK_OF_WEEKYEAR, DurationFieldType::weeks(), DurationFieldType::weekyears());
        self::$dayOfWeekType = new StandardDateTimeFieldType("dayOfWeek", self::DAY_OF_WEEK, DurationFieldType::days(), DurationFieldType::weeks());

        self::$halfdayOfDayType = new StandardDateTimeFieldType("halfdayOfDay", self::HALFDAY_OF_DAY, DurationFieldType::halfdays(), DurationFieldType::days());
        self::$hourOfHalfdayType = new StandardDateTimeFieldType("hourOfHalfday", self::HOUR_OF_HALFDAY, DurationFieldType::hours(), DurationFieldType::halfdays());
        self::$clockhourOfHalfdayType = new StandardDateTimeFieldType("clockhourOfHalfday", self::CLOCKHOUR_OF_HALFDAY, DurationFieldType::hours(), DurationFieldType::halfdays());
        self::$clockhourOfDayType = new StandardDateTimeFieldType("clockhourOfDay", self::CLOCKHOUR_OF_DAY, DurationFieldType::hours(), DurationFieldType::days());
        self::$hourOfDayType = new StandardDateTimeFieldType("hourOfDay", self::HOUR_OF_DAY, DurationFieldType::hours(), DurationFieldType::days());
        self::$minuteOfDayType = new StandardDateTimeFieldType("minuteOfDay", self::MINUTE_OF_DAY, DurationFieldType::minutes(), DurationFieldType::days());
        self::$minuteOfHourType = new StandardDateTimeFieldType("minuteOfHour", self::MINUTE_OF_HOUR, DurationFieldType::minutes(), DurationFieldType::hours());
        self::$secondOfDayType = new StandardDateTimeFieldType("secondOfDay", self::SECOND_OF_DAY, DurationFieldType::seconds(), DurationFieldType::days());
        self::$secondOfMinuteType = new StandardDateTimeFieldType("secondOfMinute", self::SECOND_OF_MINUTE, DurationFieldType::seconds(), DurationFieldType::minutes());
        self::$millisOfDayDayType = new StandardDateTimeFieldType("millisOfDay", self::MILLIS_OF_DAY, DurationFieldType::millis(), DurationFieldType::days());
        self::$millisOfSecondType = new StandardDateTimeFieldType("millisOfSecond", self::MILLIS_OF_SECOND, DurationFieldType::millis(), DurationFieldType::seconds());

    }

    /**
     * @return DateTimeFieldType
     */
    public static function year() {
        return self::$yearType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function millisOfSecond() {
        return self::$millisOfSecondType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function millisOfDay() {
        return self::$millisOfDayDayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function secondOfMinute() {
        return self::$secondOfMinuteType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function secondOfDay() {
        return self::$secondOfDayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function minuteOfHour() {
        return self::$minuteOfHourType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function minuteOfDay() {
        return self::$minuteOfDayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function hourOfDay() {
        return self::$hourOfDayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function clockhourOfDay() {
        return self::$clockhourOfDayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function hourOfHalfday() {
        return self::$hourOfHalfdayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function clockhourOfHalfday() {
        return self::$clockhourOfHalfdayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function halfdayOfDay() {
        return self::$halfdayOfDayType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function dayOfWeek() {
        return self::$dayOfWeekType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function dayOfMonth() {
        return self::$dayOfMonthType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function dayOfYear() {
        return self::$dayOfYearType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function weekOfWeekyear() {
        return self::$weekOfWeekyearType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function weekyear() {
        return self::$weekyearType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function weekyearOfCentury() {
        return self::$weekyearOfCenturyType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function monthOfYear() {
        return self::$monthOfYearType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function yearOfEra() {
        return self::$yearOfEraType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function yearOfCentury() {
        return self::$yearOfCenturyType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function centuryOfEra() {
        return self::$centuryOfEraType;
    }

    /**
     * @return DateTimeFieldType
     */
    public static function era() {
        return self::$eraType;
    }

    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     */
    protected function __construct($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Gets a suitable field for this type from the given Chronology.
     *
     * @param Chronology $chronology  the chronology to use, null means ISOChronology in default zone
     * @return DateTimeField a suitable field
     */
    public abstract function getField(Chronology $chronology);
}

DateTimeFieldType::staticInit();

/**
 * Basic implementation of DateTimeFieldType
 */
class StandardDateTimeFieldType extends DateTimeFieldType {

    /**
     * @var int
     */
    private $ordinal;

    /**
     * @var DurationFieldType
     */
    private $unitType;

    /**
     * @var DurationFieldType
     */
    private $rangeType;

    /**
     * @param string $name
     * @param int $ordinal
     * @param DurationFieldType $unitType
     * @param DurationFieldType|null $rangeType
     */
    public function __construct($name, $ordinal, DurationFieldType $unitType, DurationFieldType $rangeType = null) {
        parent::__construct($name);
        $this->ordinal = $ordinal;
        $this->unitType = $unitType;
        $this->rangeType = $rangeType;
    }

    /**
     * Gets a suitable field for this type from the given Chronology.
     *
     * @param Chronology $chronology  the chronology to use, null means ISOChronology in default zone
     * @throws InternalError
     * @return DateTimeField a suitable field
     */
    public function getField(Chronology $chronology) {
        $chronology = DateTimeUtils::getChronology($chronology);

        switch ($this->ordinal) {
            case self::ERA:
                return $chronology->era();
            case self::YEAR_OF_ERA:
                return $chronology->yearOfEra();
            case self::CENTURY_OF_ERA:
                return $chronology->centuryOfEra();
            case self::YEAR_OF_CENTURY:
                return $chronology->yearOfCentury();
            case self::YEAR:
                return $chronology->year();
            case self::DAY_OF_YEAR:
                return $chronology->dayOfYear();
            case self::MONTH_OF_YEAR:
                return $chronology->monthOfYear();
            case self::DAY_OF_MONTH:
                return $chronology->dayOfMonth();
            case self::WEEKYEAR_OF_CENTURY:
                return $chronology->weekyearOfCentury();
            case self::WEEKYEAR:
                return $chronology->weekyear();
            case self::WEEK_OF_WEEKYEAR:
                return $chronology->weekOfWeekyear();
            case self::DAY_OF_WEEK:
                return $chronology->dayOfWeek();
            case self::HALFDAY_OF_DAY:
                return $chronology->halfdayOfDay();
            case self::HOUR_OF_HALFDAY:
                return $chronology->hourOfHalfday();
            case self::CLOCKHOUR_OF_HALFDAY:
                return $chronology->clockhourOfHalfday();
            case self::CLOCKHOUR_OF_DAY:
                return $chronology->clockhourOfDay();
            case self::HOUR_OF_DAY:
                return $chronology->hourOfDay();
            case self::MINUTE_OF_DAY:
                return $chronology->minuteOfDay();
            case self::MINUTE_OF_HOUR:
                return $chronology->minuteOfHour();
            case self::SECOND_OF_DAY:
                return $chronology->secondOfDay();
            case self::SECOND_OF_MINUTE:
                return $chronology->secondOfMinute();
            case self::MILLIS_OF_DAY:
                return $chronology->millisOfDay();
            case self::MILLIS_OF_SECOND:
                return $chronology->millisOfSecond();
            default:
                // Shouldn't happen.
                throw new InternalError();
        }
    }
}