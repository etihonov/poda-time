<?php

include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../field/precise-duration-datetime-field.php';

/**
 * Provides time calculations for the day of the year component of time.
 */
final class BasicDayOfYearDateTimeField extends PreciseDurationDateTimeField {

    /** @var \BasicChronology */
    private $chronology;

    /**
     * @param BasicChronology $chronology
     * @param DurationField $days
     */
    public function __construct(BasicChronology $chronology, DurationField $days) {
        parent::__construct(DateTimeFieldType::dayOfYear(), $days);
        $this->chronology = $chronology;
    }

    /**
     * Get the day of the year component of the specified time instant.
     *
     * @param int $millis
     * @return int
     */
    public function get($millis) {
        return $this->chronology->getDayOfYear($millis);
    }
}
