<?php

include_once dirname(__FILE__) . '/'  . '../field/precise-duration-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../locale.php';

include_once dirname(__FILE__) . '/'  . 'gj-locale-symbols.php';

/**
 * GJDayOfWeekDateTimeField provides time calculations for the
 * day of the week component of time.
 */
class GJDayOfWeekDateTimeField extends PreciseDurationDateTimeField {

    /** @var \BasicChronology */
    private $chronology;

    /**
     * @param BasicChronology $chronology
     * @param DurationField $days
     */
    public function __construct(BasicChronology $chronology, DurationField $days) {
        parent::__construct(DateTimeFieldType::dayOfWeek(), $days);
        $this->chronology = $chronology;
    }

    /**
     * @param int $instant
     * @return int
     */
    public function get($instant) {
        return $this->chronology->getDayOfWeek($instant);
    }

    /**
     * Get the abbreviated textual value of the specified time instant.
     *
     * @param int $fieldValue  the field value to query
     * @param PTLocale $locale  the locale to use
     * @return string the day of the week, such as 'Mon'
     */
    public function getAsShortTextFromValue($fieldValue, PTLocale $locale = null) {
        return GJLocaleSymbols::forLocale($locale)->dayOfWeekValueToShortText($fieldValue);
    }

}
