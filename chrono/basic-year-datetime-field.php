<?php

include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../field/imprecise-datetime-field.php';

/**
 * A year field suitable for many calendars.
 */
class BasicYearDateTimeField extends ImpreciseDateTimeField {
    /**
     * @var BasicChronology
     */
    protected $chronology;

    /**
     * @param BasicChronology $chronology
     */
    public function __construct(BasicChronology $chronology) {
        parent::__construct(DateTimeFieldType::year());
        $this->chronology = $chronology;
    }


    /**
     * @param int $instant
     * @return int
     */
    public function get($instant) {
        return $this->chronology->getYear($instant);
    }

    /**
     * {@inheritdoc}
     */
    public function set($instant, $year) {
        FieldUtils::verifyValueBounds($this, $year, $this->chronology->getMinYear(), $this->chronology->getMaxYear());
        return $this->chronology->setYear($instant, $year);
    }
}