<?php

/**
 * Provides time calculations for the week of the weekyear component of time.
 */
final class BasicWeekyearDateTimeField extends ImpreciseDateTimeField {
    private $chronology;

    /**
     * @param BasicChronology $chronology
     */
    public function __construct(BasicChronology $chronology) {
        parent::__construct(DateTimeFieldType::weekyear(), $chronology->getAverageMillisPerYear());
        $this->chronology = $chronology;
    }

    /**
     * Get the Year of a week based year component of the specified time instant.
     *
     * @param int $millis
     * @return int
     */
    public function get($millis){
        return $this->chronology->getWeekyear($millis);
    }

    /**
     * Set the Year of a week based year component of the specified time instant.
     *
     * @see DateTimeField.set
     * @param int $instant the time instant in millis to update.
     * @param int $year the year (-9999,9999) to set the date to.
     * @return int the updated DateTime.
     * @throws IllegalArgumentException  if year is invalid.
     */
    public function set($instant, $year) {
        //FieldUtils::verifyValueBounds(this, Math.abs(year), iChronology.getMinYear(), iChronology.getMaxYear());

        //
        // Do nothing if no real change is requested.
        //
        $thisWeekyear = $this->get($instant);
        if ($thisWeekyear == $year) {
            return $instant;
        }

        //
        // Calculate the DayOfWeek (to be preserved).
        //
        $thisDow = $this->chronology->getDayOfWeek($instant);

        //
        // Calculate the maximum weeks in the target year.
        //
        $weeksInFromYear = $this->chronology->getWeeksInYear($thisWeekyear);
        $weeksInToYear = $this->chronology->getWeeksInYear($year);
        $maxOutWeeks = ($weeksInToYear < $weeksInFromYear) ? $weeksInToYear : $weeksInFromYear;

        //
        // Get the current week of the year. This will be preserved in
        // the output unless it is greater than the maximum possible
        // for the target weekyear.  In that case it is adjusted
        // to the maximum possible.
        //
        $setToWeek = $this->chronology->getWeekOfWeekyear($instant);
        if ($setToWeek > $maxOutWeeks) {
            $setToWeek = $maxOutWeeks;
        }

        //
        // Get a wroking copy of the current date-time.
        // This can be a convenience for debugging.
        //
        $workInstant = $instant; // Get a copy

        //
        // Attempt to get close to the proper weekyear.
        // Note - we cannot currently call ourself, so we just call
        // set for the year.  This at least gets us close.
        //
        $workInstant = $this->chronology->setYear($workInstant, $year);

        //
        // Calculate the weekyear number for the get close to value
        // (which might not be equal to the year just set).
        //
        $workWoyYear = $this->get($workInstant);

        //
        // At most we are off by one year, which can be "fixed" by
        // adding/subtracting a week.
        //
        if ($workWoyYear < $year) {
            $workInstant += DateTimeConstants::$MILLIS_PER_WEEK;
        } else if ( $workWoyYear > $year ) {
            $workInstant -= DateTimeConstants::$MILLIS_PER_WEEK;
        }
        //
        // Set the proper week in the current weekyear.
        //

        // BEGIN: possible set WeekOfWeekyear logic.
        $currentWoyWeek = $this->chronology->getWeekOfWeekyear($workInstant);
        // No range check required (we already know it is OK).
        $workInstant = $workInstant + ($setToWeek - $currentWoyWeek) * DateTimeConstants::$MILLIS_PER_WEEK;
        // END: possible set WeekOfWeekyear logic.

        //
        // Reset DayOfWeek to previous value.
        //
        // Note: This works fine, but it ideally shouldn't invoke other
        // fields from within a field.
        $workInstant = $this->chronology->dayOfWeek()->set( $workInstant, $thisDow );
        //
        // Return result.
        //
        return $workInstant;
    }
}
