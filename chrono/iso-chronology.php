<?php

include_once dirname(__FILE__) . '/'  . '../core/containers.php';
include_once dirname(__FILE__) . '/'  . '../datetime-zone.php';
include_once dirname(__FILE__) . '/'  . 'zoned-chronology.php';
include_once dirname(__FILE__) . '/'  . 'iso-year-of-era-datetime-field.php';

/**
 * Implements a chronology that follows the rules of the ISO8601 standard,
 * which is compatible with Gregorian for all modern dates.
 * When ISO does not define a field, but it can be determined (such as AM/PM)
 * it is included.
 * <p>
 * With the exception of century related fields, ISOChronology is exactly the
 * same as {@link GregorianChronology}. In this chronology, centuries and year
 * of century are zero based. For all years, the century is determined by
 * dropping the last two digits of the year, ignoring sign. The year of century
 * is the value of the last two year digits.
 * <p>
 * ISOChronology is immutable.
 */
final class ISOChronology extends AssembledChronology {

    private static $INSTANCE_UTC;

    /** @var IMap */
    private static $cache;

    /**
     * Static constructor
     */
    public static function staticInit() {
        self::$cache = new Map();
        self::$INSTANCE_UTC = new ISOChronology(GregorianChronology::getInstanceUTC());

        self::$cache->put(PTDateTimeZone::$UTC, self::$INSTANCE_UTC);
    }

    /**
     * Gets an instance of the ISOChronology.
     * The time zone of the returned instance is UTC.
     *
     * @return ISOChronology a singleton UTC instance of the chronology
     */
    public static function getInstanceUTC() {
        return self::$INSTANCE_UTC;
    }

    /**
     * Gets an instance of the ISOChronology in the given time zone.
     *
     * @param PTDateTimeZone $zone  the time zone to get the chronology in, null is default
     * @return ISOChronology a chronology in the specified time zone
     */
    public static function getInstance(PTDateTimeZone $zone = null) {
        if ($zone == null) {
            $zone = PTDateTimeZone::getDefault();
        }
        $result = self::$cache->get($zone);
        if ($result == null) {
            $result = new ISOChronology(ZonedChronology::getInstance(self::$INSTANCE_UTC, $zone));
            self::$cache->put($zone, $result);
        }
        return $result;
    }

    /**
     * @param Chronology $base
     */
    public function __construct(Chronology $base) {
        parent::__construct($base, null);
    }

    /**
     * Gets the Chronology in the UTC time zone.
     * @return Chronology
     */
    public function withUTC() {
        return self::$INSTANCE_UTC;
    }

    /**
     * Gets the Chronology in a specific time zone.
     *
     * @param PTDateTimeZone $zone  the zone to get the chronology in, null is default
     * @return Chronology the chronology
     */
    public function withZone(PTDateTimeZone $zone = null) {
        if ($zone == null) {
            $zone = PTDateTimeZone::getDefault();
        }
        if ($zone == $this->getZone()) {
            return $this;
        }
        return self::getInstance($zone);
    }

    /**
     * @param Fields $fields container of fields
     */
    protected function assemble(Fields $fields) {


        if ($this->getBase()->getZone() == PTDateTimeZone::$UTC) { // Why?!

            // Use zero based century and year of century.
            $fields->centuryOfEra = new DividedDateTimeField(
                ISOYearOfEraDateTimeField::getInstance(), DateTimeFieldType::centuryOfEra(), 100);

            /*$fields->yearOfCentury = new RemainderDateTimeField(
                $fields->centuryOfEra, DateTimeFieldType::yearOfCentury());

            $fields->weekyearOfCentury = new RemainderDateTimeField(
                $fields->centuryOfEra, DateTimeFieldType::weekyearOfCentury());

            $fields->centuries = $fields->centuryOfEra->getDurationField();*/

        }

        /* TODO implement

            if ($this->getBase()->getZone() == PTDateTimeZone::$UTC) {

            // Use zero based century and year of century.
            $fields->centuryOfEra = new DividedDateTimeField(
                ISOYearOfEraDateTimeField::$INSTANCE, DateTimeFieldType::centuryOfEra(), 100);

            $fields->yearOfCentury = new RemainderDateTimeField(
                $fields->centuryOfEra, DateTimeFieldType::yearOfCentury());

            $fields->weekyearOfCentury = new RemainderDateTimeField(
                $fields->centuryOfEra, DateTimeFieldType::weekyearOfCentury());

            $fields->centuries = $fields->centuryOfEra->getDurationField();
        }

        */
    }
}
ISOChronology::staticInit();