<?php

include_once dirname(__FILE__) . '/'  . '../field/decorated-datetime-field.php';

/**
 * Provides time calculations for the year of era component of time.
 */
class GJYearOfEraDateTimeField extends DecoratedDateTimeField {

    private $chronology;

    /**
     * @param DateTimeField $yearField
     * @param BasicChronology $chronology
     */
    public function __construct(DateTimeField $yearField, BasicChronology $chronology) {
        parent::__construct($yearField, DateTimeFieldType::yearOfEra());
        $this->chronology = $chronology;
    }

    /**
     * @param int $millis
     * @return int
     */
    public function get($millis) {
        $year = $this->getWrappedField()->get($millis);
        if ($year <= 0) {
            $year = 1 - $year;
        }
        return $year;
    }

    /**
     * Set the year component of the specified time instant.
     *
     * @param int $instant  the time instant in millis to update.
     * @param int $year  the year (0,292278994) to update the time to.
     * @return int the updated time instant.
     * @throws IllegalArgumentException  if year is invalid.
     */
    public function set($instant, $year) {
        //FieldUtils::verifyValueBounds(this, year, 1, getMaximumValue());
        if ($this->chronology->getYear($instant) <= 0) {
            $year = 1 - $year;
        }
        return parent::set($instant, $year);
    }
}
