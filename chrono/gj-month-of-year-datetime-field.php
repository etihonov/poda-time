<?php

include_once dirname(__FILE__) . '/'  . 'basic-chronology.php';
include_once dirname(__FILE__) . '/'  . 'basic-month-of-year-datetime-field.php';

/**
 * Provides time calculations for the month of the year component of time.
 */
final class GJMonthOfYearDateTimeField extends BasicMonthOfYearDateTimeField {

    /**
     * @param BasicChronology $chronology
     */
    public function __construct(BasicChronology $chronology) {
        parent::__construct($chronology, 2);
    }
}
