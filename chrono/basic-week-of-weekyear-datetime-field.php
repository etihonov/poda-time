<?php

include_once dirname(__FILE__) . '/'  . '../chronology.php';
include_once dirname(__FILE__) . '/'  . '../duration-field.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../field/precise-duration-datetime-field.php';

/**
 * Provides time calculations for the week of a week based year component of time.
 */
final class BasicWeekOfWeekyearDateTimeField extends PreciseDurationDateTimeField {

    /** @var \BasicChronology */
    private $chronology;

    /**
     * @param BasicChronology $chronology
     * @param DurationField $weeks
     */
    public function __construct(BasicChronology $chronology, DurationField $weeks) {
        parent::__construct(DateTimeFieldType::weekOfWeekyear(), $weeks);
        $this->chronology = $chronology;
    }

    /**
     * Get the week of a week based year component of the specified time instant.
     * @param int $instant  the time instant in millis to query.
     * @return int the week of the year extracted from the input.
     */
    public function get($instant) {
        return $this->chronology->getWeekOfWeekyear($instant);
    }

}
