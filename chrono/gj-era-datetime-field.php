<?php

include_once dirname(__FILE__) . '/'  . '../chronology.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../field/base-datetime-field.php';

/**
 * Provides time calculations for the era component of time.
 */
class GJEraDateTimeField extends BaseDateTimeField {

    /** @var \BasicChronology */
    private $chronology;

    /**
     * @param BasicChronology $chronology
     */
    public function __construct(BasicChronology $chronology) {
        parent::__construct(DateTimeFieldType::era());
        $this->chronology = $chronology;
    }

    /**
     * @return bool
     */
    public function isLenient() {
        return false;
    }

    /**
     * @param int $millis
     * @return int
     */
    public function get($millis) {
        if ($this->chronology->getYear($millis) <= 0) {
            return DateTimeConstants::$BCE;
        } else {
            return DateTimeConstants::$CE;
        }
    }

    /**
     * Set the Era component of the specified time instant.
     *
     * @param int $instant  the time instant in millis to update.
     * @param int $era  the era to update the time to.
     * @return int the updated time instant.
     * @throws IllegalArgumentException  if era is invalid.
     */
    public function set($instant, $era) {
        //FieldUtils::verifyValueBounds(this, era, DateTimeConstants.BCE, DateTimeConstants.CE);
        $oldEra = $this->get($instant);
        if ($oldEra != $era) {
            $year = $this->chronology->getYear($instant);
            return $this->chronology->setYear($instant, -$year);
        } else {
            return $instant;
        }
    }
}
