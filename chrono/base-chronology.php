<?php

include_once dirname(__FILE__) . '/'  . '../chronology.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../field/unsupported-datetime-field.php';

/**
 * BaseChronology provides a skeleton implementation for chronology
 * classes. Many utility methods are defined, but all fields are unsupported.
 * <p>
 * BaseChronology is immutable, and all subclasses must be as well.
 */
abstract class BaseChronology extends Chronology {

    /**
     * Empty constructor
     */
    protected function __construct() {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function getDateTimeMillis($year, $monthOfYear, $dayOfMonth, $hourOfDay, $minuteOfHour, $secondOfMinute, $millisOfSecond) {
        $instant = $this->year()->set(0, $year);
        $instant = $this->monthOfYear()->set($instant, $monthOfYear);
        $instant = $this->dayOfMonth()->set($instant, $dayOfMonth);
        $instant = $this->hourOfDay()->set($instant, $hourOfDay);
        $instant = $this->minuteOfHour()->set($instant, $minuteOfHour);
        $instant = $this->secondOfMinute()->set($instant, $secondOfMinute);
        $instant = $this->millisOfSecond()->set($instant, $millisOfSecond);
        return $instant;
    }

    /**
     * @return DateTimeField
     */
    public function millisOfSecond() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::millisOfSecond());
    }

    /**
     * @return DateTimeField
     */
    public function millisOfDay() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::millisOfDay());
    }

    /**
     * @return DateTimeField
     */
    public function secondOfMinute() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::secondOfMinute());
    }

    /**
     * @return DateTimeField
     */
    public function secondOfDay() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::secondOfDay());
    }

    /**
     * @return DateTimeField
     */
    public function minuteOfHour() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::minuteOfHour());
    }

    /**
     * @return DateTimeField
     */
    public function minuteOfDay() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::minuteOfDay());
    }

    /**
     * @return DateTimeField
     */
    public function hourOfDay() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::hourOfDay());
    }

    /**
     * @return DateTimeField
     */
    public function clockhourOfDay() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::clockhourOfDay());
    }

    /**
     * @return DateTimeField
     */
    public function hourOfHalfday() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::hourOfHalfday());
    }

    /**
     * @return DateTimeField
     */
    public function clockhourOfHalfday() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::clockhourOfHalfday());
    }

    /**
     * @return DateTimeField
     */
    public function halfdayOfDay() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::halfdayOfDay());
    }

    /**
     * @return DateTimeField
     */
    public function dayOfWeek() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::dayOfWeek());
    }

    /**
     * @return DateTimeField
     */
    public function dayOfMonth() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::dayOfMonth());
    }

    /**
     * @return DateTimeField
     */
    public function dayOfYear() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::dayOfYear());
    }

    /**
     * @return DateTimeField
     */
    public function weekOfWeekyear() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::weekOfWeekyear());
    }

    /**
     * @return DateTimeField
     */
    public function weekyear() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::weekyear());
    }

    /**
     * @return DateTimeField
     */
    public function weekyearOfCentury() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::weekyearOfCentury());
    }

    /**
     * @return DateTimeField
     */
    public function monthOfYear() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::monthOfYear());
    }

    /**
     * @return DateTimeField
     */
    public function year() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::year() /* TODO , years() */);
    }

    /**
     * @return DateTimeField
     */
    public function yearOfEra() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::yearOfEra());
    }

    /**
     * @return DateTimeField
     */
    public function yearOfCentury() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::yearOfCentury());
    }

    /**
     * @return DateTimeField
     */
    public function centuryOfEra() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::centuryOfEra());
    }

    /**
     * @return DateTimeField
     */
    public function era() {
        return UnsupportedDateTimeField::getInstance(DateTimeFieldType::era());
    }
}