<?php

include_once dirname(__FILE__) . '/'  . '../field/decorated-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'gregorian-chronology.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';

/**
 * This field is not publicy exposed by ISOChronology, but rather it is used to
 * build the yearOfCentury and centuryOfEra fields. It merely drops the sign of
 * the year.
 */
class ISOYearOfEraDateTimeField extends DecoratedDateTimeField {

    private static $instance;

    public static function getInstance(){
        return self::$instance;
    }
    
    public static function staticInit() {
        self::$instance = new ISOYearOfEraDateTimeField();
    }

    /**
     * Internal constructor
     */
    public function __construct() {
        parent::__construct(GregorianChronology::getInstanceUTC()->year(), DateTimeFieldType::yearOfEra());
    }

    /**
     * @param int $millis
     * @return int
     */
    public function get($millis){
        $year = $this->getWrappedField()->get($millis);
        return $year < 0 ? -$year : $year;
    }
}
ISOYearOfEraDateTimeField::staticInit();
