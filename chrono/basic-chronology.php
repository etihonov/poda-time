<?php

include_once dirname(__FILE__) . '/'  . '../datetime-constants.php';
include_once dirname(__FILE__) . '/'  . 'assembled-chronology.php';

include_once dirname(__FILE__) . '/'  . 'basic-year-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'basic-day-of-year-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'basic-weekyear-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'basic-week-of-weekyear-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'gj-era-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'gj-month-of-year-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'gj-year-of-era-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'gj-day-of-week-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../field/basic-day-of-month-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../field/precise-duration-field.php';
include_once dirname(__FILE__) . '/'  . '../field/precise-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../field/millis-duration-field.php';
include_once dirname(__FILE__) . '/'  . '../field/offset-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../field/divided-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../field/remainder-datetime-field.php';

include_once dirname(__FILE__) . '/'  . '../core/utils.php';


/**
 * Abstract implementation for calendar systems that use a typical
 * day/month/year/leapYear model.
 * Most of the utility methods required by subclasses are package-private,
 * reflecting the intention that they be defined in the same package.
 * <p>
 * BasicChronology immutable, and all subclasses must
 * be as well.
 */
abstract class BasicChronology extends AssembledChronology {

    private static $millisField;
    private static $secondsField;
    private static $minutesField;
    private static $hoursField;
    private static $halfdaysField;
    private static $daysField;
    private static $weeksField;

    private static $millisOfSecondField;
    private static $millisOfDayField;
    private static $secondOfMinuteField;
    private static $secondOfDayField;
    private static $minuteOfHourField;
    private static $minuteOfDayField;
    private static $hourOfDayField;
    private static $hourOfHalfdayField;
    private static $clockhourOfDayField;
    private static $clockhourOfHalfdayField;
    private static $halfdayOfDayField;

    public static function staticInit() {
        self::$millisField = MillisDurationField::getInstance();

        self::$secondsField = new PreciseDurationField(DurationFieldType::seconds(), DateTimeConstants::$MILLIS_PER_SECOND);
        self::$minutesField = new PreciseDurationField(DurationFieldType::minutes(), DateTimeConstants::$MILLIS_PER_MINUTE);
        self::$hoursField = new PreciseDurationField(DurationFieldType::hours(), DateTimeConstants::$MILLIS_PER_HOUR);
        self::$halfdaysField = new PreciseDurationField(DurationFieldType::halfdays(), DateTimeConstants::$MILLIS_PER_DAY / 2);
        self::$daysField = new PreciseDurationField(DurationFieldType::days(), DateTimeConstants::$MILLIS_PER_DAY);
        self::$weeksField = new PreciseDurationField(DurationFieldType::weeks(), DateTimeConstants::$MILLIS_PER_WEEK);


        self::$millisOfSecondField = new PreciseDateTimeField(DateTimeFieldType::millisOfSecond(), self::$millisField, self::$secondsField);
        self::$millisOfDayField = new PreciseDateTimeField(DateTimeFieldType::millisOfDay(), self::$millisField, self::$daysField);
        self::$secondOfMinuteField = new PreciseDateTimeField(DateTimeFieldType::secondOfMinute(), self::$secondsField, self::$minutesField);
        self::$secondOfDayField = new PreciseDateTimeField(DateTimeFieldType::secondOfDay(), self::$secondsField, self::$daysField);
        self::$minuteOfHourField = new PreciseDateTimeField(DateTimeFieldType::minuteOfHour(), self::$minutesField, self::$hoursField);
        self::$minuteOfDayField = new PreciseDateTimeField(DateTimeFieldType::minuteOfDay(), self::$minutesField, self::$daysField);
        self::$hourOfDayField = new PreciseDateTimeField(DateTimeFieldType::hourOfDay(), self::$hoursField, self::$daysField);
        self::$hourOfHalfdayField = new PreciseDateTimeField(DateTimeFieldType::hourOfHalfday(), self::$hoursField, self::$halfdaysField);
        //self::$clockhourOfDayField = new ZeroIsMaxDateTimeField(self::$hourOfDayField, DateTimeFieldType::clockhourOfDay());
        //self::$clockhourOfHalfdayField = new ZeroIsMaxDateTimeField(self::$hourOfHalfdayField, DateTimeFieldType::clockhourOfHalfday());

    }

    /**
     * @param Chronology $base
     * @param mixed $param
     * @param int $minDaysInFirstWeek
     * @throws IllegalArgumentException
     */
    public function __construct(Chronology $base = null, $param, $minDaysInFirstWeek) {
        parent::__construct($base, $param);

        if ($minDaysInFirstWeek < 1 || $minDaysInFirstWeek > 7) {
            throw new IllegalArgumentException("Invalid min days in first week: " + $minDaysInFirstWeek);
        }

        $this->minDaysInFirstWeek = $minDaysInFirstWeek;
    }

    /**
     * @return PTDateTimeZone
     */
    public function getZone() {
        $base = $this->getBase();
        if ($base != null) {
            return $base->getZone();
        }
        return PTDateTimeZone::$UTC;
    }


    /**
     * @param Fields $fields
     */
    protected function assemble(Fields $fields) {

        $fields->days = self::$daysField;
        $fields->weeks = self::$weeksField;

        $fields->millisOfSecond = self::$millisOfSecondField;
        $fields->millisOfDay = self::$millisOfDayField;
        $fields->secondOfMinute = self::$secondOfMinuteField;
        $fields->secondOfDay = self::$secondOfDayField;
        $fields->minuteOfHour = self::$minuteOfHourField;
        $fields->minuteOfDay = self::$minuteOfDayField;
        $fields->hourOfDay = self::$hourOfDayField;
        $fields->hourOfHalfday = self::$hourOfHalfdayField;
        $fields->clockhourOfDay = self::$clockhourOfDayField;
        $fields->clockhourOfHalfday = self::$clockhourOfHalfdayField;
        $fields->halfdayOfDay = self::$halfdayOfDayField;


        $fields->year = new BasicYearDateTimeField($this);
        $fields->yearOfEra = new GJYearOfEraDateTimeField($fields->year, $this);

        $field = new OffsetDateTimeField(
            $fields->yearOfEra,
            DateTimeFieldType::yearOfEra(),
            99,
            PTInteger::minValue(),
            PTInteger::maxValue()
        );
        $fields->centuryOfEra = new DividedDateTimeField($field, DateTimeFieldType::centuryOfEra(), 100);

        $field = RemainderDateTimeField::createFromDividedField($fields->centuryOfEra);
        $fields->yearOfCentury = new OffsetDateTimeField(
            $field,
            DateTimeFieldType::yearOfCentury(),
            1,
            PTInteger::minValue(),
            PTInteger::maxValue()
        );

        $fields->era = new GJEraDateTimeField($this);
        $fields->dayOfWeek = new GJDayOfWeekDateTimeField($this, $fields->days);
        $fields->dayOfMonth = new BasicDayOfMonthDateTimeField($this, $fields->days);
        $fields->dayOfYear = new BasicDayOfYearDateTimeField($this, $fields->days);
        $fields->monthOfYear = new GJMonthOfYearDateTimeField($this);
        $fields->weekyear = new BasicWeekyearDateTimeField($this);
        $fields->weekOfWeekyear = new BasicWeekOfWeekyearDateTimeField($this, $fields->weeks);


        //$field = new RemainderDateTimeField(fields.weekyear, DateTimeFieldType.weekyearOfCentury(), 100);
        //fields->weekyearOfCentury = new OffsetDateTimeField($field, DateTimeFieldType::weekyearOfCentury(), 1);

    }

    /**
     * Gets the number of days in the specified month and year.
     *
     * @param int $year  the year
     * @param int $month  the month
     * @return int the number of days
     */
    public abstract function getDaysInYearMonth($year, $month);

    public abstract function getAverageMillisPerYearDividedByTwo();

    public abstract function getApproxMillisAtEpochDividedByTwo();

    public abstract function calculateFirstDayOfYearMillis($year);

    public function getYearMillis($year) {
        return $this->calculateFirstDayOfYearMillis($year);
    }

    /**
     * @param int $instant millis from 1970-01-01T00:00:00Z
     * @return int
     */
    public function getYear($instant) {
        // Get an initial estimate of the year, and the millis value that
        // represents the start of that year. Then verify estimate and fix if
        // necessary.

        // Initial estimate uses values divided by two to avoid overflow.
        $unitMillis = $this->getAverageMillisPerYearDividedByTwo();


        $i2 = ($instant / 2) + $this->getApproxMillisAtEpochDividedByTwo();

        if ($i2 < 0) {
            $i2 = $i2 - $unitMillis + 1;
        }
        $year = (int)($i2 / $unitMillis);

        $yearStart = $this->getYearMillis($year);
        $diff = $instant - $yearStart;

        if ($diff < 0) {
            $year--;
        } else if ($diff >= DateTimeConstants::$MILLIS_PER_DAY * 365) {
            // One year may need to be added to fix estimate.
            $oneYear = null;

            if ($this->isLeapYear($year)) {
                $oneYear = DateTimeConstants::$MILLIS_PER_DAY * 366;
            } else {
                $oneYear = DateTimeConstants::$MILLIS_PER_DAY * 365;
            }

            $yearStart += $oneYear;

            if ($yearStart <= $instant) {
                // Didn't go too far, so actually add one year.
                $year++;
            }
        }

        return $year;
    }

    public abstract function isLeapYear($year);

    /**
     * @param int $instant
     * @return int
     */
    public function getMonthOfYear($instant) {
        return $this->getMonthOfYear_WithYear($instant, $this->getYear($instant));
    }

    /**
     * @param int $millis from 1970-01-01T00:00:00Z
     * @return int
     */
    public function getDayOfMonth($millis) {
        $year = $this->getYear($millis);
        $month = $this->getMonthOfYear_WithYear($millis, $year);
        return $this->getDayOfMonth_WithYearAdnMonth($millis, $year, $month);
    }

    /**
     * @param int $millis from 1970-01-01T00:00:00Z
     * @param int $year precalculated year of millis
     * @param int $month precalculated month of millis
     * @return int
     */
    public function getDayOfMonth_WithYearAdnMonth($millis, $year, $month) {
        $dateMillis = $this->getYearMillis($year);
        $dateMillis += $this->getTotalMillisByYearMonth($year, $month);
        return (int)(($millis - $dateMillis) / DateTimeConstants::$MILLIS_PER_DAY) + 1;
    }

    /**
     * Gets the total number of millis elapsed in this year at the start
     * of the specified month, such as zero for month 1.
     *
     * @param int $year  the year
     * @param int $month  the month
     * @return int  the elapsed millis at the start of the month
     */
    abstract function getTotalMillisByYearMonth($year, $month);

    /**
     * @param int $millis
     * @param int $year
     * @return int
     */
    abstract function getMonthOfYear_WithYear($millis, $year);

    /**
     * @param int $instant millis from 1970-01-01T00:00:00Z
     * @return int
     */
    public function getDayOfWeek($instant) {
        // 1970-01-01 is day of week 4, Thursday.

        /** @var $daysSince19700101 int */
        $daysSince19700101 = 0;

        if ($instant >= 0) {
            $daysSince19700101 = intval(bcdiv($instant, DateTimeConstants::$MILLIS_PER_DAY, 0));
        } else {
            $daysSince19700101 = intval(($instant - (DateTimeConstants::$MILLIS_PER_DAY - 1)) / DateTimeConstants::$MILLIS_PER_DAY);
            if ($daysSince19700101 < -3) {
                return 7 + intval(bcmod(($daysSince19700101 + 4), 7));
            }
        }

        return 1 + intval(bcmod(($daysSince19700101 + 3), 7));
    }

    /**
     * @param int $millis
     * @return int
     */
    public function getDayOfYear($millis) {
        return $this->getDayOfYear_WithYear($millis, $this->getYear($millis));
    }

    /**
     * @param int $millis
     * @param int $year precalculated year of millis
     * @return int
     */
    private function getDayOfYear_WithYear($millis, $year) {
        $yearStart = $this->getYearMillis($year);
        return (int)(($millis - $yearStart) / DateTimeConstants::$MILLIS_PER_DAY) + 1;
    }

    /**
     * Gets an average value for the milliseconds per year.
     *
     * @return int the millis per year
     */
    public abstract function getAverageMillisPerYear();

    /**
     * @param int $millis
     * @return int
     */
    public function getWeekyear($millis) {
        $year = $this->getYear($millis);
        $week = $this->getWeekOfWeekyear_WithYear($millis, $year);
        if ($week == 1) {
            return $this->getYear($millis + DateTimeConstants::$MILLIS_PER_WEEK);
        } else if ($week > 51) {
            return $this->getYear($millis - (2 * DateTimeConstants::$MILLIS_PER_WEEK));
        } else {
            return $year;
        }
    }

    /**
     * @param int $millis
     * @param int $year precalculated year of millis
     * @return int
     */
    private function getWeekOfWeekyear_WithYear($millis, $year) {
        $firstWeekMillis1 = $this->getFirstWeekOfYearMillis($year);
        if ($millis < $firstWeekMillis1) {
            return $this->getWeeksInYear($year - 1);
        }
        $firstWeekMillis2 = $this->getFirstWeekOfYearMillis($year + 1);
        if ($millis >= $firstWeekMillis2) {
            return 1;
        }
        return (int)(($millis - $firstWeekMillis1) / DateTimeConstants::$MILLIS_PER_WEEK) + 1;
    }

    /**
     * Get the millis for the first week of a year.
     *
     * @param int $year  the year to use
     * @return int
     */
    private function getFirstWeekOfYearMillis($year) {
        $jan1millis = $this->getYearMillis($year);
        $jan1dayOfWeek = $this->getDayOfWeek($jan1millis);

        if ($jan1dayOfWeek > (8 - $this->minDaysInFirstWeek)) {
            // First week is end of previous year because it doesn't have enough days.
            return $jan1millis + (8 - $jan1dayOfWeek) * DateTimeConstants::$MILLIS_PER_DAY;
        } else {
            // First week is start of this year because it has enough days.
            return $jan1millis - ($jan1dayOfWeek - 1) * DateTimeConstants::$MILLIS_PER_DAY;
        }
    }

    /**
     * Get the number of weeks in the year.
     *
     * @param int $year  the year to use
     * @return int number of weeks in the year
     */
    public function getWeeksInYear($year) {
        $firstWeekMillis1 = $this->getFirstWeekOfYearMillis($year);
        $firstWeekMillis2 = $this->getFirstWeekOfYearMillis($year + 1);
        return (int)(($firstWeekMillis2 - $firstWeekMillis1) / DateTimeConstants::$MILLIS_PER_WEEK);
    }

    /**
     * @param int $instant
     * @return int
     */
    public function getWeekOfWeekyear($instant) {
        return $this->getWeekOfWeekyear_WithYear($instant, $this->getYear($instant));
    }

    /**
     * Gets the minimum supported year.
     *
     * @return int the year
     */
    public abstract function getMinYear();

    /**
     * Gets the maximum supported year.
     *
     * @return int the year
     */
    public abstract function getMaxYear();

    /**
     * Sets the year from an instant and year.
     *
     * @param int $instant  millis from 1970-01-01T00:00:00Z
     * @param int $year  the year to set
     * @return int the updated millis
     */
    public abstract function setYear($instant, $year);

    /**
     * Get the milliseconds for a particular date.
     *
     * @param int $year The year to use.
     * @param int $month The month to use
     * @param int $dayOfMonth The day of the month to use
     * @return int millis from 1970-01-01T00:00:00Z
     */
    public function getYearMonthDayMillis($year, $month, $dayOfMonth) {
        $millis = $this->getYearMillis($year);
        $millis += $this->getTotalMillisByYearMonth($year, $month);
        return $millis + bcmul(bcsub($dayOfMonth, 1), DateTimeConstants::$MILLIS_PER_DAY);
    }

    /**
     * @param int $instant millis from 1970-01-01T00:00:00Z
     * @return int
     */
    public function getMillisOfDay($instant) {
        if ($instant >= 0) {
            return intval((bcmod($instant, DateTimeConstants::$MILLIS_PER_DAY)));
        } else {
            return bcadd((DateTimeConstants::$MILLIS_PER_DAY - 1),
                intval(bcmod( bcadd($instant, 1), DateTimeConstants::$MILLIS_PER_DAY)));
        }
    }

    /**
     * Gets the maximum number of months.
     * @return int 12
     */
    public function getMaxMonth() {
        return 12;
    }

}

BasicChronology::staticInit();