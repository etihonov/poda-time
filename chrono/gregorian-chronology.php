<?php

include_once dirname(__FILE__) . '/'  . 'basic-gj-chronology.php';
include_once dirname(__FILE__) . '/'  . '../datetime-constants.php';

/**
 * Implements a pure proleptic Gregorian calendar system, which defines every
 * fourth year as leap, unless the year is divisible by 100 and not by 400.
 * This improves upon the Julian calendar leap year rule.
 * <p>
 * Although the Gregorian calendar did not exist before 1582 CE, this
 * chronology assumes it did, thus it is proleptic. This implementation also
 * fixes the start of the year at January 1, and defines the year zero.
 * <p>
 * GregorianChronology is immutable.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Gregorian_calendar">Wikipedia</a>
 * @see JulianChronology
 * @see GJChronology
 */
class GregorianChronology extends BasicGJChronology {

    private static $MILLIS_PER_YEAR;
    private static $DAYS_0000_TO_1970;

    private static $INSTANCE_UTC;

    /** The lowest year that can be fully supported. */
    private static $MIN_YEAR = -292275054;

    /** The highest year that can be fully supported. */
    private static $MAX_YEAR = 292278993;

    public static function staticInit() {
        self::$DAYS_0000_TO_1970 = 719527;

        self::$MILLIS_PER_YEAR =
            (365.2425 * DateTimeConstants::$MILLIS_PER_DAY);

        self::$INSTANCE_UTC = self::getInstance(PTDateTimeZone::$UTC);
    }

    /**
     * Gets an instance of the GregorianChronology in the given time zone.
     *
     * @param PTDateTimeZone $zone  the time zone to get the chronology in, null is default
     * @param int $minDaysInFirstWeek  minimum number of days in first week of the year; default is 4
     * @return GregorianChronology a chronology in the specified time zone
     */
    public static function getInstance(PTDateTimeZone $zone = null, $minDaysInFirstWeek = 4) {
        if ($zone == null) {
            $zone = PTDateTimeZone::getDefault();
        }

        $chrono = null;
        if ($zone == PTDateTimeZone::$UTC) {
            $chrono = new GregorianChronology(null, null, $minDaysInFirstWeek);
        } else {
            $chrono = self::getInstance(PTDateTimeZone::$UTC, $minDaysInFirstWeek);
            $chrono = new GregorianChronology
            (ZonedChronology::getInstance($chrono, $zone), null, $minDaysInFirstWeek);
        }
        return $chrono;
    }

    /**
     * Gets an instance of the GregorianChronology.
     * The time zone of the returned instance is UTC.
     *
     * @return GregorianChronology a singleton UTC instance of the chronology
     */
    public static function getInstanceUTC() {
        return self::$INSTANCE_UTC;
    }

    /**
     * @param Chronology $base
     * @param mixed $param
     * @param int $minDaysInFirstWeek
     */
    public function __construct(Chronology $base = null, $param, $minDaysInFirstWeek) {
        parent::__construct($base, $param, $minDaysInFirstWeek);
    }

    public function getAverageMillisPerYearDividedByTwo() {
        return bcdiv(self::$MILLIS_PER_YEAR, 2, 0);
    }

    public function getApproxMillisAtEpochDividedByTwo() {
        return bcdiv((1970 * self::$MILLIS_PER_YEAR), 2, 0);
    }

    public function calculateFirstDayOfYearMillis($year) {
        // Initial value is just temporary.
        $leapYears = intval($year / 100);

        if ($year < 0) {
            // Add 3 before shifting right since /4 and >>2 behave differently
            // on negative numbers. When the expression is written as
            // (year / 4) - (year / 100) + (year / 400),
            // it works for both positive and negative values, except this optimization
            // eliminates two divisions.
            $leapYears = intval(bcdiv($year, 4, 0) - $leapYears + bcdiv($leapYears, 4, 0));
        } else {
            $leapYears = intval(bcdiv($year, 4, 0) - $leapYears + bcdiv($leapYears, 4, 0));
            if ($this->isLeapYear($year)) {
                $leapYears--;
            }
        }

        return ($year * 365 + ($leapYears - self::$DAYS_0000_TO_1970)) * DateTimeConstants::$MILLIS_PER_DAY;

    }

    public function isLeapYear($year) {
        return (($year & 3) == 0) && (($year % 100) != 0 || ($year % 400) == 0);
    }

    /**
     * Returns an instance of this Chronology that operates in the UTC time
     * zone. Chronologies that do not operate in a time zone or are already
     * UTC must return themself.
     *
     * @return Chronology a version of this chronology that ignores time zones
     */
    public function withUTC() {
        return self::$INSTANCE_UTC;
    }

    /**
     * Returns an instance of this Chronology that operates in any time zone.
     *
     * @return Chronology a version of this chronology with a specific time zone
     * @param PTDateTimeZone $zone to use, or default if null
     * @see ZonedChronology
     */
    public function withZone(PTDateTimeZone $zone = null) {
        if ($zone == null) {
            $zone = PTDateTimeZone::getDefault();
        }
        if ($zone == $this->getZone()) {
            return $this;
        }
        return self::getInstance($zone);
    }

    /**
     * Gets an average value for the milliseconds per year.
     *
     * @return int the millis per year
     */
    public function getAverageMillisPerYear() {
        return self::$MILLIS_PER_YEAR;
    }

    public function getMinYear() {
        return self::$MIN_YEAR;
    }

    public function getMaxYear() {
        return self::$MAX_YEAR;
    }

}

GregorianChronology::staticInit();