<?php

/**
 * Wraps another Chronology to add support for time zones.
 * <p>
 * ZonedChronology is immutable.
 */
final class ZonedChronology extends AssembledChronology {

    /**
     * @param Chronology $base
     * @param \PTDateTimeZone $zone
     */
    public function __construct(Chronology $base, PTDateTimeZone $zone) {
        parent::__construct($base, $zone);
    }

    /**
     * @return PTDateTimeZone
     */
    public function getZone() {
        return $this->getParam();
    }


    /**
     * Returns an instance of this Chronology that operates in the UTC time
     * zone. Chronologies that do not operate in a time zone or are already
     * UTC must return themself.
     *
     * @return Chronology a version of this chronology that ignores time zones
     */
    public function withUTC() {
        return $this->getBase();
    }

    /**
     * Returns an instance of this Chronology that operates in any time zone.
     *
     * @return Chronology a version of this chronology with a specific time zone
     * @param PTDateTimeZone $zone to use, or default if null
     * @see ZonedChronology
     */
    public function withZone(PTDateTimeZone $zone = null) {
        if ($zone == null) {
            $zone = PTDateTimeZone::getDefault();
        }
        if ($zone == $this->getParam()) {
            return $this;
        }
        if ($zone == PTDateTimeZone::$UTC) {
            return $this->getBase();
        }
        return new ZonedChronology($this->getBase(), $zone);
    }

    /**
     * Invoked by the constructor and after deserialization to allow subclasses
     * to define all of its supported fields. All unset fields default to
     * unsupported instances.
     *
     * @param Fields $fields container of fields
     */
    protected function assemble(Fields $fields) {
        $converted = null;

        $fields->year = $this->convertDateTimeField($fields->year, $converted);
        $fields->yearOfEra = $this->convertDateTimeField($fields->yearOfEra, $converted);
        $fields->yearOfCentury = $this->convertDateTimeField($fields->yearOfCentury, $converted);
        $fields->centuryOfEra = $this->convertDateTimeField($fields->centuryOfEra, $converted);
        $fields->era = $this->convertDateTimeField($fields->era, $converted);
        $fields->dayOfWeek = $this->convertDateTimeField($fields->dayOfWeek, $converted);
        $fields->dayOfMonth = $this->convertDateTimeField($fields->dayOfMonth, $converted);
        $fields->dayOfYear = $this->convertDateTimeField($fields->dayOfYear, $converted);
        $fields->monthOfYear = $this->convertDateTimeField($fields->monthOfYear, $converted);
        $fields->weekOfWeekyear = $this->convertDateTimeField($fields->weekOfWeekyear, $converted);
        $fields->weekyear = $this->convertDateTimeField($fields->weekyear, $converted);
        $fields->weekyearOfCentury = $this->convertDateTimeField($fields->weekyearOfCentury, $converted);

        $fields->millisOfSecond = $this->convertDateTimeField($fields->millisOfSecond, $converted);
        $fields->millisOfDay = $this->convertDateTimeField($fields->millisOfDay, $converted);
        $fields->secondOfMinute = $this->convertDateTimeField($fields->secondOfMinute, $converted);
        $fields->secondOfDay = $this->convertDateTimeField($fields->secondOfDay, $converted);
        $fields->minuteOfHour = $this->convertDateTimeField($fields->minuteOfHour, $converted);
        $fields->minuteOfDay = $this->convertDateTimeField($fields->minuteOfDay, $converted);
        $fields->hourOfDay = $this->convertDateTimeField($fields->hourOfDay, $converted);
        $fields->hourOfHalfday = $this->convertDateTimeField($fields->hourOfHalfday, $converted);
        $fields->clockhourOfDay = $this->convertDateTimeField($fields->clockhourOfDay, $converted);
        $fields->clockhourOfHalfday = $this->convertDateTimeField($fields->clockhourOfHalfday, $converted);
        $fields->halfdayOfDay = $this->convertDateTimeField($fields->halfdayOfDay, $converted);
    }

    /**
     * @param DateTimeField $field
     * @param $converted
     * @return DateTimeField
     */
    private function convertDateTimeField(DateTimeField $field, $converted) {
        if ($field == null /* TODO || !$field->isSupported() */) {
            return $field;
        }
        /*TODO if (converted.containsKey(field)) {
            return (DateTimeField)converted.get(field);
        }*/
        $zonedField =
            new ZonedDateTimeField($field, $this->getZone()
            /*  TODO
                , $this->convertField($field->getDurationField(), $converted),
                $this->convertField($field->getRangeDurationField(), $converted),
                $this->convertField($field->getLeapDurationField(), $converted)*/
            );
        /*$converted->put(field, zonedField);*/
        return $zonedField;
    }

    /**
     * @param Chronology $base
     * @param \PTDateTimeZone $zone
     * @throws IllegalArgumentException
     * @return ZonedChronology
     */
    public static function getInstance(Chronology $base, PTDateTimeZone $zone) {
        if ($base == null) {
            throw new IllegalArgumentException("Must supply a chronology");
        }
        $base = $base->withUTC();
        if ($base == null) {
            throw new IllegalArgumentException("UTC chronology must not be null");
        }
        if ($zone == null) {
            throw new IllegalArgumentException("DateTimeZone must not be null");
        }
        return new ZonedChronology($base, $zone);
    }


    /**
     * {@inheritdoc}
     */
    public function equals($object) {
        if ($this === $object) {
            return true;
        }
        if ($object instanceof ZonedChronology == false) {
            return false;
        }
        /** @var ZonedChronology $chrono  */
        $chrono = $object;
        return
            $this->getBase()->equals($chrono->getBase()) &&
            $this->getZone()->equals($chrono->getZone());
    }
}

/**
 * A DateTimeField that decorates another to add timezone behaviour.
 * <p>
 * This class converts passed in instants to local wall time, and vice
 * versa on output.
 */
final class ZonedDateTimeField extends BaseDateTimeField {

    /** @var \DateTimeField */
    private $field;

    /** @var \PTDateTimeZone */
    private $zone;

    /**
     * @param DateTimeField $field
     * @param \PTDateTimeZone $zone
     */
    public function __construct(DateTimeField $field,
                                PTDateTimeZone $zone
        /* TODO , DurationField $durationField,
        DurationField $rangeDurationField,
        DurationField $leapDurationField*/) {

        parent::__construct($field->getType());
        /* TODO if (!field.isSupported()) {
            throw new IllegalArgumentException();
        }*/
        $this->field = $field;
        $this->zone = $zone;
        /* TODO
        $this->durationField = $durationField;
        $this->timeField = $this->useTimeArithmetic($durationField);
        $this->rangeDurationField = $rangeDurationField;
        $this->leapDurationField = $leapDurationField;
        */

    }

    /**
     * @param int $instant
     * @return int
     */
    public function get($instant) {
        $localInstant = $this->zone->convertUTCToLocal($instant);
        return $this->field->get($localInstant);
    }

    /**
     * @param int $fieldValue
     * @param PTLocale $locale
     * @return string
     */
    public function getAsTextFromValue($fieldValue, PTLocale $locale = null) {
        return $this->field->getAsTextFromValue($fieldValue, $locale);
    }

    public function getAsShortTextFromValue($fieldValue, PTLocale $locale = null) {
        return $this->field->getAsShortTextFromValue($fieldValue, $locale);
    }

}