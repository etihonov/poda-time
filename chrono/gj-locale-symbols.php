<?php

include_once dirname(__FILE__) . '/'  . '../locale.php';
include_once dirname(__FILE__) . '/'  . '../core/containers.php';

/**
 * Utility class used by a few of the GJDateTimeFields.
 */
class GJLocaleSymbols {

    /** @var IMap */
    private static $cache;

    public static function staticInit() {
        self::$cache = new Map();
    }

    /**
     * @param PTLocale $locale
     * @return \GJLocaleSymbols
     */
    public static function forLocale(PTLocale $locale = null) {
        if ($locale == null) {
            $locale = PTLocale::getDefault();
        }
        $result = self::$cache->get($locale);
        if ($result == null) {
            $result = new GJLocaleSymbols($locale);
            self::$cache->put($locale, $result);
        }
        return $result;
    }

    /** @var \PTLocale */
    private $locale;

    /** @var string[] */
    private $shortDaysOfWeek;

    /**
     * @param PTLocale $locale
     */
    public function __construct(PTLocale $locale) {
        $this->locale = $locale;
        $this->shortDaysOfWeek = $this->realignDaysOfWeek($this->locale->getShortWeekdays());
    }

    /**
     * @param int $value
     * @return string
     */
    public function dayOfWeekValueToShortText($value) {
        return $this->shortDaysOfWeek[$value];
    }

    /**
     * @param string[] $daysOfWeek
     * @return string[]
     */
    private function realignDaysOfWeek($daysOfWeek) {
        $a = array();
        for ($i = 1; $i < 8; $i++) {
            $a[$i] = $daysOfWeek[$i - 1];
        }
        return $a;
    }
}

GJLocaleSymbols::staticInit();