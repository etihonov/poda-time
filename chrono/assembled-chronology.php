<?php

include_once dirname(__FILE__) . '/'  . '../datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'base-chronology.php';

/**
 * A container of fields used for assembling a chronology.
 */
class Fields {
    /** @var DateTimeField */
    public $millisOfSecond;
    /** @var DateTimeField */
    public $millisOfDay;
    /** @var DateTimeField */
    public $secondOfMinute;
    /** @var DateTimeField */
    public $secondOfDay;
    /** @var DateTimeField */
    public $minuteOfHour;
    /** @var DateTimeField */
    public $minuteOfDay;
    /** @var DateTimeField */
    public $hourOfDay;
    /** @var DateTimeField */
    public $clockhourOfDay;
    /** @var DateTimeField */
    public $hourOfHalfday;
    /** @var DateTimeField */
    public $clockhourOfHalfday;
    /** @var DateTimeField */
    public $halfdayOfDay;
    /** @var DateTimeField */
    public $dayOfWeek;
    /** @var DateTimeField */
    public $dayOfMonth;
    /** @var DateTimeField */
    public $dayOfYear;
    /** @var DateTimeField */
    public $weekOfWeekyear;
    /** @var DateTimeField */
    public $weekyear;
    /** @var DateTimeField */
    public $weekyearOfCentury;
    /** @var DateTimeField */
    public $monthOfYear;
    /** @var DateTimeField */
    public $year;
    /** @var DateTimeField */
    public $yearOfEra;
    /** @var DateTimeField */
    public $yearOfCentury;
    /** @var DateTimeField */
    public $centuryOfEra;
    /** @var DateTimeField */
    public $era;

    /**
     * Copy the supported fields from a chronology into this container.
     * @param Chronology $chrono
     */
    public function copyFieldsFrom(Chronology $chrono) {
        $this->millisOfSecond              = $this->isDateTimeFieldSupported($chrono->millisOfSecond     ()) ?         $chrono->millisOfSecond     () :     $this->millisOfSecond    ;
        $this->millisOfDay                 = $this->isDateTimeFieldSupported($chrono->millisOfDay        ()) ?         $chrono->millisOfDay        () :     $this->millisOfDay       ;
        $this->secondOfMinute              = $this->isDateTimeFieldSupported($chrono->secondOfMinute     ()) ?         $chrono->secondOfMinute     () :     $this->secondOfMinute    ;
        $this->secondOfDay                 = $this->isDateTimeFieldSupported($chrono->secondOfDay        ()) ?         $chrono->secondOfDay        () :     $this->secondOfDay       ;
        $this->minuteOfHour                = $this->isDateTimeFieldSupported($chrono->minuteOfHour       ()) ?         $chrono->minuteOfHour       () :     $this->minuteOfHour      ;
        $this->minuteOfDay                 = $this->isDateTimeFieldSupported($chrono->minuteOfDay        ()) ?         $chrono->minuteOfDay        () :     $this->minuteOfDay       ;
        $this->hourOfDay                   = $this->isDateTimeFieldSupported($chrono->hourOfDay          ()) ?         $chrono->hourOfDay          () :     $this->hourOfDay         ;
        $this->clockhourOfDay              = $this->isDateTimeFieldSupported($chrono->clockhourOfDay     ()) ?         $chrono->clockhourOfDay     () :     $this->clockhourOfDay    ;
        $this->hourOfHalfday               = $this->isDateTimeFieldSupported($chrono->hourOfHalfday      ()) ?         $chrono->hourOfHalfday      () :     $this->hourOfHalfday     ;
        $this->clockhourOfHalfday          = $this->isDateTimeFieldSupported($chrono->clockhourOfHalfday ()) ?         $chrono->clockhourOfHalfday () :     $this->clockhourOfHalfday;
        $this->halfdayOfDay                = $this->isDateTimeFieldSupported($chrono->halfdayOfDay       ()) ?         $chrono->halfdayOfDay       () :     $this->halfdayOfDay      ;
        $this->dayOfWeek                   = $this->isDateTimeFieldSupported($chrono->dayOfWeek          ()) ?         $chrono->dayOfWeek          () :     $this->dayOfWeek         ;
        $this->dayOfMonth                  = $this->isDateTimeFieldSupported($chrono->dayOfMonth         ()) ?         $chrono->dayOfMonth         () :     $this->dayOfMonth        ;
        $this->dayOfYear                   = $this->isDateTimeFieldSupported($chrono->dayOfYear          ()) ?         $chrono->dayOfYear          () :     $this->dayOfYear         ;
        $this->weekOfWeekyear              = $this->isDateTimeFieldSupported($chrono->weekOfWeekyear     ()) ?         $chrono->weekOfWeekyear     () :     $this->weekOfWeekyear    ;
        $this->weekyear                    = $this->isDateTimeFieldSupported($chrono->weekyear           ()) ?         $chrono->weekyear           () :     $this->weekyear          ;
        $this->weekyearOfCentury           = $this->isDateTimeFieldSupported($chrono->weekyearOfCentury  ()) ?         $chrono->weekyearOfCentury  () :     $this->weekyearOfCentury ;
        $this->monthOfYear                 = $this->isDateTimeFieldSupported($chrono->monthOfYear        ()) ?         $chrono->monthOfYear        () :     $this->monthOfYear       ;
        $this->year                        = $this->isDateTimeFieldSupported($chrono->year               ()) ?         $chrono->year               () :     $this->year              ;
        $this->yearOfEra                   = $this->isDateTimeFieldSupported($chrono->yearOfEra          ()) ?         $chrono->yearOfEra          () :     $this->yearOfEra         ;
        $this->yearOfCentury               = $this->isDateTimeFieldSupported($chrono->yearOfCentury      ()) ?         $chrono->yearOfCentury      () :     $this->yearOfCentury     ;
        $this->centuryOfEra                = $this->isDateTimeFieldSupported($chrono->centuryOfEra       ()) ?         $chrono->centuryOfEra       () :     $this->centuryOfEra      ;
        $this->era                         = $this->isDateTimeFieldSupported($chrono->era                ()) ?         $chrono->era                () :     $this->era               ;
    }

    /**
     * @param DateTimeField $field
     * @return bool
     */
    private function isDateTimeFieldSupported(DateTimeField $field) {
        return true;
    }
}

/**
 * Abstract Chronology that enables chronologies to be assembled from
 * a container of fields.
 */
abstract class AssembledChronology extends BaseChronology {

    #region DateTime Fields

    /** @var DateTimeField */
    private $millisOfSecond;

    /** @var DateTimeField */
    private $millisOfDay;

    /** @var DateTimeField */
    private $secondOfMinute;

    /** @var DateTimeField */
    private $secondOfDay;

    /** @var DateTimeField */
    private $minuteOfHour;

    /** @var DateTimeField */
    private $minuteOfDay;

    /** @var DateTimeField */
    private $hourOfDay;

    /** @var DateTimeField */
    private $clockhourOfDay;

    /** @var DateTimeField */
    private $hourOfHalfday;

    /** @var DateTimeField */
    private $clockhourOfHalfday;

    /** @var DateTimeField */
    private $halfdayOfDay;

    /** @var DateTimeField */
    private $dayOfWeek;

    /** @var DateTimeField */
    private $dayOfMonth;

    /** @var DateTimeField */
    private $dayOfYear;

    /** @var DateTimeField */
    private $weekOfWeekyear;

    /** @var DateTimeField */
    private $weekyear;

    /** @var DateTimeField */
    private $weekyearOfCentury;

    /** @var DateTimeField */
    private $monthOfYear;

    /** @var DateTimeField */
    private $year;

    /** @var DateTimeField */
    private $yearOfEra;

    /** @var DateTimeField */
    private $yearOfCentury;

    /** @var DateTimeField */
    private $centuryOfEra;

    /** @var DateTimeField */
    private $era;

    #endregion

    /**
     * Constructor calls the assemble method, enabling subclasses to define its
     * supported fields. If a base chronology is supplied, the field set
     * initially contains references to each base chronology field.
     * <p>
     * Other methods in this class will delegate to the base chronology, if it
     * can be determined that the base chronology will produce the same results
     * as AbstractChronology.
     *
     * @param Chronology $base optional base chronology to copy initial fields from
     * @param mixed $param optional param object available for assemble method
     */
    protected function __construct(Chronology $base = null, $param) {
        parent::__construct();
        $this->base = $base;
        $this->param = $param;
        $this->setFields();
    }

    /**
     * Returns the same base chronology as passed into the constructor.
     * @return Chronology
     */
    protected final function getBase() {
        return $this->base;
    }

    /**
     * Returns the same param object as passed into the constructor.
     * @return mixed
     */
    protected final function getParam() {
        return $this->param;
    }



/**
     * @return DateTimeField
     */
    public final function millisOfSecond() {
        return $this->millisOfSecond;
    }

    /**
     * Returns the PTDateTimeZone that this Chronology operates in, or null if
     * unspecified.
     *
     * @return PTDateTimeZone the PTDateTimeZone, null if unspecified
     */
    public function getZone() {
        $base = $this->base;
        if ($base != null) {
            return $base->getZone();
        }
        return null;
    }


    /**
     * @return DateTimeField
     */
    public final function millisOfDay() {
        return $this->millisOfDay;
    }

    /**
     * @return DateTimeField
     */
    public final function secondOfMinute() {
        return $this->secondOfMinute;
    }

    /**
     * @return DateTimeField
     */
    public final function secondOfDay() {
        return $this->secondOfDay;
    }

    /**
     * @return DateTimeField
     */
    public final function minuteOfHour() {
        return $this->minuteOfHour;
    }

    /**
     * @return DateTimeField
     */
    public final function minuteOfDay() {
        return $this->minuteOfDay;
    }

    /**
     * @return DateTimeField
     */
    public final function hourOfDay() {
        return $this->hourOfDay;
    }

    /**
     * @return DateTimeField
     */
    public final function clockhourOfDay() {
        return $this->clockhourOfDay;
    }

    /**
     * @return DateTimeField
     */
    public final function hourOfHalfday() {
        return $this->hourOfHalfday;
    }

    /**
     * @return DateTimeField
     */
    public final function clockhourOfHalfday() {
        return $this->clockhourOfHalfday;
    }

    /**
     * @return DateTimeField
     */
    public final function halfdayOfDay() {
        return $this->halfdayOfDay;
    }

    /**
     * @return DateTimeField
     */
    public final function dayOfWeek() {
        return $this->dayOfWeek;
    }

    /**
     * @return DateTimeField
     */
    public final function dayOfMonth() {
        return $this->dayOfMonth;
    }

    /**
     * @return DateTimeField
     */
    public final function dayOfYear() {
        return $this->dayOfYear;
    }

    /**
     * @return DateTimeField
     */
    public final function weekOfWeekyear() {
        return $this->weekOfWeekyear;
    }

    /**
     * @return DateTimeField
     */
    public final function weekyear() {
        return $this->weekyear;
    }

    /**
     * @return DateTimeField
     */
    public final function weekyearOfCentury() {
        return $this->weekyearOfCentury;
    }

    /**
     * @return DateTimeField
     */
    public final function monthOfYear() {
        return $this->monthOfYear;
    }

    /**
     * @return DateTimeField
     */
    public final function year() {
        return $this->year;
    }

    /**
     * @return DateTimeField
     */
    public final function yearOfEra() {
        return $this->yearOfEra;
    }

    /**
     * @return DateTimeField
     */
    public final function yearOfCentury() {
        return $this->yearOfCentury;
    }

    /**
     * @return DateTimeField
     */
    public final function centuryOfEra() {
        return $this->centuryOfEra;
    }

    /**
     * @return DateTimeField
     */
    public final function era() {
        return $this->era;
    }

    /**
     * Invoked by the constructor and after deserialization to allow subclasses
     * to define all of its supported fields. All unset fields default to
     * unsupported instances.
     *
     * @param Fields $fields container of fields
     */
    protected abstract function assemble(Fields $fields);

    private function setFields() {
        $fields = new Fields();

        if ($this->base != null) {
            $fields->copyFieldsFrom($this->base);
        }
        $this->assemble($fields);

        $this->millisOfSecond = $fields->millisOfSecond ? $fields->millisOfSecond : parent::millisOfSecond();
        $this->millisOfDay = $fields->millisOfDay ? $fields->millisOfDay : parent::millisOfDay();
        $this->secondOfMinute = $fields->secondOfMinute ? $fields->secondOfMinute : parent::secondOfMinute();
        $this->secondOfDay = $fields->secondOfDay ? $fields->secondOfDay : parent::secondOfDay();
        $this->minuteOfHour = $fields->minuteOfHour ? $fields->minuteOfHour : parent::minuteOfHour();
        $this->minuteOfDay = $fields->minuteOfDay ? $fields->minuteOfDay : parent::minuteOfDay();
        $this->hourOfDay = $fields->hourOfDay ? $fields->hourOfDay : parent::hourOfDay();
        $this->clockhourOfDay = $fields->clockhourOfDay ? $fields->clockhourOfDay : parent::clockhourOfDay();
        $this->hourOfHalfday = $fields->hourOfHalfday ? $fields->hourOfHalfday : parent::hourOfHalfday();
        $this->clockhourOfHalfday = $fields->clockhourOfHalfday ? $fields->clockhourOfHalfday : parent::clockhourOfHalfday();
        $this->halfdayOfDay = $fields->halfdayOfDay ? $fields->halfdayOfDay : parent::halfdayOfDay();
        $this->dayOfWeek = $fields->dayOfWeek ? $fields->dayOfWeek : parent::dayOfWeek();
        $this->dayOfMonth = $fields->dayOfMonth ? $fields->dayOfMonth : parent::dayOfMonth();
        $this->dayOfYear = $fields->dayOfYear ? $fields->dayOfYear : parent::dayOfYear();
        $this->weekOfWeekyear = $fields->weekOfWeekyear ? $fields->weekOfWeekyear : parent::weekOfWeekyear();
        $this->weekyear = $fields->weekyear ? $fields->weekyear : parent::weekyear();
        $this->weekyearOfCentury = $fields->weekyearOfCentury ? $fields->weekyearOfCentury : parent::weekyearOfCentury();
        $this->monthOfYear = $fields->monthOfYear ? $fields->monthOfYear : parent::monthOfYear();
        $this->year = $fields->year ? $fields->year : parent::year();
        $this->yearOfEra = $fields->yearOfEra ? $fields->yearOfEra : parent::yearOfEra();
        $this->yearOfCentury = $fields->yearOfCentury ? $fields->yearOfCentury : parent::yearOfCentury();
        $this->centuryOfEra = $fields->centuryOfEra ? $fields->centuryOfEra : parent::centuryOfEra();
        $this->era = $fields->era ? $fields->era : parent::era();

    }
}
