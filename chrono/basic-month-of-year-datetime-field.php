<?php

include_once dirname(__FILE__) . '/'  . 'basic-chronology.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../field/imprecise-datetime-field.php';

/**
 * Provides time calculations for the month of the year component of time.
 */
class BasicMonthOfYearDateTimeField extends ImpreciseDateTimeField {
    private static $MIN;

    public static function staticInit() {
        self::$MIN = DateTimeConstants::$JANUARY;
    }

    /** @var BasicChronology */
    private $chronology;

    /** @var int */
    private $leapMonth;

    /**
     * @param BasicChronology $chronology
     * @param int $leapMonth
     */
    public function __construct(BasicChronology $chronology, $leapMonth) {
        parent::__construct(DateTimeFieldType::monthOfYear());
        $this->chronology = $chronology;
        $this->max = $this->chronology->getMaxMonth();
        $this->leapMonth = $leapMonth;
    }

    /**
     * @param int $instant
     * @return int
     */
    public function get($instant) {
        return $this->chronology->getMonthOfYear($instant);
    }

    /**
     * Set the Month component of the specified time instant.<p>
     * If the new month has less total days than the specified
     * day of the month, this value is coerced to the nearest
     * sane value. e.g.<p>
     * 07-31 to month 6 = 06-30<p>
     * 03-31 to month 2 = 02-28 or 02-29 depending<p>
     *
     * @param int $instant  the time instant in millis to update.
     * @param int $month  the month (1,12) to update the time to.
     * @return int the updated time instant.
     * @throws IllegalArgumentException  if month is invalid
     */
    public function set($instant, $month) {
        FieldUtils::verifyValueBounds($this, $month, self::$MIN, $this->max);
        //
        $thisYear = $this->chronology->getYear($instant);
        //
        $thisDom = $this->chronology->getDayOfMonth($instant, $thisYear);
        $maxDom = $this->chronology->getDaysInYearMonth($thisYear, $month);
        if ($thisDom > $maxDom) {
            // Quietly force DOM to nearest sane value.
            $thisDom = $maxDom;
        }
        // Return newly calculated millis value
        return $this->chronology->getYearMonthDayMillis($thisYear, $month, $thisDom) +
            $this->chronology->getMillisOfDay($instant);
    }


}

BasicMonthOfYearDateTimeField::staticInit();