<?php

include_once dirname(__FILE__) . '/'  . 'basic-chronology.php';

abstract class BasicGJChronology extends BasicChronology {

    /** @var int[] */
    private static $MIN_DAYS_PER_MONTH_ARRAY = array(
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    );

    /** @var int[] */
    private static $MAX_DAYS_PER_MONTH_ARRAY = array(
        31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    );


    /** @var int[] */
    private static $MIN_TOTAL_MILLIS_BY_MONTH_ARRAY;

    /** @var int[] */
    private static $MAX_TOTAL_MILLIS_BY_MONTH_ARRAY;

    public static function staticInit() {
        $minSum = 0;
        $maxSum = 0;

        self::$MIN_TOTAL_MILLIS_BY_MONTH_ARRAY[0] = 0;
        self::$MAX_TOTAL_MILLIS_BY_MONTH_ARRAY[0] = 0;

        for ($i = 0; $i < 11; $i++) {
            $millis = self::$MIN_DAYS_PER_MONTH_ARRAY[$i] * DateTimeConstants::$MILLIS_PER_DAY;
            $minSum += $millis;
            self::$MIN_TOTAL_MILLIS_BY_MONTH_ARRAY[$i + 1] = $minSum;

            $millis = self::$MAX_DAYS_PER_MONTH_ARRAY[$i] * DateTimeConstants::$MILLIS_PER_DAY;
            $maxSum += $millis;
            self::$MAX_TOTAL_MILLIS_BY_MONTH_ARRAY[$i + 1] = $maxSum;
        }
    }

    /**
     * @param Chronology $base
     * @param mixed $param
     * @param int $minDaysInFirstWeek
     */
    public function __construct(Chronology $base = null, $param, $minDaysInFirstWeek) {
        parent::__construct($base, $param, $minDaysInFirstWeek);
    }

    /**
     * {@inheritdoc}
     */
    public function getDaysInYearMonth($year, $month) {
        if ($this->isLeapYear($year)) {
            return self::$MAX_DAYS_PER_MONTH_ARRAY[$month - 1];
        } else {
            return self::$MIN_DAYS_PER_MONTH_ARRAY[$month - 1];
        }
    }

    public function getTotalMillisByYearMonth($year, $month) {
        if ($this->isLeapYear($year)) {
            return self::$MAX_TOTAL_MILLIS_BY_MONTH_ARRAY[$month - 1];
        } else {
            return self::$MIN_TOTAL_MILLIS_BY_MONTH_ARRAY[$month - 1];
        }
    }

    public function getMonthOfYear_WithYear($millis, $year) {
        // Perform a binary search to get the month. To make it go even faster,
        // compare using ints instead of longs. The number of milliseconds per
        // year exceeds the limit of a 32-bit int's capacity, so divide by
        // 1024. No precision is lost (except time of day) since the number of
        // milliseconds per day contains 1024 as a factor. After the division,
        // the instant isn't measured in milliseconds, but in units of
        // (128/125)seconds.

        $i = intval(($millis - $this->getYearMillis($year)) / 1024);

        // There are 86400000 milliseconds per day, but divided by 1024 is
        // 84375. There are 84375 (128/125)seconds per day.


            if ($this->isLeapYear($year)) {
                    if ($i < (182 * 84375)) {
                        if ($i < (91 * 84375))
                             if ($i < (31 * 84375)) {
                                 return 1;
                             } else {
                                 if ($i < (60 * 84375)) {
                                     return 2;
                                 } else {
                                     return 3;
                                 }
                             }
                        else 
                            if ($i < (121 * 84375)) {
                                return 4;
                            } else {
                                if ($i < (152 * 84375)) {
                                    return 5;
                                } else {
                                    return 6;
                                }
                            }
                    }
                    else {
                        if ($i < (274 * 84375)) {
                            if ($i < (213 * 84375)) {
                                return 7;
                            }
                            else {
                                if ($i < (244 * 84375)) {
                                    return 8;
                                } else {
                                    return 9;
                                }
                            }
                        }
                        else {
                            if ($i < (305 * 84375)) {
                                return 10;
                            }
                            else {
                                if ($i < (335 * 84375)) {
                                    return 11;
                                }
                                else {
                                    return 12;
                                }
                            }
                        }
                    }
                }
                else {
                    if ($i < (181 * 84375)) {
                        if ($i < (90 * 84375)) {
                            if ($i < (31 * 84375)) {
                                return 1;
                            }
                            else {
                                if ($i < (59 * 84375))
                                    return 2;
                                else
                                    return 3;
                            }
                        }
                        else {
                            if ($i < (120 * 84375)) {
                                return 4;
                            } else {
                                if ($i < (151 * 84375)) {
                                    return 5;
                                } else {
                                    return 6;
                                }
                            }
                        }
                    }
                    else {
                        if ($i < (273 * 84375))
                            if ($i < (212 * 84375)) {
                                return 7;
                            } else {
                                if ($i < (243 * 84375)) {
                                    return 8;
                                } else {
                                    return 9;
                                }
                            }
                            else {
                                if ($i < (304 * 84375)) {
                                    return 10;
                                } else {
                                    if ($i < (334 * 84375)) {
                                        return 11;
                                    } else {
                                        return 12;
                                    }
                                }
                            }
                    }
                }
    }

    /**
     * {@inheritdoc}
     * @param int $instant
     * @param int $year
     * @return int
     */
    public function setYear($instant, $year) {
        $thisYear = $this->getYear($instant);
        $dayOfYear = $this->getDayOfYear($instant, $thisYear);
        $millisOfDay = $this->getMillisOfDay($instant);

        if ($dayOfYear > (31 + 28)) { // after Feb 28
            if ($this->isLeapYear($thisYear)) {
                // Current date is Feb 29 or later.
                if (!$this->isLeapYear($year)) {
                    // Moving to a non-leap year, Feb 29 does not exist.
                    $dayOfYear--;
                }
            } else {
                // Current date is Mar 01 or later.
                if ($this->isLeapYear($year)) {
                    // Moving to a leap year, account for Feb 29.
                    $dayOfYear++;
                }
            }
        }

        $instant = $this->getYearMonthDayMillis($year, 1, $dayOfYear);

        $instant = $instant + $millisOfDay;

        return $instant;
    }

}

BasicGJChronology::staticInit();