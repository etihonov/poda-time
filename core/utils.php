<?php

/**
 * Utility class to incapsulate some php integer api bugs
 */
class PTInteger {
    public static function minValue() {
        return (-1 * PHP_INT_MAX) - 1;
    }

    public static function maxValue() {
        return PHP_INT_MAX;
    }
}