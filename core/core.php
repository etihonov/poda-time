<?php

/**
 * Core utils static class
 */
class CoreUtils {

    /**
     * Return hash id for given object
     *
     * @param object $object
     * @return null|string
     */
    public static function getObjectUniqueHashCode($object){
        if (function_exists('spl_object_hash'))
            return spl_object_hash($object);
        else {
            if (is_object($object)) {
                ob_start(); var_dump($object); $dump = ob_get_contents(); ob_end_clean();
                return md5($dump);
            }
            return null;
        }
    }
}

/**
 * An interface describes objects that can be equals or not
 */
interface IPTEquatable {

    /**
     * @param mixed $object
     * @return bool
     */
    function equals($object);


    /**
     * @return string
     */
    function hashCode();
}