<?php

include_once dirname(__FILE__) . '/'  . 'core.php';

/**
 * Key-value pair
 */
class KeyValuePair {
    /** @var \IPTEquatable */
    public $key;
    public $value;
}

/**
 * Map data structure
 */
interface IMap {
    /**
     * @param IPTEquatable $key
     * @param mixed $value
     */
    public function put(IPTEquatable $key, $value);

    /**
     * @param IPTEquatable $key
     * @return null|mixed
     */
    public function get(IPTEquatable $key);
}

/**
 * Map dummy implementation
 */
class Map implements IMap {
    /** @var KeyValuePair[] */
    private $array;

    public function __construct() {
        $this->array = array();
    }

    /**
     * @param IPTEquatable $key
     * @param mixed $value
     */
    public function put(IPTEquatable $key, $value) {
        $pair = $this->findPairByKey($key);
        if ($pair == null) {
            $pair = new KeyValuePair();
            $pair->key = $key;
        }
        $pair->value = $value;
        $this->array[] = $pair;
    }

    /**
     * @param IPTEquatable $key
     * @return KeyValuePair|null
     */
    private function findPairByKey(IPTEquatable $key) {
        foreach($this->array as $pair) {
            if ($pair->key->equals($key)) {
                return $pair;
            }
        }
        return null;
    }

    /**
     * @param IPTEquatable $key
     * @return null|mixed
     */
    public function get(IPTEquatable $key) {
        $pair = $this->findPairByKey($key);
        if ($pair == null) {
            return null;
        }
        return $pair->value;
    }
}
