<?php

include_once dirname(__FILE__) . '/'  . 'core/core.php';

/**
 * Describes a locale
 */
abstract class PTLocale implements IPTEquatable {

    /**
     * @param $id
     * @return PTLocale
     * @throws InvalidArgumentException
     */
    public static function getById($id) {

        $currentLocale = setlocale(LC_TIME, '0');
        if (!setlocale(LC_TIME, $id)) {
            throw new InvalidArgumentException('Unknown locale id');
        }
        setlocale(LC_TIME, $currentLocale);

        return new PHPBasedLocale($id);
    }

    /**
     * Empty constructor
     */
    public function __construct() {
    }

    public abstract function getShortWeekdays();

    /**
     * @param mixed $object
     * @return bool
     */
    function equals($object) {
        return $object === $this;
    }

    /**
     * @return PTLocale
     */
    public static function getDefault() {
        return PHPBasedLocale::getCurrent();
    }
}

/**
 * Locale based on php setlocale function
 */
class PHPBasedLocale extends PTLocale {

    /**
     * @return PHPBasedLocale
     */
    public static function getCurrent() {
        return new PHPBasedLocale(setlocale(LC_TIME, '0'));
    }

    /** @var string */
    private $localeId;

    /**
     * @param string $localeId
     */
    public function __construct($localeId) {
        parent::__construct();
        $this->localeId = $localeId;
    }

    /**
     * @return string[]
     */
    public function getShortWeekdays() {
        $oldLocale = setlocale(LC_TIME, '0');
        setlocale(LC_TIME, $this->localeId);
        $result = array();
        for ($i = 0; $i < 7; $i++) {
            array_push($result, strftime('%a', strtotime('2012-12-03 +' . $i . ' days')));
        }
        setlocale(LC_TIME, $oldLocale);
        return $result;
    }

    /**
     * @return string
     */
    public function getLocaleId() {
        return $this->localeId;
    }

    /**
     * @param mixed $object
     * @return bool
     */
    public function equals($object) {
        if (!is_object($object))
            return false;
        if (!($object instanceof PHPBasedLocale))
            return parent::equals($object);
        return $this->getLocaleId() == $object->getLocaleId();
    }

    /**
     * @return string
     */
    public function hashCode() {
        return md5($this->localeId);
    }
}
