<?php

include_once dirname(__FILE__) . '/'  . '../datetime-zone.php';
include_once dirname(__FILE__) . '/'  . '../datetime-constants.php';

/**
 * PTDateTimeZone implementation based on php5.2+ DateTimeZone class
 */
class NativeBasedDateTimeZone extends PTDateTimeZone implements IPTEquatable {

    private static $UTC_PHP_TIME_ZONE;

    public static function staticInit() {
        self::$UTC_PHP_TIME_ZONE = new DateTimeZone("UTC");
    }

    /** @var DateTimeZone */
    private $phpTimeZone;

    /**
     * @param DateTimeZone $phpTimeZone
     */
    public function __construct(DateTimeZone $phpTimeZone) {
        parent::__construct($phpTimeZone->getName());
        $this->phpTimeZone = $phpTimeZone;
    }

    /**
     * Gets the millisecond offset to add to UTC to get local time.
     *
     * @param int $instant  milliseconds from 1970-01-01T00:00:00Z to get the offset for
     * @return int the millisecond offset to add to UTC to get local time
     */
    public function getOffset($instant) {
        $phpDateTime = new DateTime("now", self::$UTC_PHP_TIME_ZONE);
        $phpDateTime->setTimestamp($instant);
        return $this->phpTimeZone->getOffset($phpDateTime) * DateTimeConstants::$MILLIS_PER_SECOND;
    }
}
NativeBasedDateTimeZone::staticInit();