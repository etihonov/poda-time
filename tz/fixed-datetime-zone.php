<?php

include_once dirname(__FILE__) . '/'  . '../datetime-zone.php';

/**
 * Basic DateTimeZone implementation that has a fixed name key and offsets.
 * <p>
 * FixedDateTimeZone is immutable.
 */
final class FixedDateTimeZone extends PTDateTimeZone implements IPTEquatable {

    private $nameKey;
    private $wallOffset;
    private $standardOffset;

    /**
     * @param string $id
     * @param string $nameKey
     * @param int $wallOffset
     * @param int $standardOffset
     */
    public function __construct($id, $nameKey, $wallOffset, $standardOffset) {
        parent::__construct($id);
        $this->nameKey = $nameKey;
        $this->wallOffset = $wallOffset;
        $this->standardOffset = $standardOffset;
    }

    /**
     * @param int $instant
     * @return int
     */
    public function getOffset($instant) {
        return $this->wallOffset;
    }
}
