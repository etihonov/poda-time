<?php

/**
 * Service provider factory for time zones.
 */
interface IProvider {

    /**
     * Retrieves a DateTimeZone for the given id. All providers must at
     * least support id "UTC".
     *
     * @param string $id
     * @return null|PTDateTimeZone null if not found
     */
    function getZone($id);

    /**
     * Returns an unmodifiable set of ids. All providers must at least
     * support id "UTC".
     * @return string[]
     */
    function getAvailableIDs();
}
