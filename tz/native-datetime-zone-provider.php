<?php

include_once dirname(__FILE__) . '/'  . 'provider.php';
include_once dirname(__FILE__) . '/'  . 'native-based-datetime-zone.php';

/**
 * Provides timezones based on native DateTimeZone implementation in php5.1+
 */
class NativeDateTimeZoneProvider implements IProvider {

    /**
     * @param string $id
     * @return null|PTDateTimeZone null if not found
     */
    function getZone($id) {
        return new NativeBasedDateTimeZone(new DateTimeZone($id));
    }

    /**
     * Returns an unmodifiable set of ids. All providers must at least support id "UTC".
     * @return string[]
     */
    function getAvailableIDs() {
        return DateTimeZone::listIdentifiers();
    }
}
