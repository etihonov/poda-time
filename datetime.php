<?php

include_once dirname(__FILE__) . '/'  . 'base/base-datetime.php';
include_once dirname(__FILE__) . '/'  . 'readable-datetime.php';


class PTDateTime extends BaseDateTime
    implements IReadableDateTime {

    /**
     * @param int $millis
     * @return PTDateTime
     */
    public static function fromMillis($millis) {
        return new PTDateTime($millis);
    }

    /**
     * @param int $millis
     * @param Chronology $chrono
     * @return PTDateTime
     */
    public static function fromMillisAndChrono($millis, Chronology $chrono) {
        return new PTDateTime($millis, $chrono);
    }

    /**
     * Returns a copy of this datetime with different millis.
     * <p>
     * The returned object will be either be a new instance or <code>this</code>.
     * Only the millis will change, the chronology and time zone are kept.
     *
     * @param int $newMillis  the new millis, from 1970-01-01T00:00:00Z
     * @return PTDateTime a copy of this datetime with different millis
     */
    public function withMillis($newMillis) {
        return ($newMillis == $this->getMillis() ? $this : PTDateTime::fromMillisAndChrono($newMillis, $this->getChronology()));
    }

    /**
     * Returns a copy of this datetime with a different chronology.
     * <p>
     * The returned object will be either be a new instance or <code>this</code>.
     * Only the chronology will change, the millis are kept.
     *
     * @param Chronology|null $newChronology  the new chronology, null means ISO default
     * @return PTDatetime a copy of this datetime with a different chronology
     */
    public function withChronology(Chronology $newChronology = null) {
        $newChronology = DateTimeUtils::getChronology($newChronology);
        return ($newChronology === $this->getChronology() ? $this : PTDateTime::fromMillisAndChrono($this->getMillis(), $newChronology));
    }

    /**
     * Returns a copy of this datetime with a different time zone, preserving the
     * millisecond instant.
     * <p>
     * This method is useful for finding the local time in another timezone.
     * For example, if this instant holds 12:30 in Europe/London, the result
     * from this method with Europe/Paris would be 13:30.
     * <p>
     * The returned object will be a new instance of the same implementation type.
     * This method changes the time zone, and does not change the
     * millisecond instant, with the effect that the field values usually change.
     * The returned object will be either be a new instance or <code>this</code>.
     *
     * @param null|PTDateTimeZone $newZone the new time zone
     * @return PTDateTime a copy of this datetime with a different time zone
     * @see #withZoneRetainFields
     */
    public function withZone(PTDateTimeZone $newZone = null) {
        return $this->withChronology($this->getChronology()->withZone($newZone));
    }

    /**
     * Constructs an instance from datetime field values
     * using the specified chronology.
     * <p>
     * If the chronology is null, <code>ISOChronology</code>
     * in the default time zone is used.
     *
     * @param int $year  the year
     * @param int $monthOfYear  the month of the year
     * @param int $dayOfMonth  the day of the month
     * @param int $hourOfDay  the hour of the day
     * @param int $minuteOfHour  the minute of the hour
     * @param int $secondOfMinute  the second of the minute
     * @param int $millisOfSecond  the millisecond of the second
     * @param Chronology $chronology  the chronology, null means ISOChronology in default zone
     * @return \PTDateTime
     */
    public static function fromDateTimeFieldValuesWithChrono($year,
                                                             $monthOfYear,
                                                             $dayOfMonth,
                                                             $hourOfDay,
                                                             $minuteOfHour,
                                                             $secondOfMinute,
                                                             $millisOfSecond,
                                                             Chronology $chronology = null) {
        $chronology = self::checkChronology($chronology);
        $instant = $chronology->getDateTimeMillis($year, $monthOfYear, $dayOfMonth, $hourOfDay, $minuteOfHour, $secondOfMinute, $millisOfSecond);
        $millis = self::checkInstant($instant, $chronology);

        return self::fromMillisAndChrono($millis, $chronology);
    }

    /**
     * Returns a copy of this datetime with the year field updated.
     * <p>
     * PTDateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * year changed.
     *
     * @param int $year  the year to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     */
    public function withYear($year) {
        return $this->withMillis($this->getChronology()->year()->set($this->getMillis(), $year));
    }


    /**
     * Returns a copy of this datetime with the month of year field updated.
     * <p>
     * PTDateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * month of year changed.
     *
     * @param int $monthOfYear  the month of year to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     */
    public function withMonthOfYear($monthOfYear) {
        return $this->withMillis($this->getChronology()->monthOfYear()->set($this->getMillis(), $monthOfYear));
    }

    /**
     * Returns a copy of this datetime with the day of month field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * day of month changed.
     *
     * @param int $dayOfMonth  the day of month to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     */
    public function withDayOfMonth($dayOfMonth) {
        return $this->withMillis($this->getChronology()->dayOfMonth()->set($this->getMillis(), $dayOfMonth));
    }

    /**
     * Returns a copy of this datetime with the era field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * era changed.
     *
     * @param int $era  the era to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     */
    public function withEra($era) {
        return $this->withMillis($this->getChronology()->era()->set($this->getMillis(), $era));
    }

    /**
     * Returns a copy of this datetime with the century of era field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * century of era changed.
     *
     * @param int $centuryOfEra  the centurey of era to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withCenturyOfEra($centuryOfEra) {
        return $this->withMillis($this->getChronology()->centuryOfEra()->set($this->getMillis(), $centuryOfEra));
    }

    /**
     * Returns a copy of this datetime with the year of era field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * year of era changed.
     *
     * @param int $yearOfEra  the year of era to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withYearOfEra($yearOfEra) {
        return $this->withMillis($this->getChronology()->yearOfEra()->set($this->getMillis(), $yearOfEra));
    }

    /**
     * Returns a copy of this datetime with the year of century field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * year of century changed.
     *
     * @param int $yearOfCentury  the year of century to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withYearOfCentury($yearOfCentury) {
        return $this->withMillis($this->getChronology()->yearOfCentury()->set($this->getMillis(), $yearOfCentury));
    }

    /**
     * Returns a copy of this datetime with the weekyear field updated.
     * <p>
     * The weekyear is the year that matches with the weekOfWeekyear field.
     * In the standard ISO8601 week algorithm, the first week of the year
     * is that in which at least 4 days are in the year. As a result of this
     * definition, day 1 of the first week may be in the previous year.
     * The weekyear allows you to query the effective year for that day.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * weekyear changed.
     *
     * @param int $weekyear  the weekyear to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withWeekyear($weekyear) {
        return $this->withMillis($this->getChronology()->weekyear()->set($this->getMillis(), $weekyear));
    }

    /**
     * Returns a copy of this datetime with the week of weekyear field updated.
     * <p>
     * This field is associated with the "weekyear" via {@link #withWeekyear(int)}.
     * In the standard ISO8601 week algorithm, the first week of the year
     * is that in which at least 4 days are in the year. As a result of this
     * definition, day 1 of the first week may be in the previous year.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * week of weekyear changed.
     *
     * @param int $weekOfWeekyear  the week of weekyear to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withWeekOfWeekyear($weekOfWeekyear) {
        return $this->withMillis($this->getChronology()->weekOfWeekyear()->set($this->getMillis(), $weekOfWeekyear));
    }

    /**
     * Returns a copy of this datetime with the day of year field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * day of year changed.
     *
     * @param int $dayOfYear  the day of year to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withDayOfYear($dayOfYear) {
        return $this->withMillis($this->getChronology()->dayOfYear()->set($this->getMillis(), $dayOfYear));
    }

    /**
     * Returns a copy of this datetime with the day of week field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * day of week changed.
     *
     * @param int $dayOfWeek  the day of week to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withDayOfWeek($dayOfWeek) {
        return $this->withMillis($this->getChronology()->dayOfWeek()->set($this->getMillis(), $dayOfWeek));
    }

    //-----------------------------------------------------------------------
    /**
     * Returns a copy of this datetime with the hour of day field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * hour of day changed.
     *
     * @param int $hour  the hour of day to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withHourOfDay($hour) {
        return $this->withMillis($this->getChronology()->hourOfDay()->set($this->getMillis(), $hour));
    }

    /**
     * Returns a copy of this datetime with the minute of hour updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * minute of hour changed.
     *
     * @param int $minute  the minute of hour to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withMinuteOfHour($minute) {
        return $this->withMillis($this->getChronology()->minuteOfHour()->set($this->getMillis(), $minute));
    }

    /**
     * Returns a copy of this datetime with the second of minute field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * second of minute changed.
     *
     * @param int $second  the second of minute to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withSecondOfMinute($second) {
        return $this->withMillis($this->getChronology()->secondOfMinute()->set($this->getMillis(), $second));
    }

    /**
     * Returns a copy of this datetime with the millis of second field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * millis of second changed.
     *
     * @param int $millis  the millis of second to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withMillisOfSecond($millis) {
        return $this->withMillis($this->getChronology()->millisOfSecond()->set($this->getMillis(), $millis));
    }

    /**
     * Returns a copy of this datetime with the millis of day field updated.
     * <p>
     * DateTime is immutable, so there are no set methods.
     * Instead, this method returns a new instance with the value of
     * millis of day changed.
     *
     * @param int $millis  the millis of day to set
     * @return PTDateTime a copy of this object with the field set
     * @throws IllegalArgumentException if the value is invalid
     * @since 1.3
     */
    public function withMillisOfDay($millis) {
        return $this->withMillis($this->getChronology()->millisOfDay()->set($this->getMillis(), $millis));
    }

}