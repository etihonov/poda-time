<?php

class DurationFieldType {

    // Ordinals for standard field types.
    const
        ERAS = 1,
        CENTURIES = 2,
        WEEKYEARS = 3,
        YEARS = 4,
        MONTHS = 5,
        WEEKS = 6,
        DAYS = 7,
        HALFDAYS = 8,
        HOURS = 9,
        MINUTES = 10,
        SECONDS = 11,
        MILLIS = 12;

    private static $erasType;
    private static $centuriesType;
    private static $weekyearsType;
    private static $yearsType;
    private static $monthsType;
    private static $weeksType;
    private static $daysType;
    private static $halfdaysType;
    private static $hoursType;
    private static $minutesType;
    private static $secondsType;
    private static $millisType;

    public static function staticInit() {
        self::$erasType        = new StandardDurationFieldType("eras", self::ERAS);
        self::$centuriesType   = new StandardDurationFieldType("centuries", self::CENTURIES);
        self::$weekyearsType   = new StandardDurationFieldType("weekyears", self::WEEKYEARS);
        self::$yearsType       = new StandardDurationFieldType("years", self::YEARS);
        self::$monthsType      = new StandardDurationFieldType("months", self::MONTHS);
        self::$weeksType       = new StandardDurationFieldType("weeks", self::WEEKS);
        self::$daysType        = new StandardDurationFieldType("days", self::DAYS);
        self::$halfdaysType    = new StandardDurationFieldType("halfdays", self::HALFDAYS);
        self::$hoursType       = new StandardDurationFieldType("hours", self::HOURS);
        self::$minutesType     = new StandardDurationFieldType("minutes", self::MINUTES);
        self::$secondsType     = new StandardDurationFieldType("seconds", self::SECONDS);
        self::$millisType      = new StandardDurationFieldType("millis", self::MILLIS);
    }

    public static function eras() {
        return self::$erasType;
    }

    public static function centuries() {
        return self::$centuriesType;
    }

    public static function weekyears() {
        return self::$weekyearsType;
    }

    public static function years() {
        return self::$yearsType;
    }

    public static function months() {
        return self::$monthsType;
    }

    public static function weeks() {
        return self::$weeksType;
    }

    public static function days() {
        return self::$daysType;
    }

    public static function halfdays() {
        return self::$halfdaysType;
    }

    public static function hours() {
        return self::$hoursType;
    }

    public static function minutes() {
        return self::$minutesType;
    }

    public static function seconds() {
        return self::$secondsType;
    }

    public static function millis() {
        return self::$millisType;
    }

    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     */
    protected function __construct($name) {
        $this->name = $name;
    }
}
DurationFieldType::staticInit();

class StandardDurationFieldType extends DurationFieldType {

    private $ordinal;

    /**
     * @param string $name
     * @param int $ordinal
     */
    public function __construct($name, $ordinal) {
        parent::__construct($name);
        $this->ordinal = $ordinal;
    }
}