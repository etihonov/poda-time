<?php

include_once dirname(__FILE__) . '/'  . 'core/core.php';
include_once dirname(__FILE__) . '/'  . 'exceptions.php';

/**
 * DateTimeZone represents a time zone.
 * <p>
 * A time zone is a system of rules to convert time from one geographic
 * location to another. For example, Paris, France is one hour ahead of
 * London, England. Thus when it is 10:00 in London, it is 11:00 in Paris.
 * <p>
 * All time zone rules are expressed, for historical reasons, relative to
 * Greenwich, London. Local time in Greenwich is referred to as Greenwich Mean
 * Time (GMT).  This is similar, but not precisely identical, to Universal
 * Coordinated Time, or UTC. This library only uses the term UTC.
 * <p>
 * Using this system, America/Los_Angeles is expressed as UTC-08:00, or UTC-07:00
 * in the summer. The offset -08:00 indicates that America/Los_Angeles time is
 * obtained from UTC by adding -08:00, that is, by subtracting 8 hours.
 * <p>
 * The offset differs in the summer because of daylight saving time, or DST.
 * The following definitions of time are generally used:
 * <ul>
 * <li>UTC - The reference time.
 * <li>Standard Time - The local time without a daylight saving time offset.
 * For example, in Paris, standard time is UTC+01:00.
 * <li>Daylight Saving Time - The local time with a daylight saving time
 * offset. This offset is typically one hour, but not always. It is typically
 * used in most countries away from the equator.  In Paris, daylight saving
 * time is UTC+02:00.
 * <li>Wall Time - This is what a local clock on the wall reads. This will be
 * either Standard Time or Daylight Saving Time depending on the time of year
 * and whether the location uses Daylight Saving Time.
 * </ul>
 * <p>
 * Unlike the Java TimeZone class, DateTimeZone is immutable. It also only
 * supports long format time zone ids. Thus EST and ECT are not accepted.
 * However, the factory that accepts a TimeZone will attempt to convert from
 * the old short id to a suitable long id.
 * <p>
 * DateTimeZone is immutable, and all subclasses must be as
 * well.
 */
abstract class PTDateTimeZone implements IPTEquatable {

    /**
     * The instance that is providing time zones.
     * @var IProvider
     */
    private static $provider;

    /** @var null|PTDateTimeZone */
    private static $default = null;

    /** @var PTDateTimeZone */
    public static $UTC;

    /**
     * Static constructor
     */
    public static function staticInit() {
        self::$UTC = new FixedDateTimeZone("UTC", "UTC", 0, 0);
        self::setProvider0(null);
    }

    /**
     * @return PTDateTimeZone
     */
    public static function getDefault() {
        $result = self::$default;
        if ($result == null) {
            throw new UnimplementedOperationException();
        }
        return self::$default;
    }

    /**
     * @param PTDateTimeZone $zone
     */
    public static function setDefault(PTDateTimeZone $zone) {
        self::$default = $zone;
    }

    /** @var string */
    private $id;

    /**
     * @param string $id
     * @throws IllegalArgumentException
     */
    protected function __construct($id) {
        if ($id == null) {
            throw new IllegalArgumentException("Id must not be null");
        }
        $this->id = $id;
    }

    /**
     * Gets the millisecond offset to add to UTC to get local time.
     *
     * @param int $instant  milliseconds from 1970-01-01T00:00:00Z to get the offset for
     * @return int the millisecond offset to add to UTC to get local time
     */
    public abstract function getOffset($instant);

    /**
     * Converts a standard UTC instant to a local instant with the same
     * local time. This conversion is used before performing a calculation
     * so that the calculation can be done using a simple local zone.
     *
     * @param int $instantUTC  the UTC instant to convert to local
     * @return int the local instant with the same local time
     * @throws ArithmeticException if the result overflows a long
     * @since 1.5
     */
    public function convertUTCToLocal($instantUTC) {
        $offset = $this->getOffset($instantUTC);
        $instantLocal = $instantUTC + $offset;

        // If there is a sign change, but the two values have the same sign...
        /* TODO
        if ((instantUTC ^ instantLocal) < 0 && (instantUTC ^ offset) >= 0) {
            throw new ArithmeticException("Adding time zone offset caused overflow");
        }*/

        return $instantLocal;
    }

    /**
     * @param string $id
     * @throws IllegalArgumentException
     * @return \PTDateTimeZone
     */
    public static function forID($id) {
        if ($id == null) {
            return self::getDefault();
        }
        if ($id == "UTC") {
            return self::$UTC;
        }

        $zone = self::$provider->getZone($id);
        if ($zone != null) {
            return $zone;
        }

        /*if ($id == "Europe/London") {
            return new FixedDateTimeZone("Europe/London", "Europe/London",
                DateTimeConstants::$MILLIS_PER_HOUR, DateTimeConstants::$MILLIS_PER_HOUR);
        }*/


        /* TODO

         if (id.startsWith("+") || id.startsWith("-")) {
            int offset = parseOffset(id);
            if (offset == 0L) {
                return DateTimeZone.UTC;
            } else {
                id = printOffset(offset);
                return fixedOffsetZone(id, offset);
            }
        }*/
        throw new IllegalArgumentException("The datetime zone id '" . $id . "' is not recognised");

    }

    /**
     * Sets the zone provider factory without performing the security check.
     *
     * @param IProvider $provider  provider to use, or null for default
     * @throws IllegalArgumentException if the provider is invalid
     */
    private static function setProvider0(IProvider $provider = null) {
        if ($provider == null) {
            $provider = self::getDefaultProvider();
        }
        $ids = $provider->getAvailableIDs();
        if ($ids == null || count($ids) == 0) {
            throw new IllegalArgumentException("The provider doesn't have any available ids");
        }
        if (!in_array("UTC", $ids)) {
            throw new IllegalArgumentException("The provider doesn't support UTC");
        }

        /* TODO
        if (!self::$UTC.equals(provider.getZone("UTC"))) {
            throw new IllegalArgumentException("Invalid UTC zone provided");
        }*/

        self::$provider = $provider;
    }

    /**
     * Gets the default zone provider.
     * @return IProvider
     */
    private static function getDefaultProvider() {
        return new NativeDateTimeZoneProvider();
    }

    /**
     * @param mixed $object
     * @return bool
     */
    public function equals($object) {
        return $this === $object;
    }

    /**
     * @return string
     */
    public function hashCode() {
        return CoreUtils::getObjectUniqueHashCode($this);
    }
}

include_once dirname(__FILE__) . '/'  . 'tz/fixed-datetime-zone.php';
include_once dirname(__FILE__) . '/'  . 'tz/native-datetime-zone-provider.php';

PTDateTimeZone::staticInit();

