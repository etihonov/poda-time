<?php

include_once 'simpletest/autorun.php';

include_once dirname(__FILE__) . '/'  . '../readable-instant.php';
include_once dirname(__FILE__) . '/'  . '../datetime.php';
include_once dirname(__FILE__) . '/'  . '../datetime-constants.php';
include_once dirname(__FILE__) . '/'  . '../datetime-zone.php';
include_once dirname(__FILE__) . '/'  . 'testutils.php';

class TestDateTime_Basics extends UnitTestCase {

    private $y2002days;
    private $y2003days;
    private $TEST_TIME_NOW;
    private $TEST_TIME1;
    private $TEST_TIME2;
    private $englishLocale;

    /** @var ISOChronology */
    private $ISO_DEFAULT;

    /** @var PTDateTimeZone */
    private $londonTimeZone;
    /** @var PTDateTimeZone */
    private $parisTimeZone;

    /** @var GregorianChronology */
    private $GREGORIAN_PARIS;

    /** @var GregorianChronology */
    private $GREGORIAN_DEFAULT;

    /** @var GregorianChronology */
    private $GJ_DEFAULT;

    /** @var Chronology */
    private $ISO_PARIS;

    private function initConstants() {
        $this->englishLocale = PTLocale::getById('ENGLISH');

        $this->londonTimeZone = PTDateTimeZone::forID("Europe/London");
        $this->parisTimeZone = PTDateTimeZone::forID("Europe/Paris");
        $this->ISO_DEFAULT = ISOChronology::getInstance($this->londonTimeZone);

        $this->GREGORIAN_DEFAULT = GregorianChronology::getInstance($this->londonTimeZone);
        $this->GREGORIAN_PARIS = GregorianChronology::getInstance($this->parisTimeZone);

        $this->GJ_DEFAULT = GregorianChronology::getInstance($this->londonTimeZone); // TODO GJChronology.getInstance(LONDON);
        $this->ISO_PARIS = ISOChronology::getInstance($this->parisTimeZone);

        $this->y2002days =
            365 + 365 + 366 + 365 + 365 + 365 + 366 + 365 + 365 + 365 +
                366 + 365 + 365 + 365 + 366 + 365 + 365 + 365 + 366 + 365 +
                365 + 365 + 366 + 365 + 365 + 365 + 366 + 365 + 365 + 365 +
                366 + 365;

        $this->y2003days =
            365 + 365 + 366 + 365 + 365 + 365 + 366 + 365 + 365 + 365 +
                366 + 365 + 365 + 365 + 366 + 365 + 365 + 365 + 366 + 365 +
                365 + 365 + 366 + 365 + 365 + 365 + 366 + 365 + 365 + 365 +
                366 + 365 + 365;

        // 2002-06-09
        $this->TEST_TIME_NOW = ($this->y2002days + 31 + 28 + 31 + 30 + 31 + 9 - 1) * DateTimeConstants::$MILLIS_PER_DAY;

        // 2002-04-05
        $this->TEST_TIME1 =
            ($this->y2002days + 31 + 28 + 31 + 5 - 1) * DateTimeConstants::$MILLIS_PER_DAY
                + 12 * DateTimeConstants::$MILLIS_PER_HOUR
                + 24 * DateTimeConstants::$MILLIS_PER_MINUTE;

        // 2003-05-06
        $this->TEST_TIME2 =
            ($this->y2003days + 31 + 28 + 31 + 30 + 6 - 1) * DateTimeConstants::$MILLIS_PER_DAY
                + 14 * DateTimeConstants::$MILLIS_PER_HOUR
                + 28 * DateTimeConstants::$MILLIS_PER_MINUTE;

    }

    public function setUp() {
        $this->initConstants();

        DateTimeUtils::setCurrentMillisFixed($this->TEST_TIME_NOW);
        PTDateTimeZone::setDefault($this->londonTimeZone);
    }

    public function testGetters() {
        $test = new PTDateTime();

        PTAssert::areEquals($this, $this->ISO_DEFAULT, $test->getChronology());
        PTAssert::areEquals($this, $this->londonTimeZone, $test->getZone());
        $this->assertEqual($this->TEST_TIME_NOW, $test->getMillis());

        $this->assertEqual(1, $test->getEra());
        $this->assertEqual(20, $test->getCenturyOfEra());
        $this->assertEqual(2, $test->getYearOfCentury());
        $this->assertEqual(2002, $test->getYearOfEra());
        $this->assertEqual(2002, $test->getYear());
        $this->assertEqual(6, $test->getMonthOfYear());
        $this->assertEqual(9, $test->getDayOfMonth());

        $this->assertEqual(2002, $test->getWeekyear());
        $this->assertEqual(23, $test->getWeekOfWeekyear());
        $this->assertEqual(7, $test->getDayOfWeek());
        $this->assertEqual(160, $test->getDayOfYear());
        $this->assertEqual(1, $test->getHourOfDay());
        $this->assertEqual(0, $test->getMinuteOfHour());
        $this->assertEqual(60, $test->getMinuteOfDay());
        $this->assertEqual(0, $test->getSecondOfMinute());
        $this->assertEqual(60 * 60, $test->getSecondOfDay());
        $this->assertEqual(0, $test->getMillisOfSecond());
        $this->assertEqual(60 * 60 * 1000, $test->getMillisOfDay());
    }

    public function testWithers() {
        $test = PTDateTime::fromDateTimeFieldValuesWithChrono(1970, 6, 9, 10, 20, 30, 40, $this->GJ_DEFAULT);

        $this->check($test, 1970, 6, 9, 10, 20, 30, 40);
        $this->check($test->withYear(2000), 2000, 6, 9, 10, 20, 30, 40);
        $this->check($test->withMonthOfYear(2), 1970, 2, 9, 10, 20, 30, 40);
        $this->check($test->withDayOfMonth(2), 1970, 6, 2, 10, 20, 30, 40);


        $this->check($test->withDayOfYear(6), 1970, 1, 6, 10, 20, 30, 40);

        $this->check($test->withDayOfWeek(2), 1970, 6, 9, 10, 20, 30, 40);
        $this->check($test->withWeekOfWeekyear(6), 1970, 2, 3, 10, 20, 30, 40);
        $this->check($test->withWeekyear(1971), 1971, 6, 15, 10, 20, 30, 40);
        $this->check($test->withYearOfCentury(60), 1960, 6, 9, 10, 20, 30, 40);
        $this->check($test->withCenturyOfEra(21), 2070, 6, 9, 10, 20, 30, 40);
        $this->check($test->withYearOfEra(1066), 1066, 6, 9, 10, 20, 30, 40);

        //$this->check($test->withEra(DateTimeConstants::$BC), -1970, 6, 9, 10, 20, 30, 40);
        //$this->check($test->withYear(-1200), -1200, 6, 9, 10, 20, 30, 40);

        $this->check($test->withHourOfDay(6), 1970, 6, 9, 6, 20, 30, 40);
        $this->check($test->withMinuteOfHour(6), 1970, 6, 9, 10, 6, 30, 40);
        $this->check($test->withSecondOfMinute(6), 1970, 6, 9, 10, 20, 6, 40);
        $this->check($test->withMillisOfSecond(6), 1970, 6, 9, 10, 20, 30, 6);
        $this->check($test->withMillisOfDay(61234), 1970, 6, 9, 0, 1, 1, 234);


        /* try {
        test.withMonthOfYear(0);
        fail();
        } catch (IllegalArgumentException ex) {}
                try {
                    test.withMonthOfYear(13);
                    fail();
                } catch (IllegalArgumentException ex) {}*/
    }


    public function testEqualsHashCode() {
        $test1 = PTDateTime::fromMillis($this->TEST_TIME1);
        $test2 = PTDateTime::fromMillis($this->TEST_TIME1);

        $this->assertTrue($test1->equals($test2));
        $this->assertTrue($test2->equals($test1));
        $this->assertTrue($test1->equals($test1));
        $this->assertTrue($test2->equals($test2));
        $this->assertTrue($test1->hashCode() == $test2->hashCode());
        $this->assertTrue($test1->hashCode() == $test1->hashCode());
        $this->assertTrue($test2->hashCode() == $test2->hashCode());

        $test3 = PTDateTime::fromMillis($this->TEST_TIME2);
        $this->assertFalse($test1->equals($test3));
        $this->assertFalse($test2->equals($test3));
        $this->assertFalse($test3->equals($test1));
        $this->assertFalse($test3->equals($test2));
        $this->assertFalse($test1->hashCode() == $test3->hashCode());
        $this->assertFalse($test2->hashCode() == $test3->hashCode());

        /* TODO
        assertEquals(false, test1.equals("Hello"));
        assertEquals(true, test1.equals(new MockInstant()));
        assertEquals(false, test1.equals(new DateTime(TEST_TIME1, GREGORIAN_DEFAULT)));
        assertEquals(true, new DateTime(TEST_TIME1, new MockEqualsChronology()).equals(new DateTime(TEST_TIME1, new MockEqualsChronology())));
        assertEquals(false, new DateTime(TEST_TIME1, new MockEqualsChronology()).equals(new DateTime(TEST_TIME1, ISO_DEFAULT)));*/
    }

    public function testCompareTo() {
        $test1 = PTDateTime::fromMillis($this->TEST_TIME1);
        $test1a = PTDateTime::fromMillis($this->TEST_TIME1);
        $this->assertEqual(0, $test1->compareTo($test1a));
        $this->assertEqual(0, $test1a->compareTo($test1));
        $this->assertEqual(0, $test1->compareTo($test1));
        $this->assertEqual(0, $test1a->compareTo($test1a));

        $test2 = PTDateTime::fromMillis($this->TEST_TIME2);
        $this->assertEqual(-1, $test1->compareTo($test2));
        $this->assertEqual(+1, $test2->compareTo($test1));

        $test3 = PTDateTime::fromMillisAndChrono($this->TEST_TIME2, $this->GREGORIAN_PARIS);
        $this->assertEqual(-1, $test1->compareTo($test3));
        $this->assertEqual(+1, $test3->compareTo($test1));
        $this->assertEqual(0, $test3->compareTo($test2));

        $this->assertEqual(+1, $test2->compareTo(new StubInstant($this->TEST_TIME1, $this->ISO_DEFAULT)));
        $this->assertEqual(0, $test1->compareTo(new StubInstant($this->TEST_TIME1, $this->ISO_DEFAULT)));

        try {
            $test1->compareTo(null);
            $this->fail();
        } catch (NullPointerException $ex) {
        }

    }

    public function testIsEqual_long() {
        $this->assertFalse(PTDateTime::fromMillis($this->TEST_TIME1)->isEqual($this->TEST_TIME2));
        $this->assertTrue(PTDateTime::fromMillis($this->TEST_TIME1)->isEqual($this->TEST_TIME1));
        $this->assertFalse(PTDateTime::fromMillis($this->TEST_TIME2)->isEqual($this->TEST_TIME1));
    }

    public function testIsEqualNow() {
        $this->assertFalse(PTDateTime::fromMillis($this->TEST_TIME_NOW - 1)->isEqualNow());
        $this->assertTrue(PTDateTime::fromMillis($this->TEST_TIME_NOW)->isEqualNow());
        $this->assertFalse(PTDateTime::fromMillis($this->TEST_TIME_NOW + 1)->isEqualNow());
    }

    public function testIsEqual_RI() {
        $test1 = new PTDateTime($this->TEST_TIME1);
        $test1a = new PTDateTime($this->TEST_TIME1);
        $this->assertTrue($test1->isEqual($test1a));
        $this->assertTrue($test1a->isEqual($test1));
        $this->assertTrue($test1->isEqual($test1));
        $this->assertTrue($test1a->isEqual($test1a));

        $test2 = new PTDateTime($this->TEST_TIME2);
        $this->assertFalse($test1->isEqual($test2));
        $this->assertFalse($test2->isEqual($test1));

        $test3 = new PTDateTime($this->TEST_TIME2, $this->GREGORIAN_PARIS);
        $this->assertFalse($test1->isEqual($test3));
        $this->assertFalse($test3->isEqual($test1));
        $this->assertTrue($test3->isEqual($test2));

        $this->assertFalse($test2->isEqual(new StubInstant($this->TEST_TIME1, $this->ISO_DEFAULT)));
        $this->assertTrue($test1->isEqual(new StubInstant($this->TEST_TIME1, $this->ISO_DEFAULT)));

        $this->assertFalse(PTDateTime::fromMillis($this->TEST_TIME_NOW + 1)->isEqual(null));
        $this->assertTrue(PTDateTime::fromMillis($this->TEST_TIME_NOW)->isEqual(null));
        $this->assertFalse(PTDateTime::fromMillis($this->TEST_TIME_NOW - 1)->isEqual(null));
    }


    public function testToString_String() {
        $test = new PTDateTime($this->TEST_TIME_NOW);
        $this->assertEqual("2002-06-09 01:00:00", $test->toString("yyyy-MM-dd HH:mm:ss"));
        //$this->assertEqual("2002-06-09T01:00:00.000+01:00", $test->toString(null));
    }

    public function testToString_String_Locale() {
        $test = PTDateTime::fromMillis($this->TEST_TIME_NOW);

        $this->assertEqual("Sun 9/6", $test->toString("EEE d/M", $this->englishLocale));
        $this->assertEqual("dim. 9/6", $test->toString("EEE d/M", PTLocale::getById('FRENCH')));
        //$this->assertEqual("2002-06-09T01:00:00.000+01:00", $test->toString(null, Locale::getById('ENGLISH')));
        $this->assertEqual("Sun 9/6", $test->toString("EEE d/M", null));
        //$this->assertEqual("2002-06-09T01:00:00.000+01:00", $test->toString(null, null));
    }

    //-----------------------------------------------------------------------
    public function testWithMillis_long() {
        $test = PTDateTime::fromMillis($this->TEST_TIME1);
        $result = $test->withMillis($this->TEST_TIME2);
        $this->assertEqual($this->TEST_TIME2, $result->getMillis());
        $this->assertEqual($test->getChronology(), $result->getChronology());

        $test = PTDateTime::fromMillisAndChrono($this->TEST_TIME1, $this->GREGORIAN_PARIS);
        $result = $test->withMillis($this->TEST_TIME2);
        $this->assertEqual($this->TEST_TIME2, $result->getMillis());
        $this->assertEqual($test->getChronology(), $result->getChronology());

        $test = PTDateTime::fromMillis($this->TEST_TIME1);
        $result = $test->withMillis($this->TEST_TIME1);
        $this->assertSame($test, $result);
    }

    public function testWithChronology_Chronology() {
        $test = PTDateTime::fromMillis($this->TEST_TIME1);
        $result = $test->withChronology($this->GREGORIAN_PARIS);
        $this->assertEqual($test->getMillis(), $result->getMillis());
        $this->assertEqual($this->GREGORIAN_PARIS, $result->getChronology());

        $test = PTDateTime::fromMillisAndChrono($this->TEST_TIME1, $this->GREGORIAN_PARIS);
        $result = $test->withChronology(null);
        $this->assertEqual($test->getMillis(), $result->getMillis());
        $this->assertEqual($this->ISO_DEFAULT, $result->getChronology());

        $test = PTDateTime::fromMillis($this->TEST_TIME1);
        $result = $test->withChronology(null);
        $this->assertEqual($test->getMillis(), $result->getMillis());
        $this->assertEqual($this->ISO_DEFAULT, $result->getChronology());

        $test = PTDateTime::fromMillis($this->TEST_TIME1);
        $result = $test->withChronology($this->ISO_DEFAULT);
        $this->assertSame($test, $result);
    }

    public function testWithZone_DateTimeZone() {
        $test = PTDateTime::fromMillis($this->TEST_TIME1);
        $result = $test->withZone($this->parisTimeZone);
        $this->assertEqual($test->getMillis(), $result->getMillis());
        $this->assertEqual($this->ISO_PARIS, $result->getChronology());

        $test = PTDateTime::fromMillisAndChrono($this->TEST_TIME1, $this->GREGORIAN_PARIS);
        $result = $test->withZone(null);
        $this->assertEqual($test->getMillis(), $result->getMillis());
        //TODO PTAssert::areEquals($this, $this->GREGORIAN_DEFAULT, $result->getChronology());

        $test = PTDateTime::fromMillis($this->TEST_TIME1);
        $result = $test->withZone(null);
        $this->assertSame($test, $result);
    }


    /**
     * @param PTDateTime $test
     * @param int $year
     * @param int $month
     * @param int $day
     * @param int $hour
     * @param int $min
     * @param int $sec
     * @param int $mil
     */
    private function check(PTDateTime $test, $year, $month, $day, $hour, $min, $sec, $mil) {
        $this->assertEqual($year, $test->getYear());
        $this->assertEqual($month, $test->getMonthOfYear());
        $this->assertEqual($day, $test->getDayOfMonth());
        $this->assertEqual($hour, $test->getHourOfDay());
        $this->assertEqual($min, $test->getMinuteOfHour());
        $this->assertEqual($sec, $test->getSecondOfMinute());
        $this->assertEqual($mil, $test->getMillisOfSecond());
    }
}

/**
 * Stub instant
 */
class StubInstant extends AbstractInstant {

    private $millis;
    private $chronology;

    /**
     * @param int $millis
     * @param Chronology $chronology
     */
    public function __construct($millis, Chronology $chronology) {
        parent::__construct();
        $this->millis = $millis;
        $this->chronology = $chronology;
    }

    /**
     * @return int
     */
    function getMillis() {
        return $this->millis;
    }

    /**
     * @return Chronology
     */
    function getChronology() {
        return $this->chronology;
    }
}