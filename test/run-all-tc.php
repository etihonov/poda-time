
<?php

set_include_path(implode(PATH_SEPARATOR,
    array(
        get_include_path(),
        dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    )
));

include_once dirname(__FILE__) . '/'  . 'simpletest/autorun.php';
include_once dirname(__FILE__) . '/'  . 'simpletest/extensions/teamcity.php';

SimpleTest::prefer(new TeamCityTextReporter());

class PodaTimeAllTests extends TestSuite {
    function __construct() {
        parent::__construct();
        $this->collect(dirname(__FILE__) . '/',
            new SimplePatternCollector('/test-(.*?).php/'));
        $this->collect(dirname(__FILE__) . '/core',
            new SimplePatternCollector('/test-(.*?).php/'));
    }
}
