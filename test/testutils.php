<?php

include_once 'simpletest/unit_tester.php';
include_once 'simpletest/dumper.php';
include_once dirname(__FILE__) . '/'  . '../core/core.php';

/**
 * UnitTestCase assertion extensions
 */
class PTAssert {

    /**
     * @param UnitTestCase $parentTestCase
     * @param IPTEquatable $actual
     * @param IPTEquatable $expected
     * @param string $message
     * @return bool
     */
    public static function areEquals(UnitTestCase $parentTestCase,
                              IPTEquatable $actual, IPTEquatable $expected, $message = '%s') {
        $dumper = new SimpleDumper();
        $message = sprintf(
            $message,
            '[' . $dumper->describeValue($actual) .
                '] and [' . $dumper->describeValue($expected) .
                '] should be equals (by calling method equals())');
        return $parentTestCase->assertTrue(
            $actual->equals($expected) && $expected->equals($actual),
            $message);
    }
}

