<?php

include_once dirname(__FILE__) . '/'  . 'simpletest/autorun.php';
include_once dirname(__FILE__) . '/'  . 'core/core.php';
include_once dirname(__FILE__) . '/'  . 'core/containers.php';

class DummyClass implements IPTEquatable {

    public $a;

    /**
     * @param mixed $object
     * @return bool
     */
    function equals($object) {
        return $this === $object;
    }

    /**
     * @return string
     */
    function hashCode() {
        return CoreUtils::getObjectUniqueHashCode($this);
    }
}

class MapTests extends UnitTestCase {
    public function testAppendAndGetSimpleObject() {
        $map = new Map();
        $key1 = new DummyClass();
        $key2 = new DummyClass();
        $map->put($key1, 'value1');
        $map->put($key2, 'value2');

        $this->assertEqual('value1', $map->get($key1));
        $this->assertEqual('value2', $map->get($key2));
    }

    public function testPutSimpleObjectAsKeyTwice() {
        $map = new Map();
        $key1 = new DummyClass();
        $map->put($key1, 'value1');
        $map->put($key1, 'value2');

        $this->assertEqual('value2', $map->get($key1));
    }
}