<?php

include_once dirname(__FILE__) . '/'  . '../exceptions.php';
include_once dirname(__FILE__) . '/'  . 'datetime-formatter.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';

include_once dirname(__FILE__) . '/'  . 'datetime-printer.php';
include_once dirname(__FILE__) . '/'  . 'datetime-parser.php';
include_once dirname(__FILE__) . '/'  . 'datetime-parser-bucket.php';
include_once dirname(__FILE__) . '/'  . 'format-utils.php';

class DateTimeFormatterBuilder {
    private $formatter;
    private $elementPairs;

    public function __construct() {
        $this->formatter = 0;
        $this->elementPairs = array();
    }

    /**
     * @throws UnsupportedOperationException
     * @return DateTimeFormatter
     */
    public function toFormatter() {
        $f = $this->getFormatter();
        $printer = null;
        if ($this->isPrinter($f)) {
            $printer = $f;
        }
        $parser = null;
        if ($this->isParser($f)) {
            $parser = $f;
        }
        if ($printer != null || $parser != null) {
            return new DateTimeFormatter($printer, $parser);
        }
        throw new UnsupportedOperationException("Both printing and parsing not supported");
    }

    private function isPrinter($element) {
        if ($element instanceof IDateTimePrinter) {
            if ($element instanceof Composite) {
                return $element->isPrinter();
            }
            return true;
        }
        return false;
    }

    private function isParser($element) {
        if ($element instanceof IDateTimeParser) {
            if ($element instanceof Composite) {
                return $element->isParser();
            }
            return true;
        }
        return false;
    }


    public function appendMonthOfYear($minDigits) {
        return $this->appendDecimal(DateTimeFieldType::monthOfYear(), $minDigits, 2);
    }

    /**
     * @param mixed $element
     * @return DateTimeFormatterBuilder
     */
    private function append0($element) {
        $this->formatter = null;

        // Add the element as both a printer and parser.
        array_push($this->elementPairs, $element);
        array_push($this->elementPairs, $element);
        return $this;
    }

    private function getFormatter() {
        $result = $this->formatter;

        if ($result == null) {
            if (count($this->elementPairs) == 2) {
                $printer = $this->elementPairs[0];
                $parser = $this->elementPairs[1];

                if ($printer != null) {
                    if ($printer == $parser || $parser == null) {
                        $result = $printer;
                    }
                } else {
                    $result = $parser;
                }
            }

            if ($result == null) {
                $result = new Composite($this->elementPairs);
            }
        }

        return $result;
    }

    /**
     * Instructs the printer to emit a field value as a decimal number, and the
     * parser to expect an unsigned decimal number.
     *
     * @param DateTimeFieldType $fieldType  type of field to append
     * @param int $minDigits  minimum number of digits to <i>print</i>
     * @param int $maxDigits  maximum number of digits to <i>parse</i>, or the estimated
     * maximum number of digits to print
     * @return DateTimeFormatterBuilder this DateTimeFormatterBuilder, for chaining
     * @throws IllegalArgumentException if field type is null
     */
    public function appendDecimal(DateTimeFieldType $fieldType, $minDigits, $maxDigits) {
        if ($fieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if ($maxDigits < $minDigits) {
            $maxDigits = $minDigits;
        }
        if ($minDigits < 0 || $maxDigits <= 0) {
            throw new IllegalArgumentException();
        }
        if ($minDigits <= 1) {
            return $this->append0(new UnpaddedNumber($fieldType, $maxDigits, false));
        } else {
            return $this->append0(new PaddedNumber($fieldType, $maxDigits, false, $minDigits));
        }
    }


    /**
     * Instructs the printer to emit a field value as a decimal number, and the
     * parser to expect a signed decimal number.
     *
     * @param DateTimeFieldType $fieldType  type of field to append
     * @param int $minDigits  minimum number of digits to <i>print</i>
     * @param int $maxDigits  maximum number of digits to <i>parse</i>, or the estimated
     * maximum number of digits to print
     * @return DateTimeFormatterBuilder, for chaining
     * @throws IllegalArgumentException if field type is null
     */
    public function appendSignedDecimal(DateTimeFieldType $fieldType, $minDigits, $maxDigits) {
        if ($fieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if ($maxDigits < $minDigits) {
            $maxDigits = $minDigits;
        }
        if ($minDigits < 0 || $maxDigits <= 0) {
            throw new IllegalArgumentException();
        }
        if ($minDigits <= 1) {
            return $this->append0(new UnpaddedNumber($fieldType, $maxDigits, true));
        } else {
            return $this->append0(new PaddedNumber($fieldType, $maxDigits, true, $minDigits));
        }
    }

    public function appendYear($minDigits, $maxDigits) {
        return $this->appendSignedDecimal(DateTimeFieldType::year(), $minDigits, $maxDigits);
    }

    public function appendLiteral($literal) {
        if ($literal == null) {
            throw new IllegalArgumentException("Literal must not be null");
        }
        switch (strlen($literal)) {
            case 0:
                return $this;
            /*case 1:
                return $this->append0(new CharacterLiteral($literal[0]));*/
            default:
                return $this->append0(new StringLiteral($literal));
        }
    }

    /**
     * @param $minDigits
     * @return DateTimeFormatterBuilder
     */
    public function appendDayOfMonth($minDigits) {
        return self::appendDecimal(DateTimeFieldType::dayOfMonth(), $minDigits, 2);
    }

    /**
     * @param $minDigits
     * @return DateTimeFormatterBuilder
     */
    public function appendHourOfDay($minDigits) {
        return $this->appendDecimal(DateTimeFieldType::hourOfDay(), $minDigits, 2);
    }

    /**
     * Instructs the printer to emit a numeric minuteOfHour field.
     *
     * @param int $minDigits  minimum number of digits to print
     * @return DateTimeFormatterBuilder this DateTimeFormatterBuilder, for chaining
     */
    public function appendMinuteOfHour($minDigits) {
        return $this->appendDecimal(DateTimeFieldType::minuteOfHour(), $minDigits, 2);
    }

    /**
     * Instructs the printer to emit a numeric secondOfMinute field.
     *
     * @param int $minDigits  minimum number of digits to print
     * @return DateTimeFormatterBuilder $this DateTimeFormatterBuilder, for chaining
     */
    public function appendSecondOfMinute($minDigits) {
        return $this->appendDecimal(DateTimeFieldType::secondOfMinute(), $minDigits, 2);
    }

    /**
     * Instructs the printer to emit a short locale-specific dayOfWeek
     * text. The parser will accept a long or short dayOfWeek text,
     * case-insensitive.
     *
     * @return DateTimeFormatterBuilder  this DateTimeFormatterBuilder, for chaining
     */
    public function appendDayOfWeekShortText() {
        return $this->appendShortText(DateTimeFieldType::dayOfWeek());
    }

    /**
     * Instructs the printer to emit a field value as short text, and the
     * parser to expect text.
     *
     * @param DateTimeFieldType $fieldType  type of field to append
     * @return DateTimeFormatterBuilder  this DateTimeFormatterBuilder, for chaining
     * @throws IllegalArgumentException if field type is null
     */
    public function appendShortText(DateTimeFieldType $fieldType) {
        if ($fieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        return $this->append0(new TextField($fieldType, true));
    }
}

abstract class NumberFormatter implements IDateTimePrinter, IDateTimeParser {

    /** @var \DateTimeFieldType */
    protected $fieldType;
    /** @var int */
    protected $maxParsedDigits;
    /** @var int */
    protected $signed;

    /**
     * @param DateTimeFieldType $fieldType
     * @param int $maxParsedDigits
     * @param int $signed
     */
    protected function __construct(DateTimeFieldType $fieldType, $maxParsedDigits, $signed) {
        $this->fieldType = $fieldType;
        $this->maxParsedDigits = $maxParsedDigits;
        $this->signed = $signed;
    }

    public function estimateParsedLength() {
        return $this->maxParsedDigits;
    }

    /**
     * @param DateTimeParserBucket $bucket
     * @param string $text
     * @param int $position
     * @return int
     */
    public function parseInto(DateTimeParserBucket $bucket, $text, $position) {
        // TODO
        return 0;
    }
}

class UnpaddedNumber extends NumberFormatter {
    public function __construct(DateTimeFieldType $fieldType, $maxParsedDigits, $signed) {
        parent::__construct($fieldType, $maxParsedDigits, $signed);
    }

    /**
     * @return int
     */
    function estimatePrintedLength() {
        return $this->maxParsedDigits;
    }

    /**
     * Prints an instant from milliseconds since 1970-01-01T00:00:00Z,
     * using the given Chronology.
     *
     * @param string $string  formatted instant is appended to this buffer, not null
     * @param int $millis  millis since 1970-01-01T00:00:00Z
     * @param Chronology $chrono  the chronology to use, not null
     * @param int $displayOffset  if a time zone offset is printed, force it to use this millisecond value
     * @param PTDateTimeZone $displayZone  the time zone to use, null means local time
     * @param PTLocale $locale  the locale to use, null means default locale
     */
    function printToString(&$string, $millis, Chronology $chrono = null,
                           $displayOffset, PTDateTimeZone $displayZone = null, PTLocale $locale = null) {
        $field = $this->fieldType->getField($chrono);
        FormatUtils::appendUnpaddedInteger($string, $field->get($millis));
    }
}

class PaddedNumber extends NumberFormatter {
    /** @var int */
    protected $minPrintedDigits;

    /**
     * @param DateTimeFieldType $fieldType
     * @param int $maxParsedDigits
     * @param int $signed
     * @param int $minPrintedDigits
     */
    public function __construct(DateTimeFieldType $fieldType, $maxParsedDigits, $signed, $minPrintedDigits) {
        parent::__construct($fieldType, $maxParsedDigits, $signed);
        $this->minPrintedDigits = $minPrintedDigits;
    }

    /**
     * @return int
     */
    function estimatePrintedLength() {
        return $this->maxParsedDigits;
    }

    public function printToString(&$string, $millis, Chronology $chrono = null, $displayOffset, PTDateTimeZone $displayZone = null, PTLocale $locale = null) {
        try {
            $field = $this->fieldType->getField($chrono);
            FormatUtils::appendPaddedInteger($string, $field->get($millis), $this->minPrintedDigits);
        } catch ( /*RuntimeException*/ Exception $e) {
            //appendUnknownString(buf, iMinPrintedDigits);
        }
    }
}

/**
 * Printer and parser for textual values in formatted datetime
 */
class TextField implements IDateTimePrinter, IDateTimeParser {

    /** @var \DateTimeFieldType */
    private $fieldType;

    /** @var bool */
    private $short;

    /**
     * @param DateTimeFieldType $fieldType
     * @param bool $isShort
     */
    public function __construct(DateTimeFieldType $fieldType, $isShort) {
        $this->fieldType = $fieldType;
        $this->short = $isShort;
    }

    /**
     * @return int
     */
    function estimateParsedLength() {
        return $this->estimatePrintedLength();
    }

    /**
     * @param DateTimeParserBucket $bucket
     * @param string $text
     * @param int $position
     * @return int
     */
    function parseInto(DateTimeParserBucket $bucket, $text, $position) {
        // TODO: Implement parseInto() method.
    }

    /**
     * @return int
     */
    function estimatePrintedLength() {
        return $this->short ? 6 : 20;
    }

    /**
     * @param int $millis
     * @param Chronology $chrono
     * @param PTLocale $locale
     * @return string
     */
    private function getAsString($millis, Chronology $chrono = null, PTLocale $locale = null) {
        $field = $this->fieldType->getField($chrono);
        if ($this->short) {
            return $field->getAsShortTextFromInstant($millis, $locale);
        } else {
            return $field->getAsText($millis, $locale);
        }
    }

    /**
     * Prints an instant from milliseconds since 1970-01-01T00:00:00Z,
     * using the given Chronology.
     *
     * @param string $string  formatted instant is appended to this buffer, not null
     * @param int $millis  millis since 1970-01-01T00:00:00Z
     * @param Chronology $chrono  the chronology to use, not null
     * @param int $displayOffset  if a time zone offset is printed, force it to use this millisecond value
     * @param PTDateTimeZone $displayZone  the time zone to use, null means local time
     * @param PTLocale $locale  the locale to use, null means default locale
     */
    function printToString(&$string, $millis, Chronology $chrono = null,
                           $displayOffset, PTDateTimeZone $displayZone = null, PTLocale $locale = null) {
        $string .= $this->getAsString($millis, $chrono, $locale);
    }
}

class StringLiteral implements IDateTimePrinter, IDateTimeParser {
    private $value;

    public function __construct($value) {
        $this->value = $value;
    }

    /**
     * @return int
     */
    function estimateParsedLength() {
        return strlen($this->value);
    }

    /**
     * @param DateTimeParserBucket $bucket
     * @param string $text
     * @param int $position
     * @return int
     */
    function parseInto(DateTimeParserBucket $bucket, $text, $position) {
        // TODO: Implement parseInto() method.
    }

    /**
     * @return int
     */
    function estimatePrintedLength() {
        return strlen($this->value);
    }

    /**
     * Prints an instant from milliseconds since 1970-01-01T00:00:00Z,
     * using the given Chronology.
     *
     * @param string $string  formatted instant is appended to this buffer, not null
     * @param int $millis  millis since 1970-01-01T00:00:00Z
     * @param Chronology $chrono  the chronology to use, not null
     * @param int $displayOffset  if a time zone offset is printed, force it to use this millisecond value
     * @param PTDateTimeZone $displayZone  the time zone to use, null means local time
     * @param PTLocale $locale  the locale to use, null means default locale
     */
    function printToString(&$string, $millis, Chronology $chrono = null,
                           $displayOffset, PTDateTimeZone $displayZone = null, PTLocale $locale = null) {
        $string .= $this->value;
    }
}

class Composite implements IDateTimePrinter, IDateTimeParser {

    /** @var IDateTimePrinter[] */
    private $printers;

    /** @var IDateTimeParser[] */
    private $parsers;

    private $printedLengthEstimate;
    private $parsedLengthEstimate;

    /**
     * @param array $elementPairs
     */
    public function __construct($elementPairs) {
        $printerList = array();
        $parserList = array();

        $this->decompose($elementPairs, $printerList, $parserList);

        $this->printers = $printerList;
        $this->parsers = $parserList;

        /*if ($printerList.size() <= 0) {
            iPrinters = null;
            iPrintedLengthEstimate = 0;
        } else {
                int size = printerList.size();
                iPrinters = new DateTimePrinter[size];
                int printEst = 0;
                for (int i=0; i<size; i++) {
                    DateTimePrinter printer = (DateTimePrinter) printerList.get(i);
                    printEst += printer.estimatePrintedLength();
                    iPrinters[i] = printer;
                }
                iPrintedLengthEstimate = printEst;
            }

        if (parserList.size() <= 0) {
            iParsers = null;
            iParsedLengthEstimate = 0;
        } else {
                int size = parserList.size();
                iParsers = new DateTimeParser[size];
                int parseEst = 0;
                for (int i=0; i<size; i++) {
                    DateTimeParser parser = (DateTimeParser) parserList.get(i);
                    parseEst += parser.estimateParsedLength();
                    iParsers[i] = parser;
                }
                iParsedLengthEstimate = parseEst;
            }*/
    }

    /**
     * @param array $elementPairs
     * @param IDateTimePrinter[] $printerList
     * @param IDateTimeParser[] $parserList
     */
    private function decompose($elementPairs, &$printerList, &$parserList) {
        $size = count($elementPairs);
        for ($i = 0; $i < $size; $i += 2) {
            $element = $elementPairs[$i];
            if ($element instanceof IDateTimePrinter) {
                if ($element instanceof Composite) {
                    foreach ($element->printers as $printer)
                        array_push($printerList, $printer);
                } else {
                    array_push($printerList, $element);
                }
            }

            $element = $elementPairs[$i + 1];
            if ($element instanceof IDateTimeParser) {
                if ($element instanceof Composite) {
                    foreach ($element->parsers as $parser)
                        array_push($parserList, $parser);
                } else {
                    array_push($parserList, $element);
                }
            }
        }
    }

    /**
     * @return int
     */
    function estimateParsedLength() {
        // TODO: Implement estimateParsedLength() method.
    }

    /**
     * @param DateTimeParserBucket $bucket
     * @param string $text
     * @param int $position
     * @return int
     */
    function parseInto(DateTimeParserBucket $bucket, $text, $position) {
        // TODO: Implement parseInto() method.
    }

    /**
     * @return int
     */
    function estimatePrintedLength() {
        // TODO: Implement estimatePrintedLength() method.
    }

    public function isPrinter() {
        return $this->printers != null;
    }

    public function isParser() {
        return $this->parsers != null;
    }

    public function printToString(&$string, $millis, Chronology $chrono = null, $displayOffset, PTDateTimeZone $displayZone = null, PTLocale $locale = null) {
        $elements = $this->printers;
        if ($elements == null) {
            throw new UnsupportedOperationException();
        }

        /* TODO
         if (locale == null) {
            // Guard against default locale changing concurrently.
            locale = Locale.getDefault();
        }*/

        $len = count($elements);
        for ($i = 0; $i < $len; $i++) {
            $elements[$i]->printToString($string, $millis, $chrono, $displayOffset, $displayZone, $locale);
        }

    }

}