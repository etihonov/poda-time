<?php

include_once dirname(__FILE__) . '/'  . '../chronology.php';
include_once dirname(__FILE__) . '/'  . '../datetime-zone.php';
include_once dirname(__FILE__) . '/'  . '../locale.php';

interface IDateTimePrinter {

    /**
     * @return int
     */
    function estimatePrintedLength();

    /**
     * Prints an instant from milliseconds since 1970-01-01T00:00:00Z,
     * using the given Chronology.
     *
     * @param string $string  formatted instant is appended to this buffer, not null
     * @param int $millis  millis since 1970-01-01T00:00:00Z
     * @param Chronology $chrono  the chronology to use, not null
     * @param int $displayOffset  if a time zone offset is printed, force it to use this millisecond value
     * @param PTDateTimeZone $displayZone  the time zone to use, null means local time
     * @param PTLocale $locale  the locale to use, null means default locale
     */
    function printToString(&$string, $millis, Chronology $chrono = null,
        $displayOffset, PTDateTimeZone $displayZone = null, PTLocale $locale = null);


    /*
    void printTo(StringBuffer buf, long instant, Chronology chrono, int displayOffset, PTDateTimeZone displayZone, Locale locale);
    void printTo(Writer out, long instant, Chronology chrono,
    int displayOffset, PTDateTimeZone displayZone, Locale locale) throws IOException;
    void printTo(StringBuffer buf, ReadablePartial partial, Locale locale);
    void printTo(Writer out, ReadablePartial partial, Locale locale) throws IOException;
    */
}
