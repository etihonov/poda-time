<?php

include_once dirname(__FILE__) . '/'  . 'datetime-formatter.php';
include_once dirname(__FILE__) . '/'  . 'datetime-formatter-builder.php';

class DateTimeFormat {

    private static $patternedCache;

    public static function staticInit() {
        self::$patternedCache = array();
    }

    /**
     * Factory to create a formatter from a pattern string.
     * The pattern string is described above in the class level javadoc.
     * It is very similar to SimpleDateFormat patterns.
     * <p>
     * The format may contain locale specific output, and this will change as
     * you change the locale of the formatter.
     * Call {@link DateTimeFormatter#withLocale(Locale)} to switch the locale.
     * For example:
     * <pre>
     * DateTimeFormat::forPattern(pattern)->withLocale(Locale::FRANCE)->print(dt);
     * </pre>
     *
     * @param string $pattern  pattern specification
     * @return DateTimeFormatter the formatter
     */
    public static function forPattern($pattern) {
        return self::createFormatterForPattern($pattern);
    }


    /**
     * Select a format from a custom pattern.
     *
     * @param string $pattern  pattern specification
     * @return DateTimeFormatter
     * @throws IllegalArgumentException
     */
    private static function createFormatterForPattern($pattern) {
        if ($pattern == null || count($pattern) == 0) {
            throw new IllegalArgumentException("Invalid pattern specification");
        }

        $formatter = isset(self::$patternedCache[$pattern]) ? self::$patternedCache[$pattern] : null;
        if ($formatter == null) {
            $builder = new DateTimeFormatterBuilder();
            self::parsePatternTo($builder, $pattern);
            $formatter = $builder->toFormatter();

            self::$patternedCache[$pattern] = $formatter;
        }
        return $formatter;
    }

    /**
     * Parses the given pattern and appends the rules to the given
     * DateTimeFormatterBuilder.
     *
     * @param DateTimeFormatterBuilder $builder
     * @param string $pattern  pattern specification
     * @throws IllegalArgumentException
     */
    private static function parsePatternTo(DateTimeFormatterBuilder $builder, $pattern) {
        $length = strlen($pattern);
        $indexRef = array();

        for ($i = 0; $i < $length; $i++) {
            $indexRef[0] = $i;
            $token = self::parseToken($pattern, $indexRef);
            $i = $indexRef[0];

            $tokenLen = strlen($token);

            if ($tokenLen == 0) {
                break;
            }

            $c = $token[0];

            switch ($c) {
/*                case 'G': // era designator (text)
                    //$builder->appendEraText();
                    break;
                case 'C': // century of era (number)
                    //builder.appendCenturyOfEra(tokenLen, tokenLen);
                    break;*/
                case 'x': // weekyear (number)
                case 'y': // year (number)
                case 'Y': // year of era (number)
                    if ($tokenLen == 2) {
                        $lenientParse = true;

                        // Peek ahead to next token.
                        if ($i + 1 < $length) {
                            $indexRef[0]++;
                            if (self::isNumericToken(self::parseToken($pattern, $indexRef))) {
                                // If next token is a number, cannot support
                                // lenient parse, because it will consume digits
                                // that it should not.
                                $lenientParse = false;
                            }
                            $indexRef[0]--;
                        }

                        // Use pivots which are compatible with SimpleDateFormat.
                        switch ($c) {
                            case 'x':
                                //$builder->appendTwoDigitWeekyear((new PTDateTime())->getWeekyear() - 30, $lenientParse);
                                break;
                            case 'y':
                            case 'Y':
                            default:
                                //$builder->appendTwoDigitYear((new PTDateTime())->getYear() - 30, $lenientParse);
                                break;
                        }
                    }
                    else {
                            // Try to support long year values.
                        $maxDigits = 9;

                        // Peek ahead to next token.
                        if ($i + 1 < $length) {
                            $indexRef[0]++;
                            if (self::isNumericToken(self::parseToken($pattern, $indexRef))) {
                                // If next token is a number, cannot support long years.
                                $maxDigits = $tokenLen;
                            }
                            $indexRef[0]--;
                        }

                        switch ($c) {
                            case 'x':
                                $builder->appendWeekyear($tokenLen, $maxDigits);
                                break;
                            case 'y':
                                $builder->appendYear($tokenLen, $maxDigits);
                                break;
                            case 'Y':
                                $builder->appendYearOfEra($tokenLen, $maxDigits);
                                break;
                        }
                    }
                    break;
                case 'M': // month of year (text and number)
                    if ($tokenLen >= 3) {
                        if ($tokenLen >= 4) {
                            $builder->appendMonthOfYearText();
                        } else {
                            $builder->appendMonthOfYearShortText();
                        }
                    } else {
                        $builder->appendMonthOfYear($tokenLen);
                    }
                    break;
                case 'd': // day of month (number)
                    $builder->appendDayOfMonth($tokenLen);
                    break;
                /*case 'a': // am/pm marker (text)
                    builder.appendHalfdayOfDayText();
                    break;
                case 'h': // clockhour of halfday (number, 1..12)
                    builder.appendClockhourOfHalfday(tokenLen);
                    break;*/
                case 'H': // hour of day (number, 0..23)
                    $builder->appendHourOfDay($tokenLen);
                    break;
                /*case 'k': // clockhour of day (1..24)
                    builder.appendClockhourOfDay(tokenLen);
                    break;
                case 'K': // hour of halfday (0..11)
                    builder.appendHourOfHalfday(tokenLen);
                    break;
                */
                case 'm': // minute of hour (number)
                    $builder->appendMinuteOfHour($tokenLen);
                    break;
                case 's': // second of minute (number)
                    $builder->appendSecondOfMinute($tokenLen);
                    break;
                /*case 'S': // fraction of second (number)
                    builder.appendFractionOfSecond(tokenLen, tokenLen);
                    break;
                case 'e': // day of week (number)
                    builder.appendDayOfWeek(tokenLen);
                    break; */
                case 'E': // dayOfWeek (text)
                    if ($tokenLen >= 4) {
                        $builder->appendDayOfWeekText();
                    } else {
                        $builder->appendDayOfWeekShortText();
                    }
                    break;
                /* case 'D': // day of year (number)
                    builder.appendDayOfYear(tokenLen);
                    break;
                case 'w': // week of weekyear (number)
                    builder.appendWeekOfWeekyear(tokenLen);
                    break;
                case 'z': // time zone (text)
                    if (tokenLen >= 4) {
                        builder.appendTimeZoneName();
                    } else {
                        builder.appendTimeZoneShortName();
                    }
                    break;
                case 'Z': // time zone offset
                    if (tokenLen == 1) {
                        builder.appendTimeZoneOffset(null, "Z", false, 2, 2);
                    } else if (tokenLen == 2) {
                        builder.appendTimeZoneOffset(null, "Z", true, 2, 2);
                    } else {
                        builder.appendTimeZoneId();
                    }
                    break;   */
                case '\'': // literal text
                    $sub = substr($token, 1);
                    if (strlen($sub) == 1) {
                        $builder->appendLiteral($sub[0]);
                    } else {
                        // Create copy of sub since otherwise the temporary quoted
                        // string would still be referenced internally.
                        $builder->appendLiteral($sub);
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Illegal pattern component: " . $token);
            }
        }
    }

    /**
     * Parses an individual token.
     *
     * @param string $pattern  the pattern string
     * @param int[] $indexRef  a single element array, where the input is the start location and the output is the location after parsing the token
     * @return string the parsed token
     */
    private static function parseToken($pattern, &$indexRef) {
        $buf = '';

        $i = $indexRef[0];
        $length = strlen($pattern);

        $c = $pattern[$i];
        if ($c >= 'A' && $c <= 'Z' || $c >= 'a' && $c <= 'z') {
            // Scan a run of the same character, which indicates a time
            // pattern.
            $buf .= $c;

            while ($i + 1 < $length) {
                $peek = $pattern[$i + 1];
                if ($peek == $c) {
                    $buf .= $c;
                    $i++;
                }
                else {
                    break;
                }
            }
        }
        else {

            // This will identify token as text.
            $buf .=  '\'';

            $inLiteral = false;

            for (; $i < $length; $i++) {
                $c = $pattern[$i];

                if ($c == '\'') {
                    if ($i + 1 < $length && $pattern[$i + 1] == '\'') {
                        // '' is treated as escaped '
                        $i++;
                        $buf .= $c;
                    } else {
                        $inLiteral = !$inLiteral;
                    }
                }
                else if (!$inLiteral &&
                    ($c >= 'A' && $c <= 'Z' || $c >= 'a' && $c <= 'z')) {
                    $i--;
                    break;
                } else {
                    $buf .= $c;
                }
            }
        }

        $indexRef[0] = $i;
        return $buf;
    }


    /**
     * Returns true if token should be parsed as a numeric field.
     *
     * @param string $token  the token to parse
     * @return true if numeric field
     */
    private static function isNumericToken($token) {
        $tokenLen = strlen($token);
        if ($tokenLen > 0) {
            $c = $token[0];
            switch ($c) {
                case 'c': // century (number)
                case 'C': // century of era (number)
                case 'x': // weekyear (number)
                case 'y': // year (number)
                case 'Y': // year of era (number)
                case 'd': // day of month (number)
                case 'h': // hour of day (number, 1..12)
                case 'H': // hour of day (number, 0..23)
                case 'm': // minute of hour (number)
                case 's': // second of minute (number)
                case 'S': // fraction of second (number)
                case 'e': // day of week (number)
                case 'D': // day of year (number)
                case 'F': // day of week in month (number)
                case 'w': // week of year (number)
                case 'W': // week of month (number)
                case 'k': // hour of day (1..24)
                case 'K': // hour of day (0..11)
                    return true;
                case 'M': // month of year (text and number)
                    if ($tokenLen <= 2) {
                        return true;
                    }
            }
        }
        return false;
    }

}