<?php

include_once dirname(__FILE__) . '/'  . 'datetime-parser-bucket.php';

interface IDateTimeParser {

    /**
     * @return int
     */
    function estimateParsedLength();

    /**
     * @param DateTimeParserBucket $bucket
     * @param string $text
     * @param int $position
     * @return int
     */
    function parseInto(DateTimeParserBucket $bucket, $text, $position);

}
