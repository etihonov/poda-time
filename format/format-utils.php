<?php

class FormatUtils {

    /**
     * @param string $string
     * @param int $value
     * @param int $size
     */
    public static function appendPaddedInteger(&$string, $value, $size) {
        $string .= str_pad($value, $size, '0', STR_PAD_LEFT);
    }

    /**
     * @param string $string
     * @param int $value
     */
    public static function appendUnpaddedInteger(&$string, $value) {
        $string .= $value;
    }
}
