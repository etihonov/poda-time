<?php

include_once dirname(__FILE__) . '/'  . 'datetime-parser.php';
include_once dirname(__FILE__) . '/'  . 'datetime-printer.php';
include_once dirname(__FILE__) . '/'  . '../readable-instant.php';
include_once dirname(__FILE__) . '/'  . '../locale.php';

class DateTimeFormatter {
    /** @var \IDateTimePrinter */
    private $printer;

    /** @var \IDateTimeParser */
    private $parser;

    /** @var \PTLocale */
    private $locale;

    /**
     * @param IDateTimePrinter $printer
     * @param IDateTimeParser $parser
     * @param PTLocale $locale
     */
    public function __construct(IDateTimePrinter $printer, IDateTimeParser $parser, PTLocale $locale = null) {
        $this->printer = $printer;
        $this->parser = $parser;
        $this->locale = $locale;
    }

    /**
     * Gets the locale that will be used for printing and parsing.
     *
     * @return PTLocale the locale to use; if null, formatter uses default locale at
     * invocation time
     */
    public function getLocale() {
        return $this->locale;
    }

    public function printInstantToString(&$string, IReadableInstant $instant) {
        $millis = DateTimeUtils::getInstantMillis($instant);
        $chrono = DateTimeUtils::getInstantChronology($instant);
        $this->printMillisToString($string, $millis, $chrono, null, null, $this->locale);
    }

    public function printInstant(IReadableInstant $instant) {
        $result = '';
        $this->printInstantToString($result, $instant);
        return $result;
    }

    /**
     * @param string $string
     * @param int $millis
     * @param Chronology $chrono
     */
    private function printMillisToString(&$string, $millis, $chrono, $offset, $zone, $locale) {
        $printer = $this->requirePrinter();

        /* TODO : Make this snippet work
        $chrono = $this->selectChronology(chrono);
        // Shift instant into local time (UTC) to avoid excessive offset
        // calculations when printing multiple fields in a composite printer.
        $zone = $chrono->getZone();
        $offset = $zone->getOffset($instant);
        $adjustedInstant = $instant + $offset;
        if (($instant ^ $adjustedInstant) < 0 && ($instant ^ $offset) >= 0) {
            // Time zone offset overflow, so revert to UTC.
            $zone = PTDateTimeZone::UTC;
            $offset = 0;
            $adjustedInstant = $instant;
        }
        */

        $printer->printToString($string, $millis, $chrono, $offset, $zone, $locale);
    }

    /**
     * Checks whether printing is supported.
     *
     * @throws UnsupportedOperationException if printing is not supported
     * @return IDateTimePrinter
     */
    private function requirePrinter() {
        $printer = $this->printer;
        if ($printer == null) {
            throw new UnsupportedOperationException("Printing not supported");
        }
        return $printer;
    }

    /**
     * Returns a new formatter with a different locale that will be used
     * for printing and parsing.
     * <p>
     * A DateTimeFormatter is immutable, so a new instance is returned,
     * and the original is unaltered and still usable.
     *
     * @param PTLocale $locale the locale to use; if null, formatter uses default locale
     * at invocation time
     * @return DateTimeFormatter the new formatter
     */
    public function withLocale(PTLocale $locale) {
        if ($locale === $this->getLocale() || ($locale != null && $locale->equals($this->getLocale()))) {
            return $this;
        }
        return new DateTimeFormatter($this->printer, $this->parser, $locale);
    }
}