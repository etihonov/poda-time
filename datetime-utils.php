<?php

include_once dirname(__FILE__) . '/'  . 'datetime-constants.php';

interface IMillisProvider {

    /**
     * @return int
     */
    function getMillis();
}

class SystemMillisProvider implements IMillisProvider {

    /**
     * Gets the current time.
     * @return int the current time in millis
     */
    public final function getMillis() {
        return time() * DateTimeConstants::$MILLIS_PER_SECOND;
    }
}

/**
 * Fixed millisecond provider.
 */
class FixedMillisProvider implements IMillisProvider {

    /** The fixed millis value. */
    private $millis;

    /**
     * Constructor.
     * @param int $fixedMillis the millis offset
     */
    public function __construct($fixedMillis) {
        $this->millis = $fixedMillis;
    }

    /**
     * Gets the current time.
     * @return int the current time in millis
     */
    public function getMillis() {
        return $this->millis;
    }
}


class DateTimeUtils {
    /**
     * @var SystemMillisProvider
     */
    private static $systemMillisProvider;

    /**
     * @var IMillisProvider/
     */
    private static $currentMillisProvider;

    public static function staticInit() {
        self::$systemMillisProvider = new SystemMillisProvider();
        self::$currentMillisProvider = self::$systemMillisProvider;
    }

    public static function currentTimeMillis() {
        return self::$currentMillisProvider->getMillis();
    }

    public static function setCurrentMillisFixed($fixedMillis) {
        // TODO self::checkPermission();
        self::$currentMillisProvider = new FixedMillisProvider($fixedMillis);
    }

    public static function setCurrentMillisSystem() {
        // TODO self::checkPermission();
        self::$currentMillisProvider = self::$systemMillisProvider;
    }

    /**
     * @param IReadableInstant $instant
     * @return int
     */
    public static function getInstantMillis(IReadableInstant $instant = null) {
        if ($instant == null) {
            return DateTimeUtils::currentTimeMillis();
        }
        return $instant->getMillis();
    }

    /**
     * Gets the chronology from the specified instant object handling null.
     * <p>
     * If the instant object is <code>null</code>, or the instant's chronology is
     * <code>null</code>, {@link ISOChronology#getInstance()} will be returned.
     * Otherwise, the chronology from the object is returned.
     *
     * @param IReadableInstant $instant  the instant to examine, null means ISO in the default zone
     * @return Chronology the chronology, never null
     */
    public static function getInstantChronology(IReadableInstant $instant = null) {
        if ($instant == null) {
            return null; // TODO: ISOChronology.getInstance();
        }
        $chrono = $instant->getChronology();
        if ($chrono == null) {
            return null; // TODO: ISOChronology.getInstance();
        }
        return $chrono;
    }

    public static function getChronology(Chronology $chronology = null) {
        if ($chronology == null) {
            return ISOChronology::getInstance();
        }
        return $chronology;
    }
}
DateTimeUtils::staticInit();