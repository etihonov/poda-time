<?php

include_once dirname(__FILE__) . '/'  . '../exceptions.php';
include_once dirname(__FILE__) . '/'  . '../readable-instant.php';
include_once dirname(__FILE__) . '/'  . '../field/fields-utils.php';

/**
 * AbstractInstant provides the common behaviour for instant classes.
 * <p>
 * This class has no concept of a chronology, all methods work on the
 * millisecond instant.
 * <p>
 * This class should generally not be used directly by API users. The
 * {@link ReadableInstant} interface should be used when different
 * kinds of date/time objects are to be referenced.
 * <p>
 * Whenever you want to implement <code>ReadableInstant</code> you should
 * extend this class.
 * <p>
 * AbstractInstant itself is immutable, but subclasses may be mutable.
 */
abstract class AbstractInstant implements IReadableInstant {

    /**
     * Empty constructor
     */
    public function __construct() {
    }

    /**
     * Output the date time in ISO8601 format (yyyy-MM-ddTHH:mm:ss.SSSZZ).
     * @return string ISO8601 time formatted string.
     */
    public function toString() {
        throw new UnimplementedOperationException();
        //return ISODateTimeFormat.dateTime().print(this);
    }

    /**
     * Gets the time zone of the instant from the chronology.
     * @return PTDateTimeZone the DateTimeZone that the instant is using, never null
     */
    public function getZone() {
        return $this->getChronology()->getZone();
    }

    /**
     * Compares this object with the specified object for equality based
     * on the millisecond instant, chronology and time zone.
     * <p>
     * Two objects which represent the same instant in time, but are in
     * different time zones (based on time zone id), will be considered to
     * be different. Only two objects with the same {@link DateTimeZone},
     * {@link Chronology} and instant are equal.
     * <p>
     * See {@link #isEqual(ReadableInstant)} for an equals method that
     * ignores the Chronology and time zone.
     * <p>
     * All ReadableInstant instances are accepted.
     *
     * @param mixed $readableInstant  a readable instant to check against
     * @return bool true if millisecond and chronology are equal, false if
     *  not or the instant is null or of an incorrect type
     */
    public function equals($readableInstant) {
        // must be to fulfil ReadableInstant contract
        if ($this === $readableInstant) {
            return true;
        }
        if (!($readableInstant instanceof IReadableInstant)) {
            return false;
        }
        return ($this->getMillis() == $readableInstant->getMillis()) &&
            FieldUtils::equals($this->getChronology(), $readableInstant->getChronology());
    }

    /**
     * Gets a hash code for the instant as defined in <code>IReadableInstant</code>.
     *
     * @return string a suitable hash code
     */
    function hashCode() {
        return
            $this->getMillis() .
            ($this->getChronology()->hashCode());
    }

    /**
     * Compares this object with the specified object for ascending
     * millisecond instant order. This ordering is inconsistent with
     * equals, as it ignores the Chronology.
     * <p>
     * All IReadableInstant instances are accepted.
     *
     * @param IReadableInstant|null $other  a readable instant to check against
     * @return int negative value if this is less, 0 if equal, or positive value if greater
     * @throws NullPointerException
     */
    public function compareTo(IReadableInstant $other = null) {
        if ($other == null) {
            throw new NullPointerException();
        }
        if ($this === $other) {
            return 0;
        }

        $otherMillis = $other->getMillis();
        $thisMillis = $this->getMillis();

        // cannot do (thisMillis - otherMillis) as can overflow
        if ($thisMillis == $otherMillis) {
            return 0;
        }
        if ($thisMillis < $otherMillis) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Is this instant equal to the instant passed in
     * comparing solely by millisecond.
     *
     * @param null|int|IReadableInstant $instant  an instant to check against, null means now
     * @return bool true if the instant is equal to the instant passed in
     * @throws InvalidArgumentException
     */
    public final function isEqual($instant) {
        if (is_object($instant) && $instant instanceof IReadableInstant) {
            return $this->isEqualToInstant($instant);
        }
        else if (is_numeric($instant)) {
            return $this->isEqualToMillis(floatval($instant));
        }
        else if ($instant == null) {
            return $this->isEqualToMillis(DateTimeUtils::getInstantMillis($instant));
        }
        else {
            throw new InvalidArgumentException('$instant must be a numeric, IReadableInstant or null value');
        }
    }

    /**
     * Is this instant equal to the current instant
     * comparing solely by millisecond.
     *
     * @return bool true if this instant is before the current instant
     */
    public function isEqualNow() {
        return $this->isEqual(DateTimeUtils::currentTimeMillis());
    }
    /**
     * @param IReadableInstant $instant
     * @return bool
     */
    private function isEqualToInstant(IReadableInstant $instant) {
        return $this->isEqualToMillis(DateTimeUtils::getInstantMillis($instant));
    }

    /**
     * @param $millis
     * @return bool
     */
    private function isEqualToMillis($millis) {
        return $this->getMillis() == $millis;
    }

}