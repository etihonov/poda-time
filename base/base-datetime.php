<?php

include_once dirname(__FILE__) . '/'  . 'abstract-datetime.php';
include_once dirname(__FILE__) . '/'  . '../datetime-utils.php';
include_once dirname(__FILE__) . '/'  . '../chronology.php';
include_once dirname(__FILE__) . '/'  . '../chrono/gregorian-chronology.php';
include_once dirname(__FILE__) . '/'  . '../chrono/iso-chronology.php';

/**
 * BaseDateTime is an abstract implementation of IReadableDateTime that stores
 * data in <code>long</code> and <code>Chronology</code> fields.
 * <p>
 * This class should generally not be used directly by API users.
 * The {@link IReadableDateTime} interface should be used when different
 * kinds of date/time objects are to be referenced.
 * <p>
 * BaseDateTime subclasses may be mutable
 */
class BaseDateTime extends AbstractDateTime {

    /**
     * The millis from 1970-01-01T00:00:00Z
     * @var int
     */
    private $millis;

    /** @var \Chronology */
    private $chronology;

    /**
     * Constructs an instance set to the current system millisecond time
     * using <code>ISOChronology</code> in the default time zone.
     */
    public function __construct($millis = null, Chronology $chrono = null) {
        $this->millis = ($millis !== null) ? $millis : DateTimeUtils::currentTimeMillis();
        $this->chronology = $chrono ? $chrono : ISOChronology::getInstance();
    }

    /**
     * @return int
     */
    function getMillis() {
        return $this->millis;
    }

    /**
     * @return Chronology|ISOChronology
     */
    function getChronology() {
        return $this->chronology;
    }

    /**
     * @param Chronology $chronology
     * @return Chronology
     */
    protected static function checkChronology(Chronology $chronology = null) {
        return DateTimeUtils::getChronology($chronology);
    }

    /**
     * @param int $instant
     * @param Chronology $chronology
     * @return int
     */
    protected static function checkInstant($instant, Chronology $chronology) {
        return $instant;
    }
}