<?php

include_once dirname(__FILE__) . '/'  . 'abstract-instant.php';
include_once dirname(__FILE__) . '/'  . '../readable-datetime.php';
include_once dirname(__FILE__) . '/'  . '../format/datetime-format.php';

/**
 * AbstractDateTime provides the common behaviour for datetime classes.
 * <p>
 * This class should generally not be used directly by API users.
 * The {@link IReadableDateTime} interface should be used when different
 * kinds of date/time objects are to be referenced.
 * <p>
 * Whenever you want to implement <code>ReadableDateTime</code> you should
 * extend this class.
 * <p>
 * AbstractDateTime subclasses may be mutable and not thread-safe.
 */
abstract class AbstractDateTime extends AbstractInstant implements IReadableDateTime {

    /**
     * Get the era field value.
     *
     * @return int the era
     */
    public function getEra() {
        return $this->getChronology()->era()->get($this->getMillis());
    }

    /**
     * Get the year of era field value.
     *
     * @return int the year of era
     */
    public function getCenturyOfEra() {
        return $this->getChronology()->centuryOfEra()->get($this->getMillis());
    }

    /**
     * Get the year of era field value.
     *
     * @return int the year of era
     */
    public function getYearOfEra() {
        return $this->getChronology()->yearOfEra()->get($this->getMillis());
    }

    /**
     * Get the year of century field value.
     *
     * @return int the year of century
     */
    public function getYearOfCentury() {
        return $this->getChronology()->yearOfCentury()->get($this->getMillis());
    }

    /**
     * Get the year field value.
     *
     * @return int the year
     */
    public function getYear() {
        return $this->getChronology()->year()->get($this->getMillis());
    }

    /**
     * Get the weekyear field value.
     * <p>
     * The weekyear is the year that matches with the weekOfWeekyear field.
     * In the standard ISO8601 week algorithm, the first week of the year
     * is that in which at least 4 days are in the year. As a result of this
     * definition, day 1 of the first week may be in the previous year.
     * The weekyear allows you to query the effective year for that day.
     *
     * @return int the year of a week based year
     */
    public function getWeekyear() {
        return $this->getChronology()->weekyear()->get($this->getMillis());
    }

    /**
     * Get the month of year field value.
     *
     * @return int the month of year
     */
    public function getMonthOfYear() {
        return $this->getChronology()->monthOfYear()->get($this->getMillis());
    }

    /**
     * Get the week of weekyear field value.
     * <p>
     * This field is associated with the "weekyear" via {@link #getWeekyear()}.
     * In the standard ISO8601 week algorithm, the first week of the year
     * is that in which at least 4 days are in the year. As a result of this
     * definition, day 1 of the first week may be in the previous year.
     *
     * @return int the week of a week based year
     */
    public function getWeekOfWeekyear() {
        return $this->getChronology()->weekOfWeekyear()->get($this->getMillis());
    }

    /**
     * Get the day of year field value.
     *
     * @return int the day of year
     */
    public function getDayOfYear() {
        return $this->getChronology()->dayOfYear()->get($this->getMillis());
    }

    /**
     * Get the day of month field value.
     * <p>
     * The values for the day of month are defined in {@link org.joda.time.DateTimeConstants}.
     *
     * @return int the day of month
     */
    public function getDayOfMonth() {
        return $this->getChronology()->dayOfMonth()->get($this->getMillis());
    }

    /**
     * Get the day of week field value.
     * <p>
     * The values for the day of week are defined in {@link org.joda.time.DateTimeConstants}.
     *
     * @return int the day of week
     */
    public function getDayOfWeek() {
        return $this->getChronology()->dayOfWeek()->get($this->getMillis());
    }

    //-----------------------------------------------------------------------
    /**
     * Get the hour of day field value.
     *
     * @return int the hour of day
     */
    public function getHourOfDay() {
        return $this->getChronology()->hourOfDay()->get($this->getMillis());
    }

    /**
     * Get the minute of day field value.
     *
     * @return int the minute of day
     */
    public function getMinuteOfDay() {
        return $this->getChronology()->minuteOfDay()->get($this->getMillis());
    }

    /**
     * Get the minute of hour field value.
     *
     * @return int the minute of hour
     */
    public function getMinuteOfHour() {
        return $this->getChronology()->minuteOfHour()->get($this->getMillis());
    }

    /**
     * Get the second of day field value.
     *
     * @return int the second of day
     */
    public function getSecondOfDay() {
        return $this->getChronology()->secondOfDay()->get($this->getMillis());
    }

    /**
     * Get the second of minute field value.
     *
     * @return int the second of minute
     */
    public function getSecondOfMinute() {
        return $this->getChronology()->secondOfMinute()->get($this->getMillis());
    }

    /**
     * Get the millis of day field value.
     *
     * @return int the millis of day
     */
    public function getMillisOfDay() {
        return $this->getChronology()->millisOfDay()->get($this->getMillis());
    }

    /**
     * Get the millis of second field value.
     *
     * @return int the millis of second
     */
    public function getMillisOfSecond() {
        return $this->getChronology()->millisOfSecond()->get($this->getMillis());
    }

    /**
     * @param string $pattern
     * @param PTLocale $locale
     * @return string
     */
    private function toStringWithLocale($pattern, PTLocale $locale) {
        return DateTimeFormat::forPattern($pattern)->withLocale($locale)->printInstant($this);
    }

    /**
     * Output the instant using the specified format pattern.
     *
     * @param string $pattern the pattern specification
     * @param null|PTLocale $locale
     * @return string
     */
    public function toString($pattern = null, PTLocale $locale = null) {
        if ($pattern == null) {
            parent::toString();
        }
        if ($locale != null)
            return $this->toStringWithLocale($pattern, $locale);
        return DateTimeFormat::forPattern($pattern)->printInstant($this);
    }
}