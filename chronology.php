<?php

include_once dirname(__FILE__) . '/'  . 'core/core.php';
include_once dirname(__FILE__) . '/'  . 'datetime-field.php';

/**
 * Describes a chronology system such as Gregorian or Julian calendar
 */
abstract class Chronology implements IPTEquatable {

    /**
     * Empty constructor
     */
    protected function __construct() { }

    /**
     * Returns the PTDateTimeZone that this Chronology operates in, or null if
     * unspecified.
     *
     * @return PTDateTimeZone the PTDateTimeZone, null if unspecified
     */
    public abstract function getZone();


    /**
     * Returns an instance of this Chronology that operates in the UTC time
     * zone. Chronologies that do not operate in a time zone or are already
     * UTC must return themself.
     *
     * @return Chronology a version of this chronology that ignores time zones
     */
    public abstract function withUTC();

    /**
     * Returns an instance of this Chronology that operates in any time zone.
     *
     * @return Chronology a version of this chronology with a specific time zone
     * @param PTDateTimeZone $zone to use, or default if null
     * @see ZonedChronology
     */
    public abstract function withZone(PTDateTimeZone $zone = null);


    /**
     * Returns a datetime millisecond instant, formed from the given year,
     * month, day, hour, minute, second, and millisecond values. The set of
     * given values must refer to a valid datetime, or else an
     * IllegalArgumentException is thrown.
     * <p>
     * The default implementation calls upon separate DateTimeFields to
     * determine the result. Subclasses are encouraged to provide a more
     * efficient implementation.
     *
     * @param int $year year to use
     * @param int $monthOfYear month to use
     * @param int $dayOfMonth day of month to use
     * @param int $hourOfDay hour to use
     * @param int $minuteOfHour minute to use
     * @param int $secondOfMinute second to use
     * @param int $millisOfSecond millisecond to use
     * @return int millisecond instant from 1970-01-01T00:00:00Z
     * @throws IllegalArgumentException if the values are invalid
     */
    public abstract function getDateTimeMillis($year, $monthOfYear, $dayOfMonth, $hourOfDay, $minuteOfHour, $secondOfMinute, $millisOfSecond);

    /**
     * @return DateTimeField
     */
    public abstract function millisOfSecond();

    /**
     * @return DateTimeField
     */
    public abstract function millisOfDay();

    /**
     * @return DateTimeField
     */
    public abstract function secondOfMinute();

    /**
     * @return DateTimeField
     */
    public abstract function secondOfDay();

    /**
     * @return DateTimeField
     */
    public abstract function minuteOfHour();

    /**
     * @return DateTimeField
     */
    public abstract function minuteOfDay();

    /**
     * @return DateTimeField
     */
    public abstract function hourOfDay();

    /**
     * @return DateTimeField
     */
    public abstract function clockhourOfDay();

    /**
     * @return DateTimeField
     */
    public abstract function hourOfHalfday();

    /**
     * @return DateTimeField
     */
    public abstract function clockhourOfHalfday();

    /**
     * @return DateTimeField
     */
    public abstract function halfdayOfDay();

    /**
     * @return DateTimeField
     */
    public abstract function dayOfWeek();

    /**
     * @return DateTimeField
     */
    public abstract function dayOfMonth();

    /**
     * @return DateTimeField
     */
    public abstract function dayOfYear();

    /**
     * @return DateTimeField
     */
    public abstract function weekOfWeekyear();

    /**
     * @return DateTimeField
     */
    public abstract function weekyear();

    /**
     * @return DateTimeField
     */
    public abstract function weekyearOfCentury();

    /**
     * @return DateTimeField
     */
    public abstract function monthOfYear();

    /**
     * @return DateTimeField
     */
    public abstract function year();

    /**
     * @return DateTimeField
     */
    public abstract function yearOfEra();

    /**
     * @return DateTimeField
     */
    public abstract function yearOfCentury();

    /**
     * @return DateTimeField
     */
    public abstract function centuryOfEra();

    /**
     * @return DateTimeField
     */
    public abstract function era();

    /**
     * @param mixed $object
     * @return bool
     */
    public function equals($object) {
        return $this === $object;
    }

    /**
     * @return string
     */
    public function hashCode() {
        return CoreUtils::getObjectUniqueHashCode($this);
    }
}
