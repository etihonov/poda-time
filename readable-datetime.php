<?php

include_once dirname(__FILE__) . '/'  . 'readable-instant.php';

/**
 * Defines an instant in time that can be queried using datetime fields.
 * <p>
 * The implementation of this interface may be mutable or immutable.
 * This interface only gives access to retrieve data, never to change it.
 * <p>
 * Methods in your application should be defined using <code>IReadableDateTime</code>
 * as a parameter if the method only wants to read the datetime, and not perform
 * any advanced manipulations.
 */
interface IReadableDateTime extends IReadableInstant {

    /**
     * Get the year field value.
     *
     * @return int
     */
    function getYear();

    /**
     * @return int
     */
    function getMonthOfYear();

    /**
     * @return int
     */
    function getDayOfMonth();
}