<?php

include_once dirname(__FILE__) . '/'  . '../datetime-field.php';

/**
 * BaseDateTimeField provides the common behaviour for DateTimeField
 * implementations.
 * <p>
 * This class should generally not be used directly by API users. The
 * DateTimeField class should be used when different kinds of DateTimeField
 * objects are to be referenced.
 * <p>
 * BaseDateTimeField immutable, and its subclasses must be as well.
 *
 * @see DecoratedDateTimeField
 */
abstract class BaseDateTimeField extends DateTimeField {

    /** @var \DateTimeFieldType */
    private $type;

    /**
     * @param DateTimeFieldType $type
     */
    protected function __construct(DateTimeFieldType $type) {
        parent::__construct();
        $this->type = $type;
    }

    /**
     * @return DateTimeFieldType
     */
    public final function getType() {
        return $this->type;
    }

    /**
     * @param int $fieldValue
     * @param PTLocale $locale
     * @return string
     */
    public function getAsTextFromValue($fieldValue, PTLocale $locale = null) {
        return strval($fieldValue);
    }

    /**
     * @param int $instant
     * @param PTLocale $locale
     * @return string
     */
    public function getAsShortTextFromInstant($instant, PTLocale $locale = null) {
        return $this->getAsShortTextFromValue($this->get($instant), $locale);
    }

    /**
     * @param string $fieldValue
     * @param PTLocale $locale
     * @return string
     */
    public function getAsShortTextFromValue($fieldValue, PTLocale $locale = null) {
        return $this->getAsTextFromValue($fieldValue, $locale);
    }

    /**
     * {@inheritdoc}
     */
    public function set($instant, $year) {
        throw new UnimplementedOperationException();
    }
}
