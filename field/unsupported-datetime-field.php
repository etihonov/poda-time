<?php

include_once dirname(__FILE__) . '/'  . '../datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../exceptions.php';

class UnsupportedDateTimeField extends DateTimeField {

    /**
     * @param DateTimeFieldType $type
     * @return UnsupportedDateTimeField
     */
    public static function getInstance(DateTimeFieldType $type) {
        // TODO implement instance caching
        return new UnsupportedDateTimeField($type);
    }

    /**
     * @var DateTimeFieldType
     */
    private $type;

    /**
     * @param DateTimeFieldType $type
     */
    public function __construct(DateTimeFieldType $type) {
        $this->type = $type;
    }

    /**
     * @return DateTimeFieldType
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param int $instant
     * @return int
     * @throws UnsupportedOperationException
     */
    public function get($instant) {
        throw new UnsupportedOperationException();
    }

    /**
     * Always throws UnsupportedOperationException
     *
     * @throws UnsupportedOperationException
     */
    public function set($instant, $value) {
        throw new UnsupportedOperationException();
    }

    /**
     * Get the human-readable, text value of this field from the field value.
     * If the specified locale is null, the default locale is used.
     *
     * @param int $fieldValue  the numeric value to convert to text
     * @param PTLocale $locale the locale to use for selecting a text symbol, null for default
     * @throws UnsupportedOperationException
     * @return string the text value of the field
     */
    public function getAsTextFromValue($fieldValue, PTLocale $locale = null) {
        throw new UnsupportedOperationException();
    }

    /**
     * Get the human-readable, short text value of this field from the
     * milliseconds.  If the specified locale is null, the default locale is used.
     *
     * @param int $instant  the milliseconds from 1970-01-01T00:00:00Z to query
     * @param PTLocale $locale the locale to use for selecting a text symbol, null for default
     * @throws UnsupportedOperationException
     * @return string the short text value of the field
     */
    public function getAsShortTextFromInstant($instant, PTLocale $locale = null) {
        throw new UnsupportedOperationException();
    }

    /**
     * Get the human-readable, short text value of this field from the field value.
     * If the specified locale is null, the default locale is used.
     *
     * @param string $fieldValue  the numeric value to convert to text
     * @param PTLocale $locale the locale to use for selecting a text symbol, null for default
     * @throws UnsupportedOperationException
     * @return string the text value of the field
     */
    public function getAsShortTextFromValue($fieldValue, PTLocale $locale = null) {
        throw new UnsupportedOperationException();
    }
}