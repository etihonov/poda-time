<?php

include_once dirname(__FILE__) . '/'  . 'base-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../exceptions.php';

/**
 * <code>DecoratedDateTimeField</code> extends {@link BaseDateTimeField},
 * implementing only the minimum required set of methods. These implemented
 * methods delegate to a wrapped field.
 * <p>
 * This design allows new DateTimeField types to be defined that piggyback on
 * top of another, inheriting all the safe method implementations from
 * BaseDateTimeField. Should any method require pure delegation to the
 * wrapped field, simply override and use the provided getWrappedField method.
 * <p>
 * DecoratedDateTimeField immutable, and its subclasses must
 * be as well.
 *
 * @see DelegatedDateTimeField
 */
abstract class DecoratedDateTimeField extends BaseDateTimeField {

    /**
     * The DateTimeField being wrapped
     * @var DateTimeField
     */
    private $field;


    /**
     * @param DateTimeField $field the field being decorated
     * @param DateTimeFieldType $type allow type to be overridden
     * @throws IllegalArgumentException
     */
    protected function __construct(DateTimeField $field, DateTimeFieldType $type) {
        parent::__construct($type);
        $this->field = $field;
        if ($field == null) {
            throw new IllegalArgumentException("The field must not be null");
        }
        /*if ($field->isSupported()) {
            throw new IllegalArgumentException("The field must be supported");
        }*/
        $this->field = $field;

    }

    /**
     * Gets the wrapped date time field.
     *
     * @return DateTimeField the wrapped DateTimeField
     */
    public final function getWrappedField() {
        return $this->field;
    }

    /**
     * @param int $instant
     * @return int
     */
    public function get($instant) {
        return $this->field->get($instant);
    }

    public function set($instant, $value) {
        return $this->field->set($instant, $value);
    }

}
