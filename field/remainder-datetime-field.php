<?php

include_once dirname(__FILE__) . '/'  . 'decorated-datetime-field.php';
include_once dirname(__FILE__) . '/'  . 'divided-datetime-field.php';

/**
 * Counterpart remainder datetime field to {@link DividedDateTimeField}. The
 * field's unit duration is unchanged, but the range duration is scaled
 * accordingly.
 * <p>
 * RemainderDateTimeField is immutable.
 */
class RemainderDateTimeField extends DecoratedDateTimeField {

    /**
     * @param DividedDateTimeField $field
     * @param DateTimeFieldType $type
     * @return \RemainderDateTimeField
     */
    public static function createFromDividedField(DividedDateTimeField $field, DateTimeFieldType $type = null) {
        if ($type == null) {
            $type = $field->getType();
        }
        return new RemainderDateTimeField($field->getWrappedField(), $type, $field->getDivisor());
    }

    /** @var int */
    private $divisor;

    /**
     * @param DateTimeField $field
     * @param DateTimeFieldType $type
     * @param int $divisor
     * @throws IllegalArgumentException
     */
    public function __construct(DateTimeField $field, DateTimeFieldType $type, $divisor) {
        parent::__construct($field, $type);

        if ($divisor < 2) {
            throw new IllegalArgumentException("The divisor must be at least 2");
        }

        /*$rangeField = $field->getDurationField();
        if (rangeField == null) {
            iRangeField = null;
        } else {
            iRangeField = new ScaledDurationField(
                rangeField, type.getRangeDurationType(), divisor);
        }*/

        $this->divisor = $divisor;
    }

    /**
     * @param int $millis
     * @return int
     */
    public function get($millis) {
        $value = $this->getWrappedField()->get($millis);
        if ($value >= 0) {
            return $value % $this->divisor;
        } else {
            return ($this->divisor - 1) + (($this->divisor + 1) % $this->divisor);
        }
    }

    /**
     * Set the specified amount of remainder units to the specified time instant.
     *
     * @param int $instant  the time instant in millis to update.
     * @param int $value  value of remainder units to set.
     * @return int the updated time instant.
     * @throws IllegalArgumentException if value is too large or too small.
     */
    public function set($instant, $value) {
        //FieldUtils::verifyValueBounds(this, value, 0, iDivisor - 1);
        $divided = $this->getDivided($this->getWrappedField()->get($instant));
        return $this->getWrappedField()->set($instant, $divided * $this->divisor + $value);
    }

    private function getDivided($value) {
        if ($value >= 0) {
            return bcdiv($value, $this->divisor, 0);
        } else {
            return bcdiv(($value + 1), $this->divisor, 0) - 1;
        }
    }


}
