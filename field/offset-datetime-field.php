<?php

include_once dirname(__FILE__) . '/'  . 'decorated-datetime-field.php';

/**
 * Generic offset adjusting datetime field.
 * <p>
 * OffsetDateTimeField is immutable.
 */
class OffsetDateTimeField extends DecoratedDateTimeField {

    private $offset;
    private $min;
    private $max;

    /**
     * @param DateTimeField $field
     * @param DateTimeFieldType $type
     * @param int $offset
     * @param int $minValue
     * @param int $maxValue
     * @throws IllegalArgumentException
     */
    public function __construct(DateTimeField $field, DateTimeFieldType $type, $offset, $minValue, $maxValue) {
        parent::__construct($field, $type);

        if ($offset == 0) {
            throw new IllegalArgumentException("The offset cannot be zero");
        }

        $this->offset = $offset;

        /*        if ($minValue < ($field->getMinimumValue() + $offset)) {
                    $this->min = $field->getMinimumValue() + $offset;
                } else {
                    $this->min = $minValue;
                }

                if ($maxValue > ($field->getMaximumValue() + $offset)) {
                    $this->max = $field->getMaximumValue() + $offset;
                } else {
                    $this->max = $maxValue;
                }*/
    }

    /**
     * @param int $instant
     * @return int
     */
    public function get($instant) {
        return parent::get($instant) + $this->offset;
    }

    /**
     * Set the specified amount of offset units to the specified time instant.
     *
     * @param int $instant  the time instant in millis to update.
     * @param int $value  value of units to set.
     * @return int the updated time instant.
     * @throws IllegalArgumentException if value is too large or too small.
     */
    public function set($instant, $value) {
        //FieldUtils::verifyValueBounds($this, value, iMin, iMax);
        return parent::set($instant, $value - $this->offset);
    }

}
