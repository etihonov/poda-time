<?php

include_once dirname(__FILE__) . '/'  . 'base-datetime-field.php';

/**
 * Precise datetime field, which has a precise unit duration field.
 * <p>
 * PreciseDurationDateTimeField is immutable, and its
 * subclasses must be as well.
 */
abstract class PreciseDurationDateTimeField extends BaseDateTimeField {

    /** The fractional unit in millis */
    private $unitMillis;

    private $unitField;

    /**
     * @param DateTimeFieldType $type
     * @param DurationField $unit
     * @throws IllegalArgumentException
     */
    public function __construct(DateTimeFieldType $type, DurationField $unit) {
        parent::__construct($type);

        /* if (!unit.isPrecise()) {
            throw new IllegalArgumentException("Unit duration field must be precise");
        } */

        $this->unitMillis = $unit->getUnitMillis();
        if ($this->unitMillis < 1) {
            throw new IllegalArgumentException("The unit milliseconds must be at least 1");
        }

        $this->unitField = $unit;
    }

    /**
     * Set the specified amount of units to the specified time instant.
     *
     * @param int $instant  the milliseconds from 1970-01-01T00:00:00Z to set in
     * @param int $value  value of units to set.
     * @return int the updated time instant.
     * @throws IllegalArgumentException if value is too large or too small.
     */
    public function set($instant, $value) {
        /*TODO
          FieldUtils::verifyValueBounds($this, $value,
            $this->getMinimumValue(),
            $this->getMaximumValueForSet($instant, $value));*/

        return $instant + ($value - $this->get($instant)) * $this->unitMillis;
    }

    /**
     * @return int
     */
    public final function getUnitMillis() {
        return $this->unitMillis;
    }




}