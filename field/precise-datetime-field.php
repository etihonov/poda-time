<?php

include_once dirname(__FILE__) . '/'  . '../datetime-field-type.php';
include_once dirname(__FILE__) . '/'  . '../duration-field.php';
include_once dirname(__FILE__) . '/'  . 'precise-duration-datetime-field.php';

/**
 * Precise datetime field, composed of two precise duration fields.
 * <p>
 * This DateTimeField is useful for defining DateTimeFields that are composed
 * of precise durations, like time of day fields. If either duration field is
 * imprecise, then an {@link ImpreciseDateTimeField} may be used instead.
 * <p>
 * PreciseDateTimeField is immutable.
 *
 * @see ImpreciseDateTimeField
 */
class PreciseDatetimeField extends PreciseDurationDateTimeField {

    private $rangeField;

    private $range;
    /**
     * @param DateTimeFieldType $type
     * @param DurationField $unit
     * @param DurationField $range
     */
    public function __construct(DateTimeFieldType $type, DurationField $unit, DurationField $range) {
        parent::__construct($type, $unit);

        $rangeMillis = $range->getUnitMillis();
        $this->range = bcdiv($rangeMillis, $this->getUnitMillis(), 0);

        $this->rangeField = $range;
    }

    /**
     * Set the specified amount of units to the specified time instant.
     *
     * @param int $instant  the milliseconds from 1970-01-01T00:00:00Z to set in
     * @param int $value  value of units to set.
     * @return int the updated time instant.
     * @throws IllegalArgumentException if value is too large or too small.
     */
    public function set($instant, $value) {
        //FieldUtils.verifyValueBounds(this, value, getMinimumValue(), getMaximumValue());
        return bcadd($instant, bcmul(bcsub($value, $this->get($instant)), $this->getUnitMillis()));
    }

    /**
     * @param int $millis
     * @return int
     */
    public function get($millis) {
        if ($millis >= 0) {
            return intval((bcmod( bcdiv($millis, $this->getUnitMillis(), 0), $this->range)));
        }
        else {
            return bcadd(bcsub($this->range, 1), intval(bcmod(bcdiv(bcadd($millis, 1), $this->getUnitMillis(), 0), $this->range)));
        }
    }
}
