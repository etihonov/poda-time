<?php 

include_once dirname(__FILE__) . '/'  . '../duration-field.php';
include_once dirname(__FILE__) . '/'  . '../duration-field-type.php';

/**
 * BaseDurationField provides the common behaviour for DurationField
 * implementations.
 * <p>
 * This class should generally not be used directly by API users. The
 * DurationField class should be used when different kinds of DurationField
 * objects are to be referenced.
 * <p>
 * BaseDurationField is immutable, and its subclasses must
 * be as well.

 * @see DecoratedDurationField
 */
abstract class BaseDurationField extends DurationField {

    private $type;

    /**
     * @param DurationFieldType $type
     * @throws IllegalArgumentException
     */
    public function __construct(DurationFieldType $type) {
        parent::__construct();
        if ($type == null) {
            throw new IllegalArgumentException("The type must not be null");
        }
        $this->type = $type;
    }

    public final function getName() {
        $this->type->getName();
    }
}