<?php

/**
 * General utilities that don't fit elsewhere.
 * <p>
 * FieldUtils is immutable.
 */
class FieldUtils {

    /**
     * Verify that input values are within specified bounds.
     *
     * @param DateTimeField $field
     * @param int $value  the value to check
     * @param int $lowerBound  the lower bound allowed for value
     * @param int $upperBound  the upper bound allowed for value
     * @throws IllegalArgumentException
     */
    public static function verifyValueBounds(DateTimeField $field, $value, $lowerBound, $upperBound) {
        if (($value < $lowerBound) || ($value > $upperBound)) {
            // TODO throw new IllegalFieldValueException($field->getType(), Integer::valueOf(value),Integer::valueOf($lowerBound), Integer::valueOf($upperBound));
            throw new IllegalArgumentException();
        }
    }


    /**
     * Compares two objects as equals handling null.
     *
     * @param mixed $object1  the first object
     * @param mixed $object2  the second object
     * @return bool true if equal
     */
    public static function equals($object1, $object2) {
        if ($object1 === $object2) {
            return true;
        }
        if ($object1 == null || $object2 == null) {
            return false;
        }
        if ($object1 instanceof IPTEquatable && $object2 instanceof IPTEquatable) {
            return $object1->equals($object2);
        }
        return false;
    }

}