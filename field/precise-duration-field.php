<?php 

include_once dirname(__FILE__) . '/'  . 'base-duration-field.php';

/**
 * Duration field class representing a field with a fixed unit length.
 * <p>
 * PreciseDurationField is immutable.
 */
class PreciseDurationField extends BaseDurationField {

    /** @var int */
    private $unitMillis;

    /**
     * @param DurationFieldType $type
     * @param int $unitMillis
     */
    public function __construct(DurationFieldType $type, $unitMillis) {
        parent::__construct($type);
        $this->unitMillis = $unitMillis;
    }

    /**
     * Returns the amount of milliseconds per unit value of this field.
     *
     * @return int the unit size of this field, in milliseconds
     */
    public function getUnitMillis() {
        return $this->unitMillis;
    }
}
