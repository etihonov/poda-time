<?php

include_once dirname(__FILE__) . '/'  . 'decorated-datetime-field.php';

/**
 * Divides a DateTimeField such that the retrieved values are reduced by a
 * fixed divisor. The field's unit duration is scaled accordingly, but the
 * range duration is unchanged.
 * <p>
 * DividedDateTimeField is immutable.
 *
 * @see RemainderDateTimeField
 */
class DividedDateTimeField extends DecoratedDateTimeField {

    /** @var int */
    private $divisor;

    /**
     * @param DateTimeField $field the field to wrap, like "year()".
     * @param DateTimeFieldType $type the field type this field will actually use
     * @param int $divisor divisor, such as 100 years in a century
     * @throws IllegalArgumentException
     */
    public function __construct(DateTimeField $field, DateTimeFieldType $type, $divisor) {
        parent::__construct($field, $type);

        if ($divisor < 2) {
            throw new IllegalArgumentException("The divisor must be at least 2");
        }

        $this->divisor = $divisor;

    }

    /**
     * Get the amount of scaled units from the specified time instant.
     *
     * @param int $instant  the time instant in millis to query.
     * @return int the amount of scaled units extracted from the input.
     */
    public function get($instant) {
        $value = $this->getWrappedField()->get($instant);
        if ($value >= 0) {
            return (int)($value / $this->divisor);
        } else {
            return ((int)(($value + 1) / $this->divisor)) - 1;
        }
    }

    /**
     * Set the specified amount of scaled units to the specified time instant.
     *
     * @param int $instant  the time instant in millis to update.
     * @param int $value  value of scaled units to set.
     * @return int the updated time instant.
     * @throws IllegalArgumentException if value is too large or too small.
     */
    public function set($instant, $value) {
        //FieldUtils::verifyValueBounds(this, value, iMin, iMax);
        $remainder = $this->getRemainder($this->getWrappedField()->get($instant));
        return $this->getWrappedField()->set($instant, $value * $this->divisor + $remainder);
    }

    private function getRemainder($value) {
        if ($value >= 0) {
            return $value % $this->divisor;
        } else {
            return ($this->divisor - 1) + (($value + 1) % $this->divisor);
        }
    }

    /**
     * @return int
     */
    public final function getDivisor() {
        return $this->divisor;
    }
}
