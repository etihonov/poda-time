<?php

/**
 * Duration field class representing a field with a fixed unit length of one
 * millisecond.
 * <p>
 * MillisDurationField is immutable.
 */
class MillisDurationField extends DurationField {

    /** Singleton instance. */
    private static $instance;

    /**
     * @return MillisDurationField
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new MillisDurationField();
        }
        return self::$instance;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'millis';
    }

    /**
     * Returns the amount of milliseconds per unit value of this field.
     *
     * @return int one always
     */
    public final function getUnitMillis() {
        return 1;
    }

}
