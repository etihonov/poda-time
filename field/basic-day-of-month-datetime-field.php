<?php

include_once dirname(__FILE__) . '/'  . '../duration-field.php';
include_once dirname(__FILE__) . '/'  . 'precise-duration-datetime-field.php';
include_once dirname(__FILE__) . '/'  . '../chrono/basic-chronology.php';

class BasicDayOfMonthDateTimeField extends PreciseDurationDateTimeField {
    
    private $chronology;

    public function __construct(BasicChronology $chronology, DurationField $days) {
        parent::__construct(DateTimeFieldType::dayOfMonth(), $days);
        $this->chronology = $chronology;
    }

    /**
     * Get the value of this field from the milliseconds.
     * 
     * @param int $instant the milliseconds from 1970-01-01T00:00:00Z to query
     * @return int
     */
    public function get($instant) {
        return $this->chronology->getDayOfMonth($instant);
    }

}