<?php

include_once dirname(__FILE__) . '/'  . 'locale.php';

/**
 * Defines the calculation engine for date and time fields.
 * The interface defines a set of methods that manipulate a millisecond datetime
 * with regards to a single field, such as monthOfYear or secondOfMinute.
 * <p>
 * This design is extensible so, if you wish, you can extract a different field from
 * the milliseconds. A number of standard implementations are provided to assist.
 */
abstract class DateTimeField {

    /**
     * Empty constructor
     */
    protected function __construct() { }

    /**
     * @param int $instant
     * @return int
     */
    public abstract function get($instant);

    /**
     * Sets a value in the milliseconds supplied.
     * <p>
     * The value of this field will be set.
     * If the value is invalid, an exception if thrown.
     * <p>
     * If setting this field would make other fields invalid, then those fields
     * may be changed. For example if the current date is the 31st January, and
     * the month is set to February, the day would be invalid. Instead, the day
     * would be changed to the closest value - the 28th/29th February as appropriate.
     *
     * @param int $instant  the milliseconds from 1970-01-01T00:00:00Z to set in
     * @param int $value  the value to set, in the units of the field
     * @return int the updated milliseconds
     * @throws IllegalArgumentException if the value is invalid
     */
    public abstract function set($instant, $value);


    /**
     * Get the type of the field.
     * @return DateTimeFieldType field type
     */
    public abstract function getType();

    /**
     * Get the human-readable, text value of this field from the field value.
     * If the specified locale is null, the default locale is used.
     *
     * @param int $fieldValue  the numeric value to convert to text
     * @param PTLocale $locale the locale to use for selecting a text symbol, null for default
     * @return string the text value of the field
     */
    public abstract function getAsTextFromValue($fieldValue, PTLocale $locale = null);

    /**
     * Get the human-readable, short text value of this field from the
     * milliseconds.  If the specified locale is null, the default locale is used.
     *
     * @param int $instant  the milliseconds from 1970-01-01T00:00:00Z to query
     * @param PTLocale $locale the locale to use for selecting a text symbol, null for default
     * @return string the short text value of the field
     */
    public abstract function getAsShortTextFromInstant($instant, PTLocale $locale = null);

    /**
     * Get the human-readable, short text value of this field from the field value.
     * If the specified locale is null, the default locale is used.
     *
     * @param string $fieldValue  the numeric value to convert to text
     * @param PTLocale $locale the locale to use for selecting a text symbol, null for default
     * @return string the text value of the field
     */
    public abstract function getAsShortTextFromValue($fieldValue, PTLocale $locale = null);
}
