<?php

class DateTimeConstants {

    // These are ints not enumerations as they represent genuine int values
    /** Constant (1) representing January, the first month (ISO) */
    public static $JANUARY = 1;

    /** Constant (2) representing February, the second month (ISO) */
    public static $FEBRUARY = 2;

    /** Constant (3) representing March, the third month (ISO) */
    public static $MARCH = 3;

    /** Constant (4) representing April, the fourth month (ISO) */
    public static $APRIL = 4;

    /** Constant (5) representing May, the fifth month (ISO) */
    public static $MAY = 5;

    /** Constant (6) representing June, the sixth month (ISO) */
    public static $JUNE = 6;

    /** Constant (7) representing July, the seventh month (ISO) */
    public static $JULY = 7;

    /** Constant (8) representing August, the eighth month (ISO) */
    public static $AUGUST = 8;

    /** Constant (9) representing September, the nineth month (ISO) */
    public static $SEPTEMBER = 9;

    /** Constant (10) representing October, the tenth month (ISO) */
    public static $OCTOBER = 10;

    /** Constant (11) representing November, the eleventh month (ISO) */
    public static $NOVEMBER = 11;

    /** Constant (12) representing December, the twelfth month (ISO) */
    public static $DECEMBER = 12;


    public static $MILLIS_PER_SECOND;

    public static $SECONDS_PER_MINUTE;

    public static $MILLIS_PER_MINUTE;

    public static $MINUTES_PER_HOUR;

    public static $MILLIS_PER_HOUR;

    public static $HOURS_PER_DAY;

    public static $MILLIS_PER_DAY;

    /** Milliseconds in a typical week (ISO). Due to time zone offset changes, the
     * number of milliseconds per week can vary. */
    public static $MILLIS_PER_WEEK;

    /** Days in one week (7) (ISO) */
    public static $DAYS_PER_WEEK;

    /** Constant (0) representing BC, years before zero (from Calendar) */
    public static $BC = 0;
    /** Alternative constant (0) representing BCE, Before Common Era (secular) */
    public static $BCE = 0;

    /**
     * Alternative constant (1) representing CE, Common Era (secular).
     * <p>
     * All new chronologies with different Era values should try to assign
     * eras as follows. The era that was in force at 1970-01-01 (ISO) is assigned
     * the value 1. Earlier eras are assigned sequentially smaller numbers.
     * Later eras are assigned sequentially greater numbers.
     */
    public static $CE = 1;

    public static function staticInit() {

        self::$MILLIS_PER_SECOND = 1000;

        self::$SECONDS_PER_MINUTE = 60;

        self::$MILLIS_PER_MINUTE = self::$MILLIS_PER_SECOND * self::$SECONDS_PER_MINUTE;


        self::$MINUTES_PER_HOUR = 60;

        self::$MILLIS_PER_HOUR = self::$MILLIS_PER_MINUTE * self::$MINUTES_PER_HOUR;


        self::$HOURS_PER_DAY = 24;

        self::$MILLIS_PER_DAY = self::$MILLIS_PER_HOUR * self::$HOURS_PER_DAY;

        self::$DAYS_PER_WEEK = 7;

        self::$MILLIS_PER_WEEK = self::$MILLIS_PER_DAY * self::$DAYS_PER_WEEK;
    }
}

DateTimeConstants::staticInit();