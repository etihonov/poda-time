<?php

include_once dirname(__FILE__) . '/'  . 'core/core.php';

/**
 * Unmutable instant interface
 */
interface IReadableInstant extends  IPTEquatable {

    /**
     * @return int
     */
    function getMillis();

    /**
     * @return Chronology
     */
    function getChronology();

    /**
     * Gets the time zone of the instant from the chronology.
     *
     * @return PTDateTimeZone the DateTimeZone that the instant is using, never null
     */
    function getZone();


    /**
     * Is this instant equal to the instant passed in
     * comparing solely by millisecond.
     *
     * @param null|int|IReadableInstant $instant  an instant to check against, null means now
     * @return bool true if the instant is equal to the instant passed in
     */
    function isEqual($instant);
}