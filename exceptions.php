<?php

class IllegalArgumentException extends Exception { }

class UnimplementedOperationException extends Exception { }

class UnsupportedOperationException extends Exception { }

class InternalError extends Exception { }

class ArithmeticException extends Exception { }

class NullPointerException extends Exception { }