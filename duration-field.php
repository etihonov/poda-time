<?php

/**
 * Defines the calculation engine for duration fields.
 * The interface defines a set of methods that manipulate a millisecond duration
 * with regards to a single field, such as months or seconds.
 * <p>
 * This design is extensible so, if you wish, you can extract a different field from
 * the millisecond duration. A number of standard implementations are provided to assist.
 */
abstract class DurationField {

    /**
     * Empty constructor
     */
    protected function __construct() { }

    /**
     * Get the name of the field.
     * <p>
     * By convention, names are plural.
     *
     * @return string field name
     */
    public abstract function getName();

    /**
     * Returns the amount of milliseconds per unit value of this field. For
     * example, if this field represents "seconds", then this returns the
     * milliseconds in one second.
     * <p>
     * For imprecise fields, the unit size is variable, and so this method
     * returns a suitable average value.
     *
     * @return int the unit size of this field, in milliseconds
     * @see #isPrecise()
     */
    public abstract function getUnitMillis();
}